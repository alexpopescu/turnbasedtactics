﻿using SPINACH.Editor;
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(ISTopDownCamera))]
public class ISTopDownCameraEditor : Editor {

	public enum iSMouseButton{
		Left,Right,Middle
	}

	private ISTopDownCamera mCam;
	private bool baseSetting;
	private bool boundSetting;
	private bool followSetting;
	private bool mouseSetting;
	private bool keyboardSetting;
	private bool touchSetting;
	private bool previewing;
	private Vector3 op;

	void Awake () {
		mCam = target as ISTopDownCamera;
		baseSetting = EditorPrefs.GetBool ("ISTCEBS", false);
		boundSetting = EditorPrefs.GetBool("ISTCEBOS",false);
		followSetting = EditorPrefs.GetBool("ISTCEFS",false);
		mouseSetting = EditorPrefs.GetBool ("ISTCEMS", false);
		keyboardSetting = EditorPrefs.GetBool ("ISTCEKS", false);
		touchSetting = EditorPrefs.GetBool ("ISTCETS", false);
		previewing = EditorPrefs.GetBool ("ISTCEPING", false);
	}

	public override void OnInspectorGUI () {

		Undo.RecordObject (mCam, "ISTopDown Camera Setting");

		EditorGUILayout.Space ();
		mCam.followingTarget = EditorGUILayout.ObjectField ("Following Target",mCam.followingTarget, typeof(Transform), true) as Transform;
		EditorGUILayout.Space ();

		baseSetting = EditorGUILayout.Foldout(baseSetting,"Basic Setting");
		if (baseSetting) {
			EditorGUILayout.LabelField ("Smoothing Settings");
			mCam.movementLerpSpeed = EditorGUILayout.FloatField("  Movement Lerp Speed",mCam.movementLerpSpeed);
			mCam.rotationLerpSpeed = EditorGUILayout.FloatField("  Rotation Lerp Speed",mCam.rotationLerpSpeed);
			EditorGUILayout.Space ();

			EditorGUILayout.LabelField ("Scroll Settings");

			ISTopDownCScrollAnimationType tempType = mCam.scrollAnimationType;
			tempType = (ISTopDownCScrollAnimationType)EditorGUILayout.EnumPopup ("  Animation Type", tempType);

			if (tempType != mCam.scrollAnimationType 
				&& EditorUtility.DisplayDialog ("Replacing Changes", "If you switch to another animation type, your settings in current mode will be replaced or modified.", "Continue", "Cancel")) {
				mCam.scrollAnimationType = tempType;
			}


			switch (mCam.scrollAnimationType) {
			case ISTopDownCScrollAnimationType.Simple:
				
				Keyframe f_minHigh = mCam.scrollHigh.keys [0];
				Keyframe f_maxHigh = mCam.scrollHigh.keys [mCam.scrollHigh.keys.Length - 1];

				f_minHigh.value = EditorGUILayout.FloatField ("    Min High", f_minHigh.value);
				f_maxHigh.value = EditorGUILayout.FloatField ("    Max High", f_maxHigh.value);

				mCam.scrollHigh = AnimationCurve.Linear (0, f_minHigh.value, 1, f_maxHigh.value);

				EditorGUILayout.Space ();

				Keyframe f_minAngle = mCam.scrollXAngle.keys [0];
				Keyframe f_maxAngle = mCam.scrollXAngle.keys [mCam.scrollXAngle.keys.Length - 1];

				f_minAngle.value = EditorGUILayout.FloatField ("    Min Angle", f_minAngle.value);
				f_maxAngle.value = EditorGUILayout.FloatField ("    Max Angle", f_maxAngle.value);
				f_minAngle.outTangent = EditorGUILayout.FloatField ("    Increase Rate", f_minAngle.outTangent);

				f_maxAngle.inTangent = 1f;

				mCam.scrollXAngle = new AnimationCurve(f_minAngle,f_maxAngle);

				break;

			case ISTopDownCScrollAnimationType.Advanced:
				mCam.scrollXAngle = EditorGUILayout.CurveField(new GUIContent("    Scroll X Angle","Scroll X Angle Animation"),mCam.scrollXAngle);
				mCam.scrollHigh = EditorGUILayout.CurveField(new GUIContent("    Scroll High","Scroll High Animation"),mCam.scrollHigh);
				break;
			}

			EditorGUILayout.Space ();

			mCam.scrollValue = EditorGUILayout.Slider("  Start Scroll Value",mCam.scrollValue,0f,1f);
			EditorGUILayout.Space ();

			EditorGUILayout.LabelField ("Casting Settings");
			mCam.groundHighTest = EditorGUILayout.Toggle("  Ground Check",mCam.groundHighTest);
			if(mCam.groundHighTest){
				mCam.groundLayer = EditorGUILayout.LayerField("  Ground Layer",mCam.groundLayer);
			}

			EditorGUILayout.Space ();

			EditorGUILayout.LabelField ("Preview");
			previewing = EditorGUILayout.Toggle ("  Preview Settings", previewing);
			if (previewing) mCam.Adjust2AttitudeBaseOnCurrentSetting ();

			EditorGUILayout.Space ();
		}

		boundSetting = EditorGUILayout.Foldout(boundSetting,"Bound");
		if(boundSetting){

			mCam.bound.xMin = EditorGUILayout.FloatField("  Min X",mCam.bound.xMin);
			mCam.bound.xMax = EditorGUILayout.FloatField("  Max X",mCam.bound.xMax);
			mCam.bound.yMin = EditorGUILayout.FloatField("  Min Z",mCam.bound.yMin);
			mCam.bound.yMax = EditorGUILayout.FloatField("  Max Z",mCam.bound.yMax);

			if (GUILayout.Button ("Use Suggested Values") && EditorUtility.DisplayDialog("Replacing Your Setting","Use suggested value will replace your current settings.","Confirm","Cancel")) {
				Bounds[] discoveredBounds;

				MeshRenderer[] renderers = Resources.FindObjectsOfTypeAll<MeshRenderer>();
				discoveredBounds = new Bounds[renderers.Length];

				EditorUtility.DisplayProgressBar ("Calculating...", "Finding objects...", 0);
				for (int i = 0; i < discoveredBounds.Length; i++)
					discoveredBounds [i] = renderers [i].bounds;

				EditorUtility.DisplayProgressBar ("Calculating...", "Calculating bounds along X...", 0.25f);
				float endValues = Mathf.Infinity;
				for (int i = 0; i < discoveredBounds.Length; i++) {
					if (endValues > discoveredBounds [i].min.x)
						endValues = discoveredBounds [i].min.x;
				}
				mCam.bound.xMin = endValues;

				EditorUtility.DisplayProgressBar ("Calculating...", "Calculating bounds along X...", 0.5f);
				endValues = Mathf.NegativeInfinity;
				for (int i = 0; i < discoveredBounds.Length; i++) {
					if (endValues < discoveredBounds [i].max.x)
						endValues = discoveredBounds [i].max.x;
				}
				mCam.bound.xMax = endValues;

				EditorUtility.DisplayProgressBar ("Calculating...", "Calculating bounds along Z...", 0.75f);
				endValues = Mathf.Infinity;
				for (int i = 0; i < discoveredBounds.Length; i++) {
					if (endValues > discoveredBounds [i].min.z)
						endValues = discoveredBounds [i].min.z;
				}
				mCam.bound.yMin = endValues;

				EditorUtility.DisplayProgressBar ("Calculating...", "Calculating bounds along Z...", 0.99f);
				endValues = Mathf.NegativeInfinity;
				for (int i = 0; i < discoveredBounds.Length; i++) {
					if (endValues < discoveredBounds [i].max.z)
						endValues = discoveredBounds [i].max.z;
				}
				mCam.bound.yMax = endValues;

				EditorUtility.ClearProgressBar ();
			}
				
			EditorGUILayout.HelpBox("The white rectangle in scene view will help you configure scene bounds.",MessageType.Info);

			EditorGUILayout.Space ();
		}

		mouseSetting = EditorGUILayout.Foldout(mouseSetting,"Mouse Control Setting");
		if (mouseSetting) {
			mCam.mouseControl = EditorGUILayout.Toggle("  Enabled",mCam.mouseControl);
			if(mCam.mouseControl){
				if(EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android || EditorUserBuildSettings.activeBuildTarget == BuildTarget.iOS || EditorUserBuildSettings.activeBuildTarget == BuildTarget.WSAPlayer){
					EditorGUILayout.HelpBox("Please notice that mouse control is not supported on mobile platform. Mouse control is enabled now for you to debug in editor, but it will disable automatically when you build.",MessageType.Warning);
				}
				mCam.desktopRotateSpeed = EditorGUILayout.FloatField("  Rotate Speed",mCam.desktopRotateSpeed);
				mCam.desktopScrollSpeed = EditorGUILayout.FloatField("  Scroll Wheel Speed",mCam.desktopScrollSpeed);

				mCam.mouseRotateButton = System.Convert.ToInt32(EditorGUILayout.EnumPopup ("    Rotate Button", (iSMouseButton)mCam.mouseRotateButton));

				mCam.mouseLookUp = EditorGUILayout.Toggle ("  Mouse Lookup", mCam.mouseLookUp);
				if (mCam.mouseLookUp) {
					mCam.mouseLookUpRange = EditorGUILayout.FloatField ("    Lookup Range", mCam.mouseLookUpRange);

					AnimationCurve curve = mCam.mouseLookUpCurve;

					Keyframe f_minAngle = curve.keys [0];
					Keyframe f_maxAngle = curve.keys [curve.keys.Length - 1];

					f_minAngle.value = 0;
					f_maxAngle.value = 1;
					f_minAngle.outTangent = EditorGUILayout.FloatField ("    Increase Rate", f_minAngle.outTangent);

					f_maxAngle.inTangent = -f_minAngle.outTangent;

					mCam.mouseLookUpCurve = new AnimationCurve(f_minAngle,f_maxAngle);

				}

			}
			else{
				EditorGUILayout.HelpBox("Enable Mouse Control to control camera with mouse.",MessageType.Info);
			}

			EditorGUILayout.Space ();
		}

		keyboardSetting = EditorGUILayout.Foldout(keyboardSetting,"Keyboard Control Setting");
		if (keyboardSetting) {
			mCam.keyBoardControl = EditorGUILayout.Toggle("  Enabled",mCam.keyBoardControl);
			if(mCam.keyBoardControl){
				mCam.desktopRotateSpeed = EditorGUILayout.FloatField("  Rotate Speed",mCam.desktopRotateSpeed);

				EditorGUILayout.Space ();

				string[] axisList = ISEInputAxisReader.GetInputAxisList ();

				mCam.rotateAxis = StringPopup ("  Rotate Axis", axisList, mCam.rotateAxis);

			}
			else{
				EditorGUILayout.HelpBox("Enabel Keyboard Control to control camera with keyboard.",MessageType.Info);
			}

			EditorGUILayout.Space ();
		}

		touchSetting = EditorGUILayout.Foldout (touchSetting, "Touch Control Setting");
		if (touchSetting) {
			mCam.touchControl = EditorGUILayout.Toggle("  Enabled",mCam.touchControl);
			if(mCam.touchControl){
				
				ISTouchControlHandler[] handler = GameObject.FindObjectsOfType<ISTouchControlHandler>();
				if (PrefabUtility.GetPrefabType(mCam.gameObject) != PrefabType.Prefab && handler.Length <= 0){
					EditorGUILayout.HelpBox ("Touch input requires at less one ISTouchControlHandler to work. Please use Add Component to add one to any GameObject to continue. Or you can :", MessageType.Error);
					if (GUILayout.Button ("Add to this GameObject")) {
						mCam.gameObject.AddComponent<ISTouchControlHandler> ();
					}
				} else {
					mCam.touchRotateSpeed = EditorGUILayout.FloatField("  Rotate Speed",Mathf.Max(mCam.touchRotateSpeed,0.000001f));
					mCam.touchScrollSpeed = EditorGUILayout.FloatField("  Scroll Speed",Mathf.Max(mCam.touchScrollSpeed,0.000001f));
				}
			}
			else{
				EditorGUILayout.HelpBox("Enable Touch control to control camera with touch screen.",MessageType.Info);
			}

			EditorGUILayout.Space ();
		}

		EditorGUILayout.Space ();
		EditorGUILayout.Space ();

		EditorGUILayout.HelpBox ("Store your current setting into preset, so you can use them later by \" Tools > iS.Top-Down Camera > Create From Preset\".",MessageType.Info,true);

		if (GUILayout.Button ("Replace Preset with Current Settings") 
		&& EditorUtility.DisplayDialog("Replacing Preset","You are about to replace existing preset with your current setting, this cannot be undo.","Comfirm","Cancel")) {

			PrefabUtility.CreatePrefab ("Assets/iS.Top-Down Camera/Editor/Presets/iS.Top-Down Camera.prefab", mCam.gameObject);
		}

		if (GUI.changed) {
			EditorPrefs.SetBool ("ISTCEBS", baseSetting);
			EditorPrefs.SetBool ("ISTCEBOS", boundSetting);
			EditorPrefs.SetBool ("ISTCEFS", followSetting);
			EditorPrefs.SetBool ("ISTCEMS", mouseSetting);
			EditorPrefs.SetBool ("ISTCEKS", keyboardSetting);
			EditorPrefs.SetBool ("ISTCETS", touchSetting);
			EditorPrefs.GetBool ("ISTCEPING", previewing);

			EditorUtility.SetDirty (mCam);
		}
	}

	string StringPopup(string label, string[] list, string currentValue){
		int[] optionList = new int[list.Length];
		int curID = 0;
		for (int i = 0; i < list.Length; i++){
			optionList [i] = i;
			if (list [i] == currentValue)
				curID = i;
		}

		curID = EditorGUILayout.IntPopup (label, curID, list, optionList);
		return list [curID];
	}
}
