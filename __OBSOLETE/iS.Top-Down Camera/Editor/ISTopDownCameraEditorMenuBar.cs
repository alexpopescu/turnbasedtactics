﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ISTopDownCameraEditorMenuBar {

	[MenuItem("Tools/iS.Top-Down Camera/Create From Preset")]
	static public void CreateFromPreset(){
		GameObject preset =	AssetDatabase.LoadMainAssetAtPath("Assets/iS.Top-Down Camera/Editor/Presets/iS.Top-Down Camera.prefab") as GameObject;
		GameObject obj = MonoBehaviour.Instantiate (preset, Vector3.zero, Quaternion.identity) as GameObject;
			
		obj.name = "iS.Top-Down Camera";
		Selection.activeGameObject = obj;
	}

	[MenuItem("Tools/iS.Top-Down Camera/Make Selected GameObject As iS.Top-Down Camera")]
	static public void MakeAsCamera(){
		if (Selection.activeGameObject)
			Selection.activeGameObject.AddComponent<ISTopDownCamera> ();
	}
}
