﻿using UnityEngine;
using System.Collections;

/// <summary>
/// iS.Top-Down Camera
/// Created by SPINACH.
/// 
/// © 2013 - 2016 SPINACH All rights reserved.
/// </summary>

public class ISTopDownCamera : MonoBehaviour {
	
	public ISTopDownCScrollAnimationType scrollAnimationType;
	public AnimationCurve scrollXAngle = AnimationCurve.Linear(0,0,1,1);
	public AnimationCurve scrollHigh = AnimationCurve.Linear(0,0,1,1);
	public float groundHigh;
	public float minHigh;
	public bool groundHighTest;
	public int groundLayer;

	public float scrollValue;

	public float movementLerpSpeed;
	public float rotationLerpSpeed;

	public float mouseLookUpRange;
	public AnimationCurve mouseLookUpCurve;

	public Rect bound;
	public Transform followingTarget;
	public Transform fixedPoint;

	public float desktopScrollSpeed;
	public float desktopRotateSpeed;

	public float touchScrollSpeed;
	public float touchRotateSpeed; 

	public int mouseRotateButton;

	public string rotateAxis;

	public Vector3 objectPos;
	Vector3 lookUpPos = Vector3.zero;

	private Transform selfT;

	public float wantYAngle;
	public float wantXAngle;

	public bool mouseLookUp;
	public bool keyBoardControl;
	public bool mouseControl;
	public bool touchControl;

	static private ISTopDownCamera self;

	private LayerMask groundMask;

	#region runtime_control_switch
	public void KeyboardControl(bool enable){
		keyBoardControl = enable;
		if (keyBoardControl) StartCoroutine (UpdateKeyboardControl ());
	}

	public void MouseControl(bool enable){
		
		if (Application.isMobilePlatform) return;

		mouseControl = enable;
		if (mouseControl) StartCoroutine (UpdateMouseControl ());
	}

	public void TouchControl(bool enable){
		touchControl = enable;
		if (touchControl) StartCoroutine (UpdateTouchControl ());
	}
	#endregion

	#region static_methods
	static public ISTopDownCamera GetInstantiated(){ return self;}

	static public void LockFixedPointForMain(Transform pos){
		self.fixedPoint = pos;

		//Set the wantPos to make camera more smooth when leave fixed point.
		self.objectPos.x = pos.position.x;
		self.objectPos.z = pos.position.z;
	}

	static public void UnlockFixedPointForMain(){
		self.fixedPoint = null;
	}

	static public void SetTargetObjectForMain(Transform target){
		self.fixedPoint = null;
		self.followingTarget = target;
	}

	static public Transform GetFollowingTarget(){return self.followingTarget;}
	static public Transform GetFixedPoint(){return self.fixedPoint;}
	#endregion

	public void LockFixedPoint(Transform pos){
		self.fixedPoint = pos;
	}

	public void UnlockFixedPoint(){
		self.fixedPoint = null;
	}

	public void SetTargetObject(Transform target){
		self.fixedPoint = null;
		self.followingTarget = target;
	}

	public Vector3 CalculateCurrentObjectPosition(){
		
		float dist = objectPos.y * Mathf.Tan((90f - wantXAngle) * Mathf.Deg2Rad);

		Vector3 objectPosDir = -(transform.rotation * (-Vector3.forward * dist));
		return transform.position + objectPosDir;
	}

	/// <summary>
	/// Adjust to the attitude base on current setting.
	/// Editor use this method to generate preview.
	/// </summary>
	public void Adjust2AttitudeBaseOnCurrentSetting(){
		groundMask = 1<<groundLayer;
		objectPos = CalculateCurrentObjectPosition ();
		scrollValue = Mathf.Clamp01(scrollValue);

		float currentGroundHigh = groundHigh;

		RaycastHit hit;
		Vector3 emitPos = objectPos;
		emitPos.y += 9999f;
		if (groundHighTest && Physics.Raycast (emitPos, -Vector3.up, out hit, Mathf.Infinity, groundMask)) {
			currentGroundHigh = hit.point.y;
		}

		emitPos = transform.position;
		emitPos.y += 9999f;
		if (groundHighTest && Physics.Raycast (emitPos, -Vector3.up, out hit, Mathf.Infinity, groundMask)) {
			currentGroundHigh = Mathf.Max(currentGroundHigh,hit.point.y);
		}

		Vector3 rot = transform.eulerAngles;
		rot.x = ISMath.WrapAngle(rot.x);
		rot.y = ISMath.WrapAngle(rot.y);
		wantYAngle = rot.y;

		objectPos.y = scrollHigh.Evaluate(scrollValue);
		wantXAngle = scrollXAngle.Evaluate(scrollValue);

		Quaternion targetRot = Quaternion.Euler(wantXAngle,wantYAngle,0f);
		transform.rotation = targetRot;

		float dist = objectPos.y * Mathf.Tan((90f - wantXAngle) * Mathf.Deg2Rad);   

		Vector3 cameraPosDir = targetRot * (Vector3.forward * dist);

		Vector3 cameraPos = objectPos - cameraPosDir;
		cameraPos.y = objectPos.y+currentGroundHigh;

		transform.position = cameraPos;
	}

	void Awake () {
		self = this;
		selfT = transform;
		groundMask = 1<<groundLayer;
	}

	public void Start(){
		objectPos = CalculateCurrentObjectPosition();
		scrollValue = Mathf.Clamp01(scrollValue);

		Vector3 rot = selfT.eulerAngles;
		rot.x = ISMath.WrapAngle(rot.x);
		rot.y = ISMath.WrapAngle(rot.y);
		wantYAngle = rot.y;
		rot.x = scrollXAngle.Evaluate(scrollValue);
		wantXAngle = rot.x;
		selfT.eulerAngles = rot;

		KeyboardControl (keyBoardControl);
		MouseControl (mouseControl);
		TouchControl (touchControl);

	}

	void LateUpdate(){
		Vector3 cameraPosDir;
		Vector3 cameraPos;

		if(!fixedPoint){
			float currentGroundHigh = groundHigh;

			//Set wanted position to target's position if we are following something.
			if(followingTarget){
				objectPos.x = followingTarget.position.x;
				objectPos.z = followingTarget.position.z;
			}

			//Calculate vertical distance to ground to avoid intercepting ground.
			RaycastHit hit;
			Vector3 emitPos = objectPos;
			emitPos.y += 9999f;
			if (groundHighTest && Physics.Raycast (emitPos, -Vector3.up, out hit, Mathf.Infinity, groundMask)) {
				currentGroundHigh = hit.point.y;
			}

			emitPos = selfT.position;
			emitPos.y += 9999f;
			if (groundHighTest && Physics.Raycast (emitPos, -Vector3.up, out hit, Mathf.Infinity, groundMask)) {
				currentGroundHigh = Mathf.Max(currentGroundHigh,hit.point.y);
			}

			//Lerp actual rotation to wanted value.
			Quaternion targetRot = Quaternion.Euler(wantXAngle,wantYAngle,0f);
			selfT.rotation = Quaternion.Slerp (selfT.rotation, targetRot, rotationLerpSpeed * Time.deltaTime);

			//Calculate a world position refers to the center of screen.
			float dist = objectPos.y * Mathf.Tan((90f - wantXAngle) * Mathf.Deg2Rad);

			//Use this vector to move camera back and rotate.
			Quaternion targetYRot = Quaternion.Euler(0f, wantYAngle, 0f);
			cameraPosDir = targetYRot * (Vector3.forward * dist);

			//Calculate the actual world position to prepare to move our camera object.
			cameraPos = objectPos - cameraPosDir;
			cameraPos.y = (objectPos.y + (followingTarget ? followingTarget.position.y : currentGroundHigh));

			//Append Look up position and reset it.
			cameraPos += lookUpPos;

			//Lerp to wanted position.
			selfT.position = Vector3.Lerp(selfT.position,cameraPos,movementLerpSpeed*Time.deltaTime);
		}
		else{
			//If we are positioning to a fixed point, we simply move to it.
			selfT.rotation = Quaternion.Slerp(selfT.rotation,fixedPoint.rotation,rotationLerpSpeed*Time.deltaTime);
			selfT.position = Vector3.Lerp(selfT.position,fixedPoint.position,movementLerpSpeed*Time.deltaTime);

			//We also keep objectPos to fixedPoint to make a stable feeling while leave fixed point mode.
			objectPos.x = fixedPoint.position.x;
			objectPos.z = fixedPoint.position.z;
		}
	}

	IEnumerator UpdateKeyboardControl(){
		while (true) {
			if(!keyBoardControl) break;

			Rotate (Input.GetAxisRaw (rotateAxis) * desktopRotateSpeed * Time.deltaTime);

			yield return null;
		}
	}

	IEnumerator UpdateMouseControl(){
		while (true) {
			
			if(!mouseControl) break;

			if(Input.GetMouseButton(mouseRotateButton)){
				Rotate (Input.GetAxis ("Mouse X") * desktopRotateSpeed);
			}

			Scroll (Input.GetAxis ("Mouse ScrollWheel") * desktopScrollSpeed);

			if (mouseLookUp) {
				MouseLookUp (Input.mousePosition);
			}

			yield return null;
		}
	}

	IEnumerator UpdateTouchControl(){
		while (true) {
			if(ISTouchControlHandler.currentScaleValue != 0) Scroll(ISTouchControlHandler.currentScaleValue*touchScrollSpeed);
			if(ISTouchControlHandler.currentRotateValue != 0) Rotate(ISTouchControlHandler.currentRotateValue*touchRotateSpeed);
			yield return null;
		}
	}

	public void MouseLookUp(Vector3 mouse){

		float rangeX = ISMath.Map (mouse.x, new ISRange (0, Screen.width), new ISRange (-1, 1));
		float rangeY = ISMath.Map (mouse.y, new ISRange (0, Screen.height), new ISRange (-1, 1));

		float offsetX = rangeX * mouseLookUpRange * Mathf.Min(1, mouseLookUpCurve.Evaluate (Mathf.Abs(rangeX)));
		float offsetY = rangeY * mouseLookUpRange * Mathf.Min(1, mouseLookUpCurve.Evaluate (Mathf.Abs(rangeY)));

		lookUpPos = new Vector3 (offsetX, 0, offsetY);

		Quaternion wantedRot = Quaternion.Euler (0, transform.eulerAngles.y, 0);

		lookUpPos = wantedRot * lookUpPos;
	}

	public void Rotate(float dir){
		wantYAngle += dir;
		ISMath.WrapAngle(wantYAngle);
	}

	public void Scroll(float value){
		scrollValue += value;
		scrollValue = Mathf.Clamp01(scrollValue);
		objectPos.y = scrollHigh.Evaluate(scrollValue);
		wantXAngle = scrollXAngle.Evaluate(scrollValue);
	}

	void OnDrawGizmosSelected(){
		
		//Draw debug lines.
		Vector3 mp = transform.position;
		Gizmos.DrawLine(new Vector3(bound.xMin,mp.y,bound.yMin),new Vector3(bound.xMin,mp.y,bound.yMax));
		Gizmos.DrawLine(new Vector3(bound.xMin,mp.y,bound.yMax),new Vector3(bound.xMax,mp.y,bound.yMax));
		Gizmos.DrawLine(new Vector3(bound.xMax,mp.y,bound.yMax),new Vector3(bound.xMax,mp.y,bound.yMin));
		Gizmos.DrawLine(new Vector3(bound.xMax,mp.y,bound.yMin),new Vector3(bound.xMin,mp.y,bound.yMin));
	}
}

public enum ISTopDownCScrollAnimationType{
	Simple, Advanced
}