﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(IntervalRangeAttribute))]
public class IntervalRangeDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + 16;
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.serializedObject.isEditingMultipleObjects) return;

        // Now draw the property as a Slider or an IntSlider based on whether it’s a float or integer.
        if (property.type != "Range")
            Debug.LogWarning("Use only with Range type");
        else
        {
            var range = this.attribute as IntervalRangeAttribute;
            if (range == null) return;

            var minValue = property.FindPropertyRelative("Start");
            var maxValue = property.FindPropertyRelative("End");
            float newMin = minValue.floatValue;
            float newMax = maxValue.floatValue;

            float controlPosition = position.x + EditorGUIUtility.labelWidth;
            float controlWidth = position.width - EditorGUIUtility.labelWidth;
            float singleBoxWidth = controlWidth * 0.5f - 15f;
            float controlLineHeight = position.height * 0.5f;

            EditorGUI.LabelField(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, controlLineHeight), label);

            EditorGUI.LabelField(
                new Rect(position.x, position.y + controlLineHeight, 24f, controlLineHeight),
                range.MinLimit.ToString("0.##"));
            EditorGUI.LabelField(
                new Rect(position.x + position.width - 24f, position.y + controlLineHeight, 24f, controlLineHeight),
                new GUIContent(range.MaxLimit.ToString("0.##")),
                new GUIStyle { alignment = TextAnchor.MiddleRight });
            EditorGUI.MinMaxSlider(
                new Rect(position.x + 24f, position.y + controlLineHeight, position.width - 48f, controlLineHeight), 
                ref newMin, ref newMax, range.MinLimit, range.MaxLimit);

            newMin = Mathf.Clamp(
                EditorGUI.FloatField(new Rect(controlPosition, position.y, singleBoxWidth, controlLineHeight), newMin),
                range.MinLimit, newMax);
            EditorGUI.LabelField(
                new Rect(controlPosition + singleBoxWidth, position.y, 30f, controlLineHeight),
                new GUIContent("->"),
                new GUIStyle { alignment = TextAnchor.MiddleCenter });
            newMax = Mathf.Clamp(
                EditorGUI.FloatField(new Rect(controlPosition + singleBoxWidth + 30f, position.y, singleBoxWidth, controlLineHeight), newMax),
                newMin, range.MaxLimit);

            minValue.floatValue = newMin;
            maxValue.floatValue = newMax;
        }
    }
}