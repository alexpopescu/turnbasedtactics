﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ProbabilityDistribution))]
public class ProbabilityDistributionDrawer : PropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var mean = property.FindPropertyRelative("Mean");
        var stdDev = property.FindPropertyRelative("StdDev");
        float newMean = mean.floatValue;
        float newStdDev = stdDev.floatValue;

        float controlPosition = position.x + EditorGUIUtility.labelWidth;
        float controlWidth = position.width - EditorGUIUtility.labelWidth;
        float singleBoxWidth = controlWidth * 0.5f - 20f;
        float controlLineHeight = position.height;

        EditorGUI.LabelField(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, controlLineHeight), label);

        EditorGUI.LabelField(
            new Rect(controlPosition, position.y, 20f, controlLineHeight),
            new GUIContent("μ"),
            new GUIStyle { alignment = TextAnchor.MiddleCenter });
        newMean = EditorGUI.FloatField(new Rect(controlPosition + 20f, position.y, singleBoxWidth, controlLineHeight), newMean);
        EditorGUI.LabelField(
            new Rect(controlPosition + singleBoxWidth + 20f, position.y, 20f, controlLineHeight),
            new GUIContent("σ"),
            new GUIStyle { alignment = TextAnchor.MiddleCenter });
        newStdDev = EditorGUI.FloatField(new Rect(controlPosition + singleBoxWidth + 40f, position.y, singleBoxWidth, controlLineHeight), newStdDev);

        mean.floatValue = newMean;
        stdDev.floatValue = newStdDev;
    }
}