﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class LookAt : MonoBehaviour
{
    public Transform Head = null;
    public Vector3 LookAtTargetPosition;
    public float LookAtCoolTime = 0.2f;
    public float LookAtHeatTime = 0.2f;
    public bool Looking = true;

    private Vector3 _lookAtPosition;
    private Animator _animator;
    private float _lookAtWeight;

    void Start()
    {
        if (!this.Head)
        {
            Debug.LogError("No head transform - LookAt disabled");
            this.enabled = false;
            return;
        }
        this._animator = this.GetComponent<Animator>();
        this.LookAtTargetPosition = this.Head.position + this.transform.forward;
        this._lookAtPosition = this.LookAtTargetPosition;
    }

    void OnAnimatorIk()
    {
        this.LookAtTargetPosition.y = this.Head.position.y;
        float lookAtTargetWeight = this.Looking ? 1.0f : 0.0f;

        var curDir = this._lookAtPosition - this.Head.position;
        var futDir = this.LookAtTargetPosition - this.Head.position;

        curDir = Vector3.RotateTowards(curDir, futDir, 6.28f * Time.deltaTime, float.PositiveInfinity);
        this._lookAtPosition = this.Head.position + curDir;

        float blendTime = lookAtTargetWeight > this._lookAtWeight ? this.LookAtHeatTime : this.LookAtCoolTime;
        this._lookAtWeight = Mathf.MoveTowards(this._lookAtWeight, lookAtTargetWeight, Time.deltaTime / blendTime);
        this._animator.SetLookAtWeight(this._lookAtWeight, 0.2f, 0.5f, 0.7f, 0.5f);
        this._animator.SetLookAtPosition(this._lookAtPosition);
    }
}