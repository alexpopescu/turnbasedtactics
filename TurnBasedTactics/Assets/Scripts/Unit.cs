﻿using System;
using UnityEngine;

[Serializable]
public class Unit
{
    public string Name = "Nameless One";
    public string Description = string.Empty;
    public int MaxStrength = 5;
    public int Dexterity = 5;
    public float Range = 2f;
    public float MaxActionPoints = 15;
    public WeaponType WeaponType = WeaponType.None;
    public MeleeType MeleeType = MeleeType.Stab;

    public int CurrentStrength { get; private set; }
    public float CurrentActionPoints { get; private set; }
    public StrengthQualifier CurrentStrengthQualifier { get; private set; }

    private float _lowThreshold = 0.3f;
    private float _highThreshold = 0.3f;

    // Use this for initialization
    public Unit()
    {
        this.CurrentStrength = this.MaxStrength;
        this.UpdateQualifiers();
        this.ResetTurn();
    }

    public virtual void Hit(int points)
    {
        this.CurrentStrength -= points;
        this.UpdateQualifiers();
    }

    public virtual int GetHitForce()
    {
        return this.CurrentStrength;
    }

    public virtual bool IsDead
    {
        get { return this.CurrentStrength <= 0; }
    }

    private void UpdateQualifiers()
    {
        if (this.IsDead)
            this.CurrentStrengthQualifier = StrengthQualifier.Dead;
        else if (this.CurrentStrength == this.MaxStrength)
            this.CurrentStrengthQualifier = StrengthQualifier.Full;
        else if (this.CurrentStrength <= this.MaxStrength * this._lowThreshold)
            this.CurrentStrengthQualifier = StrengthQualifier.Low;
        else if (this.CurrentStrength >= this.MaxStrength * this._highThreshold)
            this.CurrentStrengthQualifier = StrengthQualifier.High;
    }

    public void ResetTurn()
    {
        this.CurrentActionPoints = this.MaxActionPoints;
    }
}
