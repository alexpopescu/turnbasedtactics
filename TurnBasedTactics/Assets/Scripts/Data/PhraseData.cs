﻿using System;
using System.Collections.Generic;
using System.Linq;
using Procedural.Algorithms.Improv;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class PhraseData : ISerializationCallbackReceiver
{
    [Serializable]
    public class TagSetDataRaw
    {
        public string[] Tags;
    }

    [Serializable]
    public class GroupDataRaw
    {
        public string Note;
        public TagSetDataRaw[] TagSets;
        public string[] Phrases;
    }

    [Serializable]
    public class SnippetDataRaw
    {
        public string Name;
        public GroupDataRaw[] Groups;
    }

    public SnippetDataRaw[] SnippetList;
    public Dictionary<string, Group[]> Snippets;

    public void OnBeforeSerialize()
    {
        throw new NotImplementedException();
    }

    public void OnAfterDeserialize()
    {
        this.Snippets = new Dictionary<string, Group[]>();
        foreach (var raw in this.SnippetList)
        {
            this.Snippets.Add(raw.Name, raw.Groups.Select(x =>
            {
                var tagSet = new TagSet(x.TagSets.Select(t => new Tag(t.Tags)).ToArray());
                return new Group(tagSet, x.Phrases);
            }).ToArray());
        }
    }

    public string Generate(string name, Model model)
    {
        var groups = this.Snippets[name].Where(x => x.Tags.Matches(model.Tags)).ToArray();
        if (groups.Length == 0)
        {
            Debug.LogError($"No sentence for model: {model}");
            return null;
        }
        var group = groups[Random.Range(0, groups.Length)];
        return group.Generate(this.Snippets, model);
    }
}
