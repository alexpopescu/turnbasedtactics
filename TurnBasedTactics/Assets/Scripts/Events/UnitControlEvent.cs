﻿using UnityEngine.Events;

public class UnitControlEvent : UnityEvent<UnitControl>
{
}
