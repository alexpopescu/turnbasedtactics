﻿using UnityEngine;
using UnityEngine.Events;

public class Vector3Event : UnityEvent<Vector3>
{
}
