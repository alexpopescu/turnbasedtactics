﻿public enum WeaponType
{
    None = 0,
    Pistol = 1,
    AssaultRifle01 = 2,
    AssultRifle02 = 3,
    Shotgun = 4,
    SniperRifle = 5,
    Rifle = 6,
    SubMachineGun = 7,
    Rpg = 8,
    MiniGun = 9,
    Grenades = 10,
    Bow = 11,
    Melee = 12
}
