﻿public enum MeleeType
{
    Stab = 0,
    OneHanded = 1,
    TwoHanded = 2
}
