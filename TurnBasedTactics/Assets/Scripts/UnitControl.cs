﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(LookAt))]
//[RequireComponent(typeof(Unit))]
public class UnitControl : MonoBehaviour
{
    public Projector Projector;

    public Unit Unit;
    public UnitControl Target { get; set; }
    public PlayerType PlayerType;

    protected bool IsSelected;
    protected NavMeshAgent Agent;
    protected Animator Animator;
    protected LookAt LookAt;

    private Vector2 _smoothDeltaPosition = Vector2.zero;
    private Vector2 _velocity = Vector2.zero;

    public virtual void Start()
    {
        this.Agent = this.GetComponent<NavMeshAgent>();
        this.Agent.updatePosition = false;

        this.Animator = this.GetComponent<Animator>();
        this.LookAt = this.GetComponent<LookAt>();

        if (this.Projector != null) this.Projector.enabled = false;

        this.Animator.SetInteger("WeaponType_int", (int)this.Unit.WeaponType);
        if (this.Unit.WeaponType == WeaponType.Melee)
            this.Animator.SetInteger("MeleeType_int", (int)this.Unit.MeleeType);
    }

    public virtual void Update()
    {
        var worldDeltaPosition = this.Agent.nextPosition - this.transform.position;

        // Map 'worldDeltaPosition' to local space
        float dx = Vector3.Dot(this.transform.right, worldDeltaPosition);
        float dy = Vector3.Dot(this.transform.forward, worldDeltaPosition);
        var deltaPosition = new Vector2(dx, dy);

        if (this.Target != null)
        {
            var distanceToTarget = this.Agent.destination - this.transform.position;
            if (distanceToTarget.magnitude <= this.Unit.Range)
            {
                this.Agent.destination = this.transform.position;
                this.Animator.SetTrigger("Attack_t");

                int hitForce = this.Unit.GetHitForce();
                this.Target.Hit(hitForce);
                if (this.Target.Unit.IsDead) this.Target = null;
            }
        }

        // Low-pass filter the deltaMove
        float smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
        this._smoothDeltaPosition = Vector2.Lerp(this._smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if time advances
        if (Time.deltaTime > 1e-5f)
            this._velocity = this._smoothDeltaPosition / Time.deltaTime;

        // Update animation parameters
        this.Animator.SetFloat("Speed_f", this._velocity.magnitude);

        this.LookAt.LookAtTargetPosition = this.Agent.steeringTarget + this.transform.forward;
    }

    void OnAnimatorMove()
    {
        // Update position to agent position
        this.transform.position = this.Agent.nextPosition;
    }

    public void SetSelected(bool isSelected)
    {
        this.IsSelected = isSelected;
        if (this.Projector != null) this.Projector.enabled = isSelected;
    }

    public void Hit(int hitForce)
    {
        this.Unit.Hit(hitForce);
        if (this.Unit.IsDead)
            this.Animator.SetTrigger("Death_t");
    }

    public void Move(Vector3 point)
    {
        this.Agent.destination = point;
    }
}
