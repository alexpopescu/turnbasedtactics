﻿namespace Gamygdala
{
    using System.Collections.Generic;

    internal class BeliefSet : List<Belief>
    {
        private readonly Dictionary<string, int> _index = new Dictionary<string,int>();

        public void Set(string name, float likelihood, string agent = null)
        {
            if (this._index.ContainsKey(name))
            {
                this[this._index[name]].Likelihood = likelihood;
                this[this._index[name]].Computed = false;
            }
            else
            {
                this._index.Add(name, this.Count);
                this.Add(new Belief(name, likelihood, agent));
            }
        }

        public void AffectsGoal(string belief, string goal, float valence)
        {
            if (!this._index.ContainsKey(belief)) return;
            this[this._index[belief]].AffectsGoal(goal, valence);
        }

    }
}
