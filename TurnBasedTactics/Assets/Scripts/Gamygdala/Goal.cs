﻿namespace Gamygdala
{
    using System;

    /// <summary>
    /// Represents a goal of an agent
    /// </summary>
    internal class Goal
    {
        private float _utility;
        private float _likelihood = float.NaN;
        private float _previousLikelihood = float.NaN;

        /// <summary>
        /// The name of the goal
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The owner of the goal (null for self)
        /// </summary>
        public string Owner { get; }

        /// <summary>
        /// The utility of the goal (positive for desirable goals, negative for undesirable goals)
        /// </summary>
        /// <remarks>For maintenance goals we will use the opposite with a negative reward</remarks>
        public float Utility
        {
            get => this._utility;
            set
            {
                if (!Util.IsWithinRange(value, -1, 1))
                    throw new ArgumentOutOfRangeException(nameof(value), string.Format("The Utility of a goal should have a value between -1 and 1. Value passed: {0}", value));
                this._utility = value;
            }
        }
        /// <summary>
        /// The likelihood of this goal succeeding
        /// </summary>
        public float Likelihood
        {
            get => this._likelihood;
            set
            {
                this._previousLikelihood = this._likelihood;
                this._likelihood = value;
            }
        }
        /// <summary>
        /// The difference in likelihood
        /// </summary>
        public float DeltaLikelihood
        {
            get
            {
                if (float.IsNaN(this._likelihood)) return float.NaN;
                if (float.IsNaN(this._previousLikelihood))
                {
                    //if (this.likelihood == 0.5) 
                    return this._likelihood; // -0.5f;
                    //else return 1 - this.likelihood;
                }
                return this._likelihood - this._previousLikelihood;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">The name of the goal</param>
        /// <param name="utility">The utility of the goal</param>
        /// <param name="owner">The owner of the goal, null if self</param>
        public Goal(string name, float utility, string owner = null)
        {
            this.Name = name;
            this.Utility = utility;
            this.Owner = owner;
        }
    }
}
