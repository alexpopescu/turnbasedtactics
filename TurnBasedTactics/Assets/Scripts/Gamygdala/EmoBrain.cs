﻿namespace Gamygdala
{
    using System.Collections.Generic;
    using System;
    using UnityEngine;

    /// <summary>
    /// Represents a function that can be used for logging
    /// </summary>
    /// <param name="message">The message to log</param>
    public delegate void Logger(string message);

    //public delegate float InternalDecayer(float value, float time, InternalOCCEmotionType type, float target);
    //public delegate float SocialDecayer(float value, float time, SocialOCCEmotionType type, float target);
    /// <summary>
    /// Represents the emotional part of the brain of an agent
    /// </summary>
    /// <remarks>This needs to be defined by the client</remarks>
    public class EmoBrain
    {
        /// <summary>
        /// The function used to log messages if any
        /// </summary>
        public static readonly Logger Logger = null;

        private bool _needsRecalculate;

        private readonly GoalSet _goals = new GoalSet();
        private readonly BeliefSet _beliefs = new BeliefSet();

        private readonly InternalOCC _defaultInternalMood;
        private readonly SocialOCC _defaultSocialMood;

        private readonly InternalOCC _internalMood;
        //SocialOCC socialMood;

        private readonly InternalOCC _emotionalState;
        private readonly Dictionary<string, SocialOCC> _emotionalRelationships = new Dictionary<string, SocialOCC>();

        private readonly Func<float, float, int, float, float> _internalDecayer;
        private readonly Func<float, float, int, float, float> _socialDecayer;

        private PAD _pad;

        private PAD PAD => this._pad ?? (this._pad = this.RecalculatePAD());

        /// <summary>
        /// The PAD pleasure level corresponding to the current emotional state
        /// </summary>
        public float Pleasure => this.PAD.Pleasure;

        /// <summary>
        /// The PAD arousal level corresponding to the current emotional state
        /// </summary>
        public float Arousal => this.PAD.Arousal;

        /// <summary>
        /// The PAD dominance level corresponding to the current emotional state
        /// </summary>
        public float Dominance => this.PAD.Dominance;

        /// <summary>
        /// The PAD exuberance temperament axis
        /// </summary>
        /// <remarks>Exuberant (+P+A+D) vs. Bored (-P-A-D)</remarks>
        public float Exuberance => this.PAD.Exuberance;

        /// <summary>
        /// The PAD dependency temperament axis
        /// </summary>
        /// <remarks>Dependent (+P+A-D) vs. Disdainful (-P-A+D)</remarks>
        public float Dependency => this.PAD.Dependency;

        /// <summary>
        /// The PAD relaxation temperament axis
        /// </summary>
        /// <remarks>Relaxed (+P-A+D) vs. Anxious (-P+A-D)</remarks>
        public float Relaxation => this.PAD.Relaxation;

        /// <summary>
        /// The PAD docility temperament axis
        /// </summary>
        /// <remarks>Docile (+P-A-D) vs. Hostile (-P+A+D)</remarks>
        public float Docility => this.PAD.Docility;

        /// <summary>
        /// The joy component
        /// </summary>
        public float Joy => this._emotionalState.Joy;

        /// <summary>
        /// The distress component
        /// </summary>
        public float Distress => this._emotionalState.Distress;

        /// <summary>
        /// The hope component
        /// </summary>
        public float Hope => this._emotionalState.Hope;

        /// <summary>
        /// The fear component
        /// </summary>
        public float Fear => this._emotionalState.Fear;

        /// <summary>
        /// The satisfaction component
        /// </summary>
        public float Satisfaction => this._emotionalState.Satisfaction;

        /// <summary>
        /// The fears confirmed component
        /// </summary>
        public float FearsConfirmed => this._emotionalState.FearsConfirmed;

        /// <summary>
        /// The disappointment component
        /// </summary>
        public float Disappointment => this._emotionalState.Disappointment;

        /// <summary>
        /// The relief component
        /// </summary>
        public float Relief => this._emotionalState.Relief;

        /// <summary>
        /// The anger level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Anger(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Anger;
        }
        /// <summary>
        /// The guilt level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Guilt(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Guilt;
        }
        /// <summary>
        /// The gratitude level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Gratitude(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Gratitude;
        }
        /// <summary>
        /// The gratification level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Gratification(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Gratification;
        }
        /// <summary>
        /// The "happy-for" level towards another agent
        /// </summary>
        /// <remarks>This happens when this agent feels happy for positive things happening to another agent</remarks>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float HappyFor(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].HappyFor;
        }
        /// <summary>
        /// The pity level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Pity(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Pity;
        }
        /// <summary>
        /// The gloating level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Gloating(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Gloating;
        }
        /// <summary>
        /// The resentment level towards another agent
        /// </summary>
        /// <param name="name">The name of the other agent</param>
        /// <returns>The intensity of the component</returns>
        public float Resentment(string name)
        {
            if (!this._emotionalRelationships.ContainsKey(name) || this._emotionalRelationships[name] == null) return float.NaN;
            return this._emotionalRelationships[name].Resentment;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="joy">The default level of joy</param>
        /// <param name="distress">The default level of distress</param>
        /// <param name="hope">The default level of hope</param>
        /// <param name="fear">The default level of fear</param>
        /// <param name="satisfaction">The default level of satisfaction</param>
        /// <param name="fearsConfirmed">The default level of fearsConfirmed</param>
        /// <param name="disappointment">The default level of disappointment</param>
        /// <param name="relief">The default level of relief</param>
        /// <param name="anger">The default level of anger</param>
        /// <param name="guilt">The default level of guilt</param>
        /// <param name="gratitude">The default level of gratitude</param>
        /// <param name="gratification">The default level of gratification</param>
        /// <param name="happyFor">The default level of happyFor</param>
        /// <param name="pity">The default level of pity</param>
        /// <param name="gloating">The default level of gloating</param>
        /// <param name="resentment">The default level of resentment</param>
        /// <param name="internalDecayer">The function to use to calculate the decay of internal emotions</param>
        /// <param name="socialDecayer">The function to use to calculate the decay of social emotions</param>
        public EmoBrain(float joy = 0, float distress = 0, float hope = 0, float fear = 0,
            float satisfaction = 0, float fearsConfirmed = 0, float disappointment = 0, float relief = 0,
            float anger = 0, float guilt = 0, float gratitude = 0, float gratification = 0,
            float happyFor = 0, float pity = 0, float gloating = 0, float resentment = 0,
            Func<float, float, int, float, float> internalDecayer = null,
            Func<float, float, int, float, float> socialDecayer = null)
        {
            this._defaultInternalMood = new InternalOCC
            {
                Joy = joy,
                Distress = distress,
                Hope = hope,
                Fear = fear,
                Satisfaction = satisfaction,
                FearsConfirmed = fearsConfirmed,
                Disappointment = disappointment,
                Relief = relief
            };
            this._defaultSocialMood = new SocialOCC
            {
                Anger = anger,
                Guilt = guilt,
                Gratitude = gratitude,
                Gratification = gratification,
                HappyFor = happyFor,
                Pity = pity,
                Gloating = gloating,
                Resentment = resentment
            };
            this._internalMood = this._defaultInternalMood.Clone();
            this._emotionalState = this._internalMood.Clone();
            this._internalDecayer = internalDecayer ?? InternalDecay;
            this._socialDecayer = socialDecayer ?? SocialDecay;
        }

        private static float InternalDecay(float oldValue, float time, int type, float target)
        {
            return oldValue - (oldValue - target) / 10f;
        }
        private static float SocialDecay(float oldValue, float time, int type, float target)
        {
            return oldValue - (oldValue - target) / 10f;
        }

        /// <summary>
        /// Add or Modify a goal in the goal set
        /// </summary>
        /// <param name="name">The new name of the goal (must uniquely identify the goal)</param>
        /// <param name="utility">The utility (importance) of this goal</param>
        /// <param name="owner">The owner of the goal, null if self</param>
        public void Goal(string name, float utility, string owner = null)
        {
            Log("adding goal: {0}, utility = {1}, owner = {2}", name, utility,
                string.IsNullOrEmpty(owner) ? "self" : owner);
            this._goals.Set(name, utility, owner);
            this._needsRecalculate = true;
        }

        /// <summary>
        /// Add or Modify a belief in the belief set
        /// </summary>
        /// <param name="name">The name of the belief (must uniquely identify the belief/event)</param>
        /// <param name="likelihood">The likelihood of this belief being true</param>
        /// <param name="agent">The agent responsible for this belief</param>
        public void Belief(string name, float likelihood, string agent = null)
        {
            Log("adding belief: {0}, likelihood = {1}{2}", name, likelihood, string.IsNullOrEmpty(agent) ? "" : string.Format(", agent = {0}", agent));
            this._beliefs.Set(name, likelihood, agent);
            this._needsRecalculate = true;
        }

        /// <summary>
        /// Updates the influence of a belief on a goal
        /// </summary>
        /// <param name="belief">The identifier of the belief</param>
        /// <param name="goal">The identifier of the goal</param>
        /// <param name="valence">The way it affects the goal (-1 = goal is blocked, 1 = goal is succeeding, everything in bewteen = variations)</param>
        public void AffectsGoal(string belief, string goal, float valence)
        {
            this._beliefs.AffectsGoal(belief, goal, valence);
            this._needsRecalculate = true;
        }

        private void Recalculate()
        {
            Log("Recalculating...");
            var emotions = new List<InternalOCC>();
            var socialEmotions = new Dictionary<string, List<SocialOCC>>();

            foreach (var b in this._beliefs)
            {
                if (b.Computed) continue;
                Log("\nrecalculating belief: {0}, likelihood = {1}", b.Name, b.Likelihood);
                b.Computed = true;
                //float timeWeight = (float)time / (float)this.beliefs.Count;
                foreach (var kvp in b.AffectedGoals)
                {
                    Log("affected goal: {0}, valence = {1}", kvp.Key, kvp.Value);
                    var goal = this._goals.Get(kvp.Key);
                    if (goal == null) continue;

                    float desirability = kvp.Value * goal.Utility;
                    if (desirability == 0) continue;
                    Log("desirability: {0} <- {1} x {2}", desirability, kvp.Value, goal.Utility);

                    float previousLikelihood = goal.Likelihood;
                    //if (g.Valence < 0)
                    //{
                    //    goal.Likelihood = 1 + g.Valence * b.Likelihood;
                    //    Log("goal likelihood: {0} <- 1 + ({1} x {2})", goal.Likelihood, g.Valence, b.Likelihood);
                    //}
                    //else
                    //{
                    goal.Likelihood = (kvp.Value * b.Likelihood + 1f) / 2f;
                    Log("goal likelihood: {0} <- ({1} x {2} + 1) / 2", goal.Likelihood, kvp.Value, b.Likelihood);
                    //}
                    float delta = goal.DeltaLikelihood;
                    if (float.IsNaN(previousLikelihood))
                        //if (goal.Likelihood >= 0.5f)
                        Log("delta goal likelihood: {0} <- {1}", delta, goal.Likelihood);
                    //else Log("delta goal likelihood: {0} <- 1 - {1}", delta, goal.Likelihood);
                    else Log("delta goal likelihood: {0} <- {2} - {1}", delta, previousLikelihood, goal.Likelihood);

                    // special case
                    if (float.IsNaN(previousLikelihood) && goal.Likelihood == 0)
                    {
                        delta = desirability;
                        Log("special case, the goal is directly blocked, delta becomes the desirability: {0}", desirability);
                    }
                    if (delta == 0) continue;

                    float emotionIntensity = Math.Abs(desirability * delta);
                    Log("emotion intensity: {0} <- abs({1} x {2})", emotionIntensity, desirability, delta);

                    // check if the affected goal belongs to this agent
                    if (goal.Owner == null)
                    {
                        if ((Mathf.Approximately(b.Likelihood, 0f) && desirability < 0) || 
                            (Mathf.Approximately(b.Likelihood, 1f) && desirability > 0))
                        {
                            Log("adding JOY: {0}", emotionIntensity);
                            emotions.Add(InternalOCC.CreateSimpleJoy(emotionIntensity));
                        }
                        else if ((Mathf.Approximately(b.Likelihood, 0f) && desirability > 0) || 
                                 (Mathf.Approximately(b.Likelihood, 1f) && desirability < 0))
                        {
                            Log("adding DISTRESS: {0}", emotionIntensity);
                            emotions.Add(InternalOCC.CreateSimpleDistress(emotionIntensity));
                        }
                        else if ((delta < 0 && desirability < 0) || (delta > 0 && desirability > 0))
                        {
                            Log("adding HOPE: {0}", emotionIntensity);
                            emotions.Add(InternalOCC.CreateSimpleHope(emotionIntensity));
                        }
                        else if ((delta < 0 && desirability > 0) || (delta > 0 && desirability < 0))
                        {
                            Log("adding FEAR: {0}", emotionIntensity);
                            emotions.Add(InternalOCC.CreateSimpleFear(emotionIntensity));
                        }

                        // prospects (dis)confirm emotions make sense only if we are modifying the goal likelihood
                        if (!float.IsNaN(previousLikelihood))
                        {
                            //bool highDelta = Math.Abs(delta) > 0.5;
                            if (Mathf.Approximately(b.Likelihood, 0f) && desirability < 0) // && highDelta)
                            {
                                Log("adding RELIEF: {0}", emotionIntensity);
                                emotions.Add(InternalOCC.CreateSimpleRelief(emotionIntensity));
                            }
                            if (Mathf.Approximately(b.Likelihood, 0f) && desirability > 0) // && highDelta)
                            {
                                Log("adding DISAPPOINTMENT: {0}", emotionIntensity);
                                emotions.Add(InternalOCC.CreateSimpleDisappointment(emotionIntensity));
                            }
                            if (Mathf.Approximately(b.Likelihood, 1f) && desirability < 0) // && !highDelta)
                            {
                                Log("adding FEARS CONFIRMED: {0}", emotionIntensity);
                                emotions.Add(InternalOCC.CreateSimpleFearsConfirmed(emotionIntensity));
                            }
                            if (Mathf.Approximately(b.Likelihood, 1f) && desirability > 0) // && !highDelta)
                            {
                                Log("adding SATISFACTION: {0}", emotionIntensity);
                                emotions.Add(InternalOCC.CreateSimpleSatisfaction(emotionIntensity));
                            }
                        }

                        if (!string.IsNullOrEmpty(b.Agent)) // the agent is someone else
                        {
                            if (!socialEmotions.ContainsKey(b.Agent))
                                socialEmotions.Add(b.Agent, new List<SocialOCC>());
                            //float praiseworthiness = goal.Utility * delta;
                            if (goal.Utility * delta > 0)
                            {
                                Log("adding GRATITUDE: {0} towards {1}", emotionIntensity, b.Agent);
                                socialEmotions[b.Agent].Add(SocialOCC.CreateGratitudeEmotion(emotionIntensity));
                            }
                            if (goal.Utility * delta < 0)
                            {
                                Log("adding ANGER: {0} towards {1}", emotionIntensity, b.Agent);
                                socialEmotions[b.Agent].Add(SocialOCC.CreateAngerEmotion(emotionIntensity));
                            }
                        }
                    }
                    else // the owner of the goal is someone else
                    {
                        if (!socialEmotions.ContainsKey(goal.Owner))
                            socialEmotions.Add(goal.Owner, new List<SocialOCC>());

                        if (this._emotionalRelationships.ContainsKey(goal.Owner))
                        {
                            float deltaLike = this._emotionalRelationships[goal.Owner].Like;
                            float socialIntensity = emotionIntensity;// Math.Abs(emotionIntensity * deltaLike);
                            //Log("social intensity: {0} <- abs({1} x {2})", socialIntensity, emotionIntensity, deltaLike);

                            if (b.Agent == "self" && deltaLike > 0) // the agent is self
                            {
                                if (desirability > 0)
                                {
                                    Log("adding GRATIFICATION: {0} towards self", socialIntensity);
                                    socialEmotions[goal.Owner].Add(SocialOCC.CreateGratificationEmotion(socialIntensity));
                                }
                                else
                                {
                                    Log("adding GUILT: {0} towards {1}", socialIntensity, goal.Owner);
                                    socialEmotions[goal.Owner].Add(SocialOCC.CreateGuiltEmotion(socialIntensity));
                                }
                            }

                            if (desirability > 0 && deltaLike > 0)
                            {
                                Log("adding HAPPY-FOR: {0} towards {1}", socialIntensity, goal.Owner);
                                socialEmotions[goal.Owner].Add(SocialOCC.CreateHappyForEmotion(socialIntensity));
                            }
                            else if (desirability < 0 && deltaLike > 0)
                            {
                                Log("adding PITY: {0} towards {1}", socialIntensity, goal.Owner);
                                socialEmotions[goal.Owner].Add(SocialOCC.CreatePityEmotion(socialIntensity));
                            }
                            else if (desirability < 0 && deltaLike < 0)
                            {
                                Log("adding GLOATING: {0} towards {1}", socialIntensity, goal.Owner);
                                socialEmotions[goal.Owner].Add(SocialOCC.CreateGloatingEmotion(socialIntensity));
                            }
                            else if (desirability > 0 && deltaLike < 0)
                            {
                                Log("adding RESENTMENT: {0} towards {1}", socialIntensity, goal.Owner);
                                socialEmotions[goal.Owner].Add(SocialOCC.CreateResentmentEmotion(socialIntensity));
                            }
                        }
                    }
                }
            }

            this._emotionalState.Combine(emotions);
            foreach (var kvp in socialEmotions)
            {
                if (!this._emotionalRelationships.ContainsKey(kvp.Key))
                    this._emotionalRelationships.Add(kvp.Key, new SocialOCC());
                this._emotionalRelationships[kvp.Key].Combine(kvp.Value);
            }
            if (emotions.Count > 0 || socialEmotions.Count > 0) this._pad = null;
        }

        /// <summary>
        /// Updates the intensities of emotions over time
        /// </summary>
        /// <param name="gameTime">The time elapsed since last update</param>
        public void Update(float gameTime = 0f)
        {
            Log("Decaying...");
            bool decayed = false;
            decayed |= this._emotionalState.Decay(this._internalDecayer, gameTime, this._internalMood);
            decayed |= this._internalMood.Decay(this._internalDecayer, gameTime, this._defaultInternalMood);
            foreach (var kvp in this._emotionalRelationships)
                decayed |= this._emotionalRelationships[kvp.Key].Decay(this._socialDecayer, gameTime, this._defaultSocialMood);
            Log("Updating...");
            if (this._needsRecalculate) this.Recalculate();
            if (decayed) this._pad = null;
        }

        /// <summary>
        /// Returns a string representation of the current state
        /// </summary>
        /// <returns>The string representation</returns>
        public override string ToString()
        {
            string ret = string.Format("\nCurrent Internal State: {0}", this._emotionalState);
            if (this._emotionalRelationships.Count > 0)
            {
                ret += "\nRelationships:";
                foreach (var kvp in this._emotionalRelationships)
                    ret += string.Format("\n\t{0} -> {1}", kvp.Key, kvp.Value);
            }
            return ret;
        }

        private static void Log(string format, params object[] arguments)
        {
            Logger?.Invoke(string.Format(format, arguments));
        }

        private PAD RecalculatePAD()
        {
            return this._emotionalState.PAD;
        }
    }
}
