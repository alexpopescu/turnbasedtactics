﻿namespace Gamygdala
{
    using System.Collections.Generic;
    using System;

    /// <summary>
    /// Represents a belief of an agent
    /// </summary>
    public class Belief
    {
        #region Private Fields

        private float _likelihood;

        #endregion

        #region Public Properties
        /// <summary>
        /// The name of this belief (just a string to identify it)
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The likelihood of this belief being true
        /// </summary>
        public float Likelihood
        {
            get => this._likelihood;
            set
            {
                if (!Util.IsWithinRange(value, 0, 1))
                    throw new ArgumentOutOfRangeException(nameof(value), string.Format("This is a probability and should have a value between 0 and 1. Value passed: {0}", value));
                this._likelihood = value;
            }
        }
        /// <summary>
        /// The agent responsible for this event (belief)
        /// </summary>
        /// <value>null for self, empty string if no agent is responsible</value>
        public string Agent { get; }

        /// <summary>
        /// The list of goals affected by this belief
        /// </summary>
        public Dictionary<string, float> AffectedGoals { get; } = new Dictionary<string, float>();

        /// <summary>
        /// Whether this belief has been computed by the brain
        /// </summary>
        public bool Computed { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">The name (unique identifier) of this belief</param>
        /// <param name="likelihood">The likelihood of this belief being true</param>
        /// <param name="causalAgent">The agent responsible for this event</param>
        public Belief(string name, float likelihood, string causalAgent = null)
        {
            this.Name = name;
            this.Likelihood = likelihood;
            this.Agent = causalAgent;
        }
        #endregion

        #region Affected Goals Management
        /// <summary>
        /// Defines the way a goal is affected by this belief
        /// </summary>
        /// <param name="name">The name of the goal</param>
        /// <param name="valence">The way it affects the goal (-1 = goal is blocked, 1 = goal is succeeding, everything in between = variations)</param>
        public void AffectsGoal(string name, float valence)
        {
            if (this.AffectedGoals.ContainsKey(name))
            {
                this.AffectedGoals[name] = valence;
            }
            else
            {
                this.AffectedGoals.Add(name, valence);
            }
        }
        #endregion
    }
}
