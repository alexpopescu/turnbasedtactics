﻿namespace Gamygdala
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents the social part of an OCC emotion
    /// </summary>
    internal class SocialOCC : BaseEmotion
    {
        #region Emotion Indexes

        private const int ANGER_INDEX = 0;
        private const int GUILT_INDEX = 1;
        private const int GRATITUDE_INDEX = 2;
        private const int GRATIFICATION_INDEX = 3;
        private const int HAPPY_FOR_INDEX = 4;
        private const int PITY_INDEX = 5;
        private const int GLOATING_INDEX = 6;
        private const int RESENTMENT_INDEX = 7;

        #endregion

        private float? _like;

        protected override string EmotionName(int emotionIndex)
        {
            switch (emotionIndex)
            {
                case ANGER_INDEX:
                    return "Anger";
                case GUILT_INDEX:
                    return "Guilt";
                case GRATITUDE_INDEX:
                    return "Gratitude";
                case GRATIFICATION_INDEX:
                    return "Gratification";
                case HAPPY_FOR_INDEX:
                    return "HappyFor";
                case PITY_INDEX:
                    return "Pity";
                case GLOATING_INDEX:
                    return "Gloating";
                case RESENTMENT_INDEX:
                    return "Resentment";
                default: 
                    return string.Empty;
            }
        }

        #region Getters

        /// <summary>
        /// Gets the intensity of the Anger component
        /// </summary>
        public float Anger
        {
            get => this.Get(ANGER_INDEX);
            set => this.Set(ANGER_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Guilt component
        /// </summary>
        public float Guilt
        {
            get => this.Get(GUILT_INDEX);
            set => this.Set(GUILT_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Gratitude component
        /// </summary>
        public float Gratitude
        {
            get => this.Get(GRATITUDE_INDEX);
            set => this.Set(GRATITUDE_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Gratification component
        /// </summary>
        public float Gratification
        {
            get => this.Get(GRATIFICATION_INDEX);
            set => this.Set(GRATIFICATION_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Happy-For component
        /// </summary>
        public float HappyFor
        {
            get => this.Get(HAPPY_FOR_INDEX);
            set => this.Set(HAPPY_FOR_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Pity component
        /// </summary>
        public float Pity
        {
            get => this.Get(PITY_INDEX);
            set => this.Set(PITY_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Gloating component
        /// </summary>
        public float Gloating
        {
            get => this.Get(GLOATING_INDEX);
            set => this.Set(GLOATING_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Resentment component
        /// </summary>
        public float Resentment
        {
            get => this.Get(RESENTMENT_INDEX);
            set => this.Set(RESENTMENT_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the social relationship
        /// </summary>
        public float Like
        {
            get
            {
                if (!this._like.HasValue) this.RecalculateLike();
                return this._like ?? float.NaN;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public SocialOCC()
            : base(8)
        {
            this.Anger = 0f;
            this.Guilt = 0f;
            this.Gratitude = 0f;
            this.Gratification = 0f;
            this.HappyFor = 0f;
            this.Pity = 0f;
            this.Gloating = 0f;
            this.Resentment = 0f;
        }

        /// <summary>
        /// Copies the emotion into a new object
        /// </summary>
        /// <returns>A cloned emotion object</returns>
        public SocialOCC Clone()
        {
            return new SocialOCC
            {
                Anger = this.Anger,
                Guilt = this.Guilt,
                Gratitude = this.Gratitude,
                Gratification = this.Gratification,
                HappyFor = this.HappyFor,
                Pity = this.Pity,
                Gloating = this.Gloating,
                Resentment = this.Resentment
            };
        }

        /// <summary>
        /// Combines a list of emotions into the current emotion
        /// </summary>
        /// <param name="others">The emotions to incorporate</param>
        public void Combine(List<SocialOCC> others)
        {
            base.Combine(others.ConvertAll(x => (BaseEmotion) x));
            this._like = null;
        }

        #region Factory methods

        /// <summary>
        /// Creates a simple Anger emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateAngerEmotion(float intensity)
        {
            return new SocialOCC {Anger = intensity};
        }

        /// <summary>
        /// Creates a simple Guilt emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateGuiltEmotion(float intensity)
        {
            return new SocialOCC {Guilt = intensity};
        }

        /// <summary>
        /// Creates a simple Gratitude emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateGratitudeEmotion(float intensity)
        {
            return new SocialOCC {Gratitude = intensity};
        }

        /// <summary>
        /// Creates a simple Gratification emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateGratificationEmotion(float intensity)
        {
            return new SocialOCC {Gratification = intensity};
        }

        /// <summary>
        /// Creates a simple Happy-For emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateHappyForEmotion(float intensity)
        {
            return new SocialOCC {HappyFor = intensity};
        }

        /// <summary>
        /// Creates a simple Pity emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreatePityEmotion(float intensity)
        {
            return new SocialOCC {Pity = intensity};
        }

        /// <summary>
        /// Creates a simple Gloating emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateGloatingEmotion(float intensity)
        {
            return new SocialOCC {Gloating = intensity};
        }

        /// <summary>
        /// Creates a simple Resentment emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>A social emotion object</returns>
        public static SocialOCC CreateResentmentEmotion(float intensity)
        {
            return new SocialOCC {Resentment = intensity};
        }

        #endregion

        private void RecalculateLike()
        {
            this._like = this.Gratitude - this.Anger;
        }
    }
}