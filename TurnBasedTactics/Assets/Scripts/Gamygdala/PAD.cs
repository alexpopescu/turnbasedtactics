﻿namespace Gamygdala
{
    /// <summary>
    /// Represents a PAD (pleasure, arousal, dominance) emotion
    /// </summary>
    internal class PAD
    {
        #region Getters
        /// <summary>
        /// The pleasure component
        /// </summary>
        public float Pleasure { get; internal set; }

        /// <summary>
        /// The arousal component
        /// </summary>
        public float Arousal { get; internal set; }

        /// <summary>
        /// The dominance component
        /// </summary>
        public float Dominance { get; internal set; }

        // Temperament model (See Mehrabian 1996 - A general framework for describing and measuring
        // individual differences in temperament
        /// <summary>
        /// Exuberant (+P+A+D) vs. Bored (-P-A-D)
        /// </summary>
        public float Exuberance => .577f * (this.Pleasure + this.Arousal + this.Dominance);

        /// <summary>
        /// Dependent (+P+A-D) vs. Disdainful (-P-A+D)
        /// </summary>
        public float Dependency => .577f * (this.Pleasure + this.Arousal - this.Dominance);

        /// <summary>
        /// Relaxed (+P-A+D) vs. Anxious (-P+A-D)
        /// </summary>
        public float Relaxation => .577f * (this.Pleasure - this.Arousal + this.Dominance);

        /// <summary>
        /// Docile (+P-A-D) vs. Hostile (-P+A+D)
        /// </summary>
        public float Docility => .577f * (this.Pleasure - this.Arousal - this.Dominance);

        #endregion

        /// <summary>
        /// The Neutral Emotion
        /// </summary>
        public static PAD Zero => new PAD(0, 0, 0);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="pleasure">The pleasure level</param>
        /// <param name="arousal">The arousal level</param>
        /// <param name="dominance">The dominance level</param>
        public PAD(float pleasure, float arousal, float dominance)
        {
            this.Pleasure = pleasure;
            this.Arousal = arousal;
            this.Dominance = dominance;
        }

        /// <summary>
        /// Adds the components of two PAD emotions together
        /// </summary>
        /// <param name="p1">The first emotion</param>
        /// <param name="p2">The second emotion</param>
        /// <returns></returns>
        public static PAD operator +(PAD p1, PAD p2)
        {
            return new PAD(p1.Pleasure + p2.Pleasure, p1.Arousal + p2.Arousal, p1.Dominance + p2.Dominance);
        }

        /// <summary>
        /// Scales an emotion up or down
        /// </summary>
        /// <param name="p1">The emotion object</param>
        /// <param name="val">The scale</param>
        /// <returns></returns>
        public static PAD operator *(PAD p1, float val)
        {
            return new PAD(p1.Pleasure * val, p1.Arousal * val, p1.Dominance * val);
        }

        /// <summary>
        /// Returns the string representation of a PAD emotion
        /// </summary>
        /// <returns>The string representation</returns>
        public override string ToString()
        {
            return string.Format("{0}P,{1}A,{2}D", this.Pleasure, this.Arousal, this.Dominance);
        }
    }
}
