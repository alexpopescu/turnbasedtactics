﻿namespace Gamygdala
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents the internal part of an OCC emotion
    /// </summary>
    internal class InternalOCC : BaseEmotion
    {
        #region Emotion Indexes

        private const int JOY_INDEX = 0;
        private const int DISTRESS_INDEX = 1;
        private const int HOPE_INDEX = 2;
        private const int FEAR_INDEX = 3;
        private const int SATISFACTION_INDEX = 4;
        private const int FEARS_CONFIRMED_INDEX = 5;
        private const int DISAPPOINTMENT_INDEX = 6;
        private const int RELIEF_INDEX = 7;

        #endregion

        protected override string EmotionName(int emotionIndex)
        {
            switch (emotionIndex)
            {
                case JOY_INDEX:
                    return "Joy";
                case DISTRESS_INDEX:
                    return "Distress";
                case HOPE_INDEX:
                    return "Hope";
                case FEAR_INDEX:
                    return "Fear";
                case SATISFACTION_INDEX:
                    return "Satisfaction";
                case FEARS_CONFIRMED_INDEX:
                    return "FearsConfirmed";
                case DISAPPOINTMENT_INDEX:
                    return "Disappointment";
                case RELIEF_INDEX:
                    return "Relief";
                default:
                    return string.Empty;
            }
        }

        #region Getters

        /// <summary>
        /// Gets the intensity of the Joy component
        /// </summary>
        public float Joy
        {
            get => this.Get(JOY_INDEX);
            set => this.Set(JOY_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Distress component
        /// </summary>
        public float Distress
        {
            get => this.Get(DISTRESS_INDEX);
            set => this.Set(DISTRESS_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Hope component
        /// </summary>
        public float Hope
        {
            get => this.Get(HOPE_INDEX);
            set => this.Set(HOPE_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Fear component
        /// </summary>
        public float Fear
        {
            get => this.Get(FEAR_INDEX);
            set => this.Set(FEAR_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Satisfaction component
        /// </summary>
        public float Satisfaction
        {
            get => this.Get(SATISFACTION_INDEX);
            set => this.Set(SATISFACTION_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Fears-Confirmed component
        /// </summary>
        public float FearsConfirmed
        {
            get => this.Get(FEARS_CONFIRMED_INDEX);
            set => this.Set(FEARS_CONFIRMED_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Disappointment component
        /// </summary>
        public float Disappointment
        {
            get => this.Get(DISAPPOINTMENT_INDEX);
            set => this.Set(DISAPPOINTMENT_INDEX, value);
        }

        /// <summary>
        /// Gets the intensity of the Relief component
        /// </summary>
        public float Relief
        {
            get => this.Get(RELIEF_INDEX);
            set => this.Set(RELIEF_INDEX, value);
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public InternalOCC()
            : base(8)
        {
            this.Joy = 0f;
            this.Distress = 0f;
            this.Hope = 0f;
            this.Fear = 0f;
            this.Satisfaction = 0f;
            this.FearsConfirmed = 0f;
            this.Disappointment = 0f;
            this.Relief = 0f;
        }

        /// <summary>
        /// Copies the emotion into a new object
        /// </summary>
        /// <returns>A cloned emotion object</returns>
        public InternalOCC Clone()
        {
            var ret = new InternalOCC
            {
                Joy = this.Joy,
                Distress = this.Distress,
                Hope = this.Hope,
                Fear = this.Fear,
                Satisfaction = this.Satisfaction,
                FearsConfirmed = this.FearsConfirmed,
                Disappointment = this.Disappointment,
                Relief = this.Relief
            };
            return ret;
        }

        public void Combine(List<InternalOCC> others)
        {
            base.Combine(others.ConvertAll(x => (BaseEmotion) x));
        }

        #region Factory methods

        /// <summary>
        /// Creates a simple Joy emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleJoy(float intensity)
        {
            return new InternalOCC {Joy = intensity};
        }

        /// <summary>
        /// Creates a simple Distress emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleDistress(float intensity)
        {
            return new InternalOCC {Distress = intensity};
        }

        /// <summary>
        /// Creates a simple Hope emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleHope(float intensity)
        {
            return new InternalOCC {Hope = intensity};
        }

        /// <summary>
        /// Creates a simple Fear emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleFear(float intensity)
        {
            return new InternalOCC {Fear = intensity};
        }

        /// <summary>
        /// Creates a simple Satisfaction emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleSatisfaction(float intensity)
        {
            return new InternalOCC {Satisfaction = intensity};
        }

        /// <summary>
        /// Creates a simple Fears Confirmed emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleFearsConfirmed(float intensity)
        {
            return new InternalOCC {FearsConfirmed = intensity};
        }

        /// <summary>
        /// Creates a simple Disappointment emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleDisappointment(float intensity)
        {
            return new InternalOCC {Disappointment = intensity};
        }

        /// <summary>
        /// Creates a simple Relief emotion
        /// </summary>
        /// <param name="intensity">The given intensity</param>
        /// <returns>An internal emotion object</returns>
        public static InternalOCC CreateSimpleRelief(float intensity)
        {
            return new InternalOCC {Relief = intensity};
        }

        #endregion

        // TODO: add the missing representations
        /// <summary>
        /// Get the PAD representation of an emotion
        /// </summary>
        /// <param name="emotion">The emotion index</param>
        /// <returns>A PAD object</returns>
        protected override PAD GetPADByEmotion(int emotion)
        {
            switch (emotion)
            {
                case HOPE_INDEX: return new PAD(.61f, .51f, .26f); // copied from joy, less pleasure, less dominance
                case FEAR_INDEX: return new PAD(-.64f, .60f, -.43f);
                case JOY_INDEX: return new PAD(.81f, .51f, .46f);
                case DISTRESS_INDEX: return new PAD(-.61f, .28f, -.36f);
                case SATISFACTION_INDEX:
                    break;
                case FEARS_CONFIRMED_INDEX: return new PAD(-.61f, .06f, -.32f);
                case DISAPPOINTMENT_INDEX:
                    break;
                case RELIEF_INDEX:
                    break;
            }

            return null;
        }
    }
}