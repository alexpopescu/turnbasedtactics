﻿namespace Gamygdala
{
    using System.Collections.Generic;

    /// <summary>
    /// A collection of goals
    /// </summary>
    internal class GoalSet : List<Goal>
    {
        private readonly Dictionary<string, int> _index = new Dictionary<string,int>();

        /// <summary>
        /// Add or Update a goal into the collection
        /// </summary>
        /// <seealso cref="Goal"/>
        /// <param name="name">The name of the goal</param>
        /// <param name="utility">The utility value of the goal</param>
        /// <param name="owner">The owner of the goal, null if self</param>
        public void Set(string name, float utility, string owner = null)
        {
            if (this._index.ContainsKey(name))
            {
                if (utility == 0)
                {
                    this.RemoveAt(this._index[name]);
                    this._index.Remove(name);
                }
                this[this._index[name]].Utility = utility;
            }
            else
            {
                this._index.Add(name, this.Count);
                this.Add(new Goal(name, utility, owner));
            }
        }

        /// <summary>
        /// Get a goal by name
        /// </summary>
        /// <param name="name">The name of the goal</param>
        /// <returns>A Goal object</returns>
        public Goal Get(string name)
        {
            return this._index.ContainsKey(name) ? this[this._index[name]] : null;
        }
    }
}
