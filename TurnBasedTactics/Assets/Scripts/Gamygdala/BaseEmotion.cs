﻿namespace Gamygdala
{
    using System.Collections.Generic;
    using System;
    using System.Text;
    using UnityEngine;

    internal abstract class BaseEmotion
    {
        private readonly float[] _intensity;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size">The number of different emotions included in the object</param>
        protected BaseEmotion(int size)
        {
            this._intensity = new float[size];
        }

        protected float Get(int emotionIndex)
        {
            return this._intensity[emotionIndex];
        }
        protected void Set(int emotionIndex, float value)
        {
            this._intensity[emotionIndex] = value;
        }

        /// <summary>
        /// Combines multiple emotions into one
        /// </summary>
        /// <param name="others">The list of emotions to combine</param>
        /// <remarks>Here we use Reilly's logarithmic function to normalize</remarks>
        protected void Combine(List<BaseEmotion> others)
        {
            if (this._intensity == null || this._intensity.Length == 0 || others == null || others.Count == 0) return;
            for (int i = 0; i < this._intensity.Length; i++)
            {
                float sum = 0;
                int howMany = 0;
                float lastNonNull = 0f;
                if (this._intensity[i] > 0)
                {
                    sum = (float)Math.Pow(2, 10 * this._intensity[i]);
                    howMany++;
                    lastNonNull = this._intensity[i];
                }
                foreach (var t in others)
                {
                    float val = t.Get(i);
                    if (val == 0) continue;
                    sum += (float)Math.Pow(2, 10 * val);
                    howMany++;
                    if (lastNonNull == 0f) lastNonNull = val;
                }
                if (howMany == 0) continue;
                if (howMany == 1)
                {
                    this.Set(i, lastNonNull);
                    continue;
                }
                this.Set(i, 0.1f * (float)Math.Log(sum, 2));
            }
        }

        /// <summary>
        /// Decays the current emotion
        /// </summary>
        /// <param name="decayer">A function that acts as a decayer</param>
        /// <param name="time">The current time</param>
        /// <param name="target">The default emotion</param>
        /// <returns></returns>
        public bool Decay(Func<float, float, int, float, float> decayer, float time, BaseEmotion target)
        {
            //List<EmotionTypes> keys = new List<EmotionTypes>(this.intensity.Keys);
            if (this._intensity == null || this._intensity.Length == 0) return false;

            bool ret = false;
            for (int i = 0; i < this._intensity.Length; i++)
            {
                float targetVal = target.Get(i);
                float currentVal = this._intensity[i];
                if (Mathf.Approximately(currentVal, targetVal)) continue;
                this._intensity[i] = decayer(currentVal, time, i, targetVal);
                ret = true;
            }
            return ret;
        }

        /// <summary>
        /// Get the PAD representation of an emotion
        /// </summary>
        /// <param name="emotionIndex">The index of the emotion to transform</param>
        /// <returns>A PAD representation</returns>
        protected virtual PAD GetPADByEmotion(int emotionIndex) { return null; }

        public PAD PAD
        {
            get
            {
                var ret = PAD.Zero;
                if (this._intensity == null || this._intensity.Length == 0) return ret;
                for (int i = 0; i < this._intensity.Length; i++)
                {
                    if (this._intensity[i] == 0) continue;
                    var p = this.GetPADByEmotion(i);
                    if (p == null) continue;
                    p *= this._intensity[i];
                    ret.Pleasure += (float)Math.Pow(2, 10 * p.Pleasure);
                    ret.Arousal += (float)Math.Pow(2, 10 * p.Arousal);
                    ret.Dominance += (float)Math.Pow(2, 10 * p.Dominance);
                }
                if (ret.Pleasure != 0) ret.Pleasure = 0.1f * (float)Math.Log(ret.Pleasure, 2);
                if (ret.Arousal != 0) ret.Arousal = 0.1f * (float)Math.Log(ret.Arousal, 2);
                if (ret.Dominance != 0) ret.Dominance = 0.1f * (float)Math.Log(ret.Dominance, 2);
                return ret;
            }
        }

        /// <summary>
        /// Returns a string representation of this emotion
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this._intensity == null || this._intensity.Length == 0) return string.Empty;
            var sb = new StringBuilder();
            for (int i = 0; i < this._intensity.Length; i++)
                if (this._intensity[i] != 0)
                {
                    if (sb.Length > 0) sb.Append("; ");
                    sb.AppendFormat("{0}: {1:0.#0}", this.EmotionName(i), this._intensity[i]);
                }
            return sb.ToString();
        }

        protected virtual string EmotionName(int emotionIndex) { return string.Empty; }
    }
}
