﻿namespace Gamygdala
{
    using System;

    /// <summary>
    /// Various helper functions
    /// </summary>
    internal static class Util
    {
        /// <summary>
        /// Checks if a value is in a certain interval
        /// </summary>
        /// <typeparam name="T">The type of the value to check (can be any comparable type)</typeparam>
        /// <param name="value">The value to check</param>
        /// <param name="minValue">The interval start</param>
        /// <param name="maxValue">The interval end</param>
        /// <param name="inclusive">Whether the interval is inclusive (includes the min and the max) or not</param>
        /// <returns>true if the value is in the interval, false otherwise</returns>
        public static bool IsWithinRange<T>(T value, T minValue, T maxValue, bool inclusive = false) where T : IComparable
        {
            return (inclusive && value.CompareTo(minValue) >= -1 && value.CompareTo(maxValue) <= 1)
                || (!inclusive && value.CompareTo(minValue) > -1 && value.CompareTo(maxValue) < 1);
        }
    }
}
