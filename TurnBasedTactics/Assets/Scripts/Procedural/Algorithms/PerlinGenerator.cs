﻿namespace Procedural.Algorithms
{
    using UnityEngine;

    public class PerlinGenerator
    {
        private readonly float _xOrigin;
        private readonly float _yOrigin;
        private readonly float _scale;
        private readonly float _width;
        private readonly float _height;

        public PerlinGenerator(int width, int height, float scale)
        {
            this._xOrigin = Random.Range(0f, 1000f);
            this._yOrigin = Random.Range(0f, 1000f);
            this._scale = scale;
            this._width = width;
            this._height = height;
        }

        public float this[float x, float y]
        {
            get
            {
                float xCoord = this._xOrigin + x / this._width * this._scale;
                float yCoord = this._yOrigin + y / this._height * this._scale;
                return Mathf.Clamp(Mathf.PerlinNoise(xCoord, yCoord), 0f, 1f);
            }
        }
    }
}
