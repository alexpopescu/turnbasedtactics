﻿namespace Procedural.Algorithms
{
    public enum Winding
    {
        NONE = 0,
        CLOCKWISE,
        COUNTERCLOCKWISE
    }
}