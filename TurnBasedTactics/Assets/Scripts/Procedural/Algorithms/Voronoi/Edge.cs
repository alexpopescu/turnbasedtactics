﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System.Collections.Generic;

    /**
     * The line segment connecting the two Sites is part of the Delaunay triangulation;
     * the line segment connecting the two Vertices is part of the Voronoi diagram
     * @author ashaw
     */
    public sealed class Edge
    {
        private static readonly Stack<Edge> Pool = new Stack<Edge>();

        public static Edge CreateBisectingEdge(Site site0, Site site1)
        {
            float a, b, c;

            float dx = site1.X - site0.X;
            float dy = site1.Y - site0.Y;
            float absdx = dx > 0 ? dx : -dx;
            float absdy = dy > 0 ? dy : -dy;
            c = site0.X * dx + site0.Y * dy + (dx * dx + dy * dy) * 0.5f;
            if (absdx > absdy)
            {
                a = 1.0f;
                b = dy / dx;
                c /= dx;
            }
            else
            {
                b = 1.0f;
                a = dx / dy;
                c /= dy;
            }

            var edge = Create();

            edge.LeftSite = site0;
            edge.RightSite = site1;
            site0.AddEdge(edge);
            site1.AddEdge(edge);

            edge.LeftVertex = null;
            edge.RightVertex = null;

            edge.a = a;
            edge.b = b;
            edge.c = c;

            return edge;
        }

        private static Edge Create()
        {
            Edge edge;
            if (Pool.Count > 0)
            {
                edge = Pool.Pop();
                edge.Init();
            }
            else
            {
                edge = new Edge();
            }
            return edge;
        }

        public LineSegment DelaunayLine()
        {
            // draw a line connecting the input Sites for which the edge is a bisector:
            return new LineSegment(this.LeftSite.Coord, this.RightSite.Coord);
        }

        public LineSegment VoronoiEdge()
        {
            if (!this.Visible)
                return new LineSegment(null, null);
            return new LineSegment(this.ClippedEnds[Side.Left], this.ClippedEnds[Side.Right]);
        }

        private static int _nedges;

        public static readonly Edge Deleted = new Edge();

        // the equation of the edge: ax + by = c
        public float a, b, c;

        // the two Voronoi vertices that the edge connects
        //		(if one of them is null, the edge extends to infinity)
        public Vertex LeftVertex { get; private set; }

        public Vertex RightVertex { get; private set; }

        public Vertex Vertex(Side leftRight)
        {
            return leftRight == Side.Left ? this.LeftVertex : this.RightVertex;
        }

        public void SetVertex(Side leftRight, Vertex v)
        {
            if (leftRight == Side.Left)
            {
                this.LeftVertex = v;
            }
            else
            {
                this.RightVertex = v;
            }
        }

        public bool IsPartOfConvexHull()
        {
            return this.LeftVertex == null || this.RightVertex == null;
        }

        public float SitesDistance()
        {
            return Vector2.Distance(this.LeftSite.Coord, this.RightSite.Coord);
        }

        public static int CompareSitesDistances_MAX(Edge edge0, Edge edge1)
        {
            return edge0.SitesDistance().CompareTo(edge1.SitesDistance());
        }

        public static int CompareSitesDistances(Edge edge0, Edge edge1)
        {
            return -CompareSitesDistances_MAX(edge0, edge1);
        }

        // Once clipVertices() is called, this Dictionary will hold two Points
        // representing the clipped coordinates of the left and right ends...

        public Dictionary<Side, Vector2?> ClippedEnds { get; private set; }

        // unless the entire Edge is outside the bounds.
        // In that case visible will be false:
        public bool Visible
        {
            get { return this.ClippedEnds != null; }
        }

        // the two input Sites for which this Edge is a bisector:
        private Dictionary<Side, Site> _sites;

        public Site LeftSite
        {
            get { return this._sites[Side.Left]; }
            set { this._sites[Side.Left] = value; }

        }

        public Site RightSite
        {
            get { return this._sites[Side.Right]; }
            set { this._sites[Side.Right] = value; }
        }

        public Site Site(Side leftRight)
        {
            return this._sites[leftRight];
        }

        private readonly int _edgeIndex;

        public void Dispose()
        {
            this.LeftVertex = null;
            this.RightVertex = null;

            if (this.ClippedEnds != null)
            {
                this.ClippedEnds[Side.Left] = null;
                this.ClippedEnds[Side.Right] = null;
                this.ClippedEnds = null;
            }

            this._sites[Side.Left] = null;
            this._sites[Side.Right] = null;
            this._sites = null;

            Pool.Push(this);
        }

        private Edge()
        {
            this._edgeIndex = _nedges++;
            this.Init();
        }

        private void Init()
        {
            this._sites = new Dictionary<Side, Site>();
        }

        public override string ToString()
        {
            return string.Format("Edge {0}; sites {1}, {2}; endVertices {3}, {4}::",
                this._edgeIndex, this._sites[Side.Left], this._sites[Side.Right],
                this.LeftVertex != null ? this.LeftVertex.vertexIndex.ToString() : "null",
                this.RightVertex != null ? this.RightVertex.vertexIndex.ToString() : "null");
        }

        /**
         * Set _clippedVertices to contain the two ends of the portion of the Voronoi edge that is visible
         * within the bounds.  If no part of the Edge falls within the bounds, leave _clippedVertices null. 
         * @param bounds
         * 
         */
        public void ClipVertices(Rect bounds)
        {
            float xmin = bounds.xMin;
            float ymin = bounds.yMin;
            float xmax = bounds.xMax;
            float ymax = bounds.yMax;

            Vertex vertex0, vertex1;
            float x0, x1, y0, y1;

            if (Mathf.Approximately(this.a, 1f) && this.b >= 0.0)
            {
                vertex0 = this.RightVertex;
                vertex1 = this.LeftVertex;
            }
            else
            {
                vertex0 = this.LeftVertex;
                vertex1 = this.RightVertex;
            }

            if (Mathf.Approximately(this.a, 1f))
            {
                y0 = ymin;
                if (vertex0 != null && vertex0.Y > ymin)
                {
                    y0 = vertex0.Y;
                }
                if (y0 > ymax)
                {
                    return;
                }
                x0 = this.c - this.b * y0;

                y1 = ymax;
                if (vertex1 != null && vertex1.Y < ymax)
                {
                    y1 = vertex1.Y;
                }
                if (y1 < ymin)
                {
                    return;
                }
                x1 = this.c - this.b * y1;

                if ((x0 > xmax && x1 > xmax) || (x0 < xmin && x1 < xmin))
                {
                    return;
                }

                if (x0 > xmax)
                {
                    x0 = xmax;
                    y0 = (this.c - x0) / this.b;
                }
                else if (x0 < xmin)
                {
                    x0 = xmin;
                    y0 = (this.c - x0) / this.b;
                }

                if (x1 > xmax)
                {
                    x1 = xmax;
                    y1 = (this.c - x1) / this.b;
                }
                else if (x1 < xmin)
                {
                    x1 = xmin;
                    y1 = (this.c - x1) / this.b;
                }
            }
            else
            {
                x0 = xmin;
                if (vertex0 != null && vertex0.X > xmin)
                {
                    x0 = vertex0.X;
                }
                if (x0 > xmax)
                {
                    return;
                }
                y0 = this.c - this.a * x0;

                x1 = xmax;
                if (vertex1 != null && vertex1.X < xmax)
                {
                    x1 = vertex1.X;
                }
                if (x1 < xmin)
                {
                    return;
                }
                y1 = this.c - this.a * x1;

                if ((y0 > ymax && y1 > ymax) || (y0 < ymin && y1 < ymin))
                {
                    return;
                }

                if (y0 > ymax)
                {
                    y0 = ymax;
                    x0 = (this.c - y0) / this.a;
                }
                else if (y0 < ymin)
                {
                    y0 = ymin;
                    x0 = (this.c - y0) / this.a;
                }

                if (y1 > ymax)
                {
                    y1 = ymax;
                    x1 = (this.c - y1) / this.a;
                }
                else if (y1 < ymin)
                {
                    y1 = ymin;
                    x1 = (this.c - y1) / this.a;
                }
            }

            this.ClippedEnds = new Dictionary<Side, Vector2?>();
            if (vertex0 == this.LeftVertex)
            {
                this.ClippedEnds[Side.Left] = new Vector2(x0, y0);
                this.ClippedEnds[Side.Right] = new Vector2(x1, y1);
            }
            else
            {
                this.ClippedEnds[Side.Right] = new Vector2(x0, y0);
                this.ClippedEnds[Side.Left] = new Vector2(x1, y1);
            }
        }

    }
}