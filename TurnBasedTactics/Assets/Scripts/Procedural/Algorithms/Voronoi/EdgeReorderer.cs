﻿namespace Procedural.Algorithms
{
    using System;
    using System.Collections.Generic;

    public enum VertexOrSite
    {
        Vertex,
        Site
    }

    sealed class EdgeReorderer : IDisposable
    {
        private List<Edge> _edges;
        private List<Side> _edgeOrientations;

        public List<Edge> edges
        {
            get { return this._edges; }
        }

        public List<Side> EdgeOrientations
        {
            get { return this._edgeOrientations; }
        }

        public EdgeReorderer(List<Edge> origEdges, VertexOrSite criterion)
        {
            this._edges = new List<Edge>();
            this._edgeOrientations = new List<Side>();
            if (origEdges.Count > 0)
            {
                this._edges = this.ReorderEdges(origEdges, criterion);
            }
        }

        public void Dispose()
        {
            this._edges = null;
            this._edgeOrientations = null;
        }

        private List<Edge> ReorderEdges(List<Edge> origEdges, VertexOrSite criterion)
        {
            int n = origEdges.Count;
            // we're going to reorder the edges in order of traversal
            var done = new bool[n];
            int nDone = 0;
            for (int j = 0; j < n; j++)
            {
                done[j] = false;
            }
            var newEdges = new List<Edge>(); // TODO: Switch to Deque if performance is a concern

            int i = 0;
            var edge = origEdges[i];
            newEdges.Add(edge);
            this._edgeOrientations.Add(Side.Left);
            var firstPoint = criterion == VertexOrSite.Vertex ? edge.LeftVertex : (ICoord) edge.LeftSite;
            var lastPoint = criterion == VertexOrSite.Vertex ? edge.RightVertex : (ICoord) edge.RightSite;

            if (firstPoint == Vertex.Infinity || lastPoint == Vertex.Infinity)
            {
                return new List<Edge>();
            }

            done[i] = true;
            ++nDone;

            while (nDone < n)
            {
                for (i = 1; i < n; ++i)
                {
                    if (done[i])
                    {
                        continue;
                    }
                    edge = origEdges[i];
                    var leftPoint = criterion == VertexOrSite.Vertex ? edge.LeftVertex : (ICoord) edge.LeftSite;
                    var rightPoint = criterion == VertexOrSite.Vertex ? edge.RightVertex : (ICoord) edge.RightSite;
                    if (leftPoint == Vertex.Infinity || rightPoint == Vertex.Infinity)
                    {
                        return new List<Edge>();
                    }
                    if (leftPoint == lastPoint)
                    {
                        lastPoint = rightPoint;
                        this._edgeOrientations.Add(Side.Left);
                        newEdges.Add(edge);
                        done[i] = true;
                    }
                    else if (rightPoint == firstPoint)
                    {
                        firstPoint = leftPoint;
                        this._edgeOrientations.Insert(0, Side.Left); // TODO: Change datastructure if this is slow
                        newEdges.Insert(0, edge);
                        done[i] = true;
                    }
                    else if (leftPoint == firstPoint)
                    {
                        firstPoint = rightPoint;
                        this._edgeOrientations.Insert(0, Side.Right);
                        newEdges.Insert(0, edge);
                        done[i] = true;
                    }
                    else if (rightPoint == lastPoint)
                    {
                        lastPoint = leftPoint;
                        this._edgeOrientations.Add(Side.Right);
                        newEdges.Add(edge);
                        done[i] = true;
                    }
                    if (done[i])
                    {
                        ++nDone;
                    }
                }
            }

            return newEdges;
        }

    }
}