﻿namespace Procedural.Algorithms
{
    using System;
    using UnityEngine;


    internal sealed class HalfedgePriorityQueue : IDisposable // also known as heap
    {
        private Halfedge[] _hash;
        private int _count;
        private int _minBucket;

        private readonly int _hashsize;
        private readonly float _ymin;
        private readonly float _deltay;

        public HalfedgePriorityQueue(float ymin, float deltay, int sqrtNsites)
        {
            this._ymin = ymin;
            this._deltay = deltay;
            this._hashsize = 4 * sqrtNsites;
            this.Initialize();
        }

        public void Dispose()
        {
            // get rid of dummies
            for (int i = 0; i < this._hashsize; ++i)
            {
                this._hash[i].Dispose();
                this._hash[i] = null;
            }
            this._hash = null;
        }

        private void Initialize()
        {
            int i;

            this._count = 0;
            this._minBucket = 0;
            this._hash = new Halfedge[this._hashsize];
            // dummy Halfedge at the top of each hash
            for (i = 0; i < this._hashsize; ++i)
            {
                this._hash[i] = Halfedge.CreateDummy();
                this._hash[i].NextInPriorityQueue = null;
            }
        }

        public void Insert(Halfedge halfEdge)
        {
            int insertionBucket = this.Bucket(halfEdge);
            if (insertionBucket < this._minBucket)
            {
                this._minBucket = insertionBucket;
            }
            var previous = this._hash[insertionBucket];
            Halfedge next;
            while ((next = previous.NextInPriorityQueue) != null
                   && (halfEdge.Ystar > next.Ystar
                       || (Mathf.Approximately(halfEdge.Ystar, next.Ystar) && halfEdge.Vertex.X > next.Vertex.X)))
            {
                previous = next;
            }
            halfEdge.NextInPriorityQueue = previous.NextInPriorityQueue;
            previous.NextInPriorityQueue = halfEdge;
            ++this._count;
        }

        public void Remove(Halfedge halfEdge)
        {
            int removalBucket = this.Bucket(halfEdge);

            if (halfEdge.Vertex != null)
            {
                var previous = this._hash[removalBucket];
                while (previous.NextInPriorityQueue != halfEdge)
                {
                    previous = previous.NextInPriorityQueue;
                }
                previous.NextInPriorityQueue = halfEdge.NextInPriorityQueue;
                this._count--;
                halfEdge.Vertex = null;
                halfEdge.NextInPriorityQueue = null;
                halfEdge.Dispose();
            }
        }

        private int Bucket(Halfedge halfEdge)
        {
            int theBucket = (int) ((halfEdge.Ystar - this._ymin) / this._deltay * this._hashsize);
            if (theBucket < 0) theBucket = 0;
            if (theBucket >= this._hashsize) theBucket = this._hashsize - 1;
            return theBucket;
        }

        public bool IsEmpty()
        {
            return this._count == 0;
        }

        private bool IsEmpty(int bucket)
        {
            return (this._hash[bucket].NextInPriorityQueue == null);
        }

        /**
         * move _minBucket until it contains an actual Halfedge (not just the dummy at the top); 
         * 
         */
        private void AdjustMinBucket()
        {
            while (this._minBucket < this._hashsize - 1 && this.IsEmpty(this._minBucket))
            {
                ++this._minBucket;
            }
        }

        /**
         * @return coordinates of the Halfedge's vertex in V*, the transformed Voronoi diagram
         * 
         */
        public Vector2 Min()
        {
            this.AdjustMinBucket();
            var answer = this._hash[this._minBucket].NextInPriorityQueue;
            return new Vector2(answer.Vertex.X, answer.Ystar);
        }

        /**
         * remove and return the min Halfedge
         * @return 
         * 
         */
        public Halfedge ExtractMin()
        {
            // get the first real Halfedge in _minBucket
            var answer = this._hash[this._minBucket].NextInPriorityQueue;

            this._hash[this._minBucket].NextInPriorityQueue = answer.NextInPriorityQueue;
            this._count--;
            answer.NextInPriorityQueue = null;

            return answer;
        }

    }
}