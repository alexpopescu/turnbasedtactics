﻿namespace Procedural.Algorithms
{
    public enum Side
    {
        Left = 0,
        Right
    }

    public static class SideHelper
    {
        public static Side Other(this Side leftRight)
        {
            return leftRight == Side.Left ? Side.Right : Side.Left;
        }
    }
}