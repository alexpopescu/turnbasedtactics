﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public sealed class SiteList : List<Site>, IDisposable
    {
        private int _currentIndex;

        private bool _sorted;

        public SiteList()
        {
            this._sorted = false;
        }

        public void Dispose()
        {
            for (int i = 0; i < this.Count; i++)
            {
                var site = this[i];
                site.Dispose();
            }
            this.Clear();
        }

        public new int Add(Site site)
        {
            this._sorted = false;
            base.Add(site);
            return this.Count;
        }

        public Site Next()
        {
            if (this._sorted == false)
            {
                UnityEngine.Debug.LogError("SiteList::next():  sites have not been sorted");
            }
            if (this._currentIndex < this.Count)
            {
                return this[this._currentIndex++];
            }
            return null;
        }

        internal Rect GetSitesBounds()
        {
            if (this._sorted == false)
            {
                Site.SortSites(this);
                this._currentIndex = 0;
                this._sorted = true;
            }
            if (this.Count == 0)
            {
                return new Rect(0, 0, 0, 0);
            }
            float xmin = float.MaxValue;
            float xmax = float.MinValue;
            for (int i = 0; i < this.Count; i++)
            {
                Site site = this[i];
                if (site.X < xmin)
                {
                    xmin = site.X;
                }
                if (site.X > xmax)
                {
                    xmax = site.X;
                }
            }
            // here's where we assume that the sites have been sorted on y:
            float ymin = this[0].Y;
            float ymax = this[this.Count - 1].Y;

            return new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
        }

        public List<uint> SiteColors( /*BitmapData referenceImage = null*/)
        {
            var colors = new List<uint>();
            for (int i = 0; i < this.Count; i++)
            {
                colors.Add( /*referenceImage ? referenceImage.getPixel(site.x, site.y) :*/this[i].Color);
            }
            return colors;
        }

        public List<Vector2> SiteCoords()
        {
            var coords = new List<Vector2>();
            for (int i = 0; i < this.Count; i++)
            {
                coords.Add(this[i].Coord);
            }
            return coords;
        }

        /**
         * 
         * @return the largest circle centered at each site that fits in its region;
         * if the region is infinite, return a circle of radius 0.
         * 
         */
        public List<Circle> Circles()
        {
            var circles = new List<Circle>();
            for (int i = 0; i < this.Count; i++)
            {
                var site = this[i];
                float radius = 0f;
                var nearestEdge = site.NearestEdge();

                if (!nearestEdge.IsPartOfConvexHull())
                {
                    radius = nearestEdge.SitesDistance() * 0.5f;
                }
                circles.Add(new Circle(site.X, site.Y, radius));
            }
            return circles;
        }

        public List<List<Vector2>> Regions(Rect plotBounds)
        {
            var regions = new List<List<Vector2>>();
            for (int i = 0; i < this.Count; i++)
            {
                regions.Add(this[i].Region(plotBounds));
            }
            return regions;
        }

        public void ResetListIndex()
        {
            this._currentIndex = 0;
        }

        /**
         * 
         * @param proximityMap a BitmapData whose regions are filled with the site index values; see PlanePointsCanvas::fillRegions()
         * @param x
         * @param y
         * @return coordinates of nearest Site to (x, y)
         * 
         */
        public Vector2? NearestSitePoint( /*proximityMap:BitmapData,*/ float x, float y)
        {
            //			uint index = proximityMap.getPixel(x, y);
            //			if (index > _sites.length - 1)
            //			{
            return null;
            //			}
            //			return _sites[index].coord;
        }

    }
}