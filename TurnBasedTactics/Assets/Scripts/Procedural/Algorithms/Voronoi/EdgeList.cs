﻿namespace Procedural.Algorithms
{
    using System;
    using UnityEngine;


    internal sealed class EdgeList : IDisposable
    {
        private float _deltax;
        private float _xmin;

        private int _hashsize;
        private Halfedge[] _hash;
        private Halfedge _leftEnd;

        public Halfedge LeftEnd
        {
            get { return this._leftEnd; }
        }

        private Halfedge _rightEnd;

        public Halfedge RightEnd
        {
            get { return this._rightEnd; }
        }

        public void Dispose()
        {
            var halfEdge = this._leftEnd;
            while (halfEdge != this._rightEnd)
            {
                var prevHe = halfEdge;
                halfEdge = halfEdge.EdgeListRightNeighbor;
                prevHe.Dispose();
            }
            this._leftEnd = null;
            this._rightEnd.Dispose();
            this._rightEnd = null;

            int i;
            for (i = 0; i < this._hashsize; ++i)
            {
                this._hash[i] = null;
            }
            this._hash = null;
        }

        public EdgeList(float xmin, float deltax, int sqrtNsites)
        {
            this._xmin = xmin;
            this._deltax = deltax;
            this._hashsize = 2 * sqrtNsites;

            this._hash = new Halfedge[this._hashsize];

            // two dummy Halfedges:
            this._leftEnd = Halfedge.CreateDummy();
            this._rightEnd = Halfedge.CreateDummy();
            this._leftEnd.EdgeListLeftNeighbor = null;
            this._leftEnd.EdgeListRightNeighbor = this._rightEnd;
            this._rightEnd.EdgeListLeftNeighbor = this._leftEnd;
            this._rightEnd.EdgeListRightNeighbor = null;
            this._hash[0] = this._leftEnd;
            this._hash[this._hashsize - 1] = this._rightEnd;
        }

        /**
         * Insert newHalfedge to the right of lb 
         * @param lb
         * @param newHalfedge
         * 
         */
        public void Insert(Halfedge lb, Halfedge newHalfedge)
        {
            newHalfedge.EdgeListLeftNeighbor = lb;
            newHalfedge.EdgeListRightNeighbor = lb.EdgeListRightNeighbor;
            lb.EdgeListRightNeighbor.EdgeListLeftNeighbor = newHalfedge;
            lb.EdgeListRightNeighbor = newHalfedge;
        }

        /**
         * This function only removes the Halfedge from the left-right list.
         * We cannot dispose it yet because we are still using it. 
         * @param halfEdge
         * 
         */
        public void Remove(Halfedge halfEdge)
        {
            halfEdge.EdgeListLeftNeighbor.EdgeListRightNeighbor = halfEdge.EdgeListRightNeighbor;
            halfEdge.EdgeListRightNeighbor.EdgeListLeftNeighbor = halfEdge.EdgeListLeftNeighbor;
            halfEdge.Edge = Edge.Deleted;
            halfEdge.EdgeListLeftNeighbor = halfEdge.EdgeListRightNeighbor = null;
        }

        /**
         * Find the rightmost Halfedge that is still left of p 
         * @param p
         * @return 
         * 
         */
        public Halfedge EdgeListLeftNeighbor(Vector2 p)
        {
            /* Use hash table to get close to desired halfedge */
            int bucket = (int) ((p.x - this._xmin) / this._deltax * this._hashsize);
            if (bucket < 0)
            {
                bucket = 0;
            }
            if (bucket >= this._hashsize)
            {
                bucket = this._hashsize - 1;
            }
            var halfEdge = this.GetHash(bucket);
            if (halfEdge == null)
            {
                int i;
                for (i = 1;; ++i)
                {
                    if ((halfEdge = this.GetHash(bucket - i)) != null)
                        break;
                    if ((halfEdge = this.GetHash(bucket + i)) != null)
                        break;
                }
            }
            /* Now search linear list of halfedges for the correct one */
            if (halfEdge == this.LeftEnd || (halfEdge != this.RightEnd && halfEdge.IsLeftOf(p)))
            {
                do
                {
                    halfEdge = halfEdge.EdgeListRightNeighbor;
                } while (halfEdge != this.RightEnd && halfEdge.IsLeftOf(p));
                halfEdge = halfEdge.EdgeListLeftNeighbor;
            }
            else
            {
                do
                {
                    halfEdge = halfEdge.EdgeListLeftNeighbor;
                } while (halfEdge != this.LeftEnd && !halfEdge.IsLeftOf(p));
            }

            /* Update hash table and reference counts */
            if (bucket > 0 && bucket < this._hashsize - 1)
            {
                this._hash[bucket] = halfEdge;
            }
            return halfEdge;
        }

        /* Get entry from hash table, pruning any deleted nodes */
        private Halfedge GetHash(int b)
        {
            if (b < 0 || b >= this._hashsize)
            {
                return null;
            }
            var halfEdge = this._hash[b];
            if (halfEdge != null && halfEdge.Edge == Edge.Deleted)
            {
                /* Hash table points to deleted halfedge.  Patch as necessary. */
                this._hash[b] = null;
                // still can't dispose halfEdge yet!
                return null;
            }
            else
            {
                return halfEdge;
            }
        }

    }
}