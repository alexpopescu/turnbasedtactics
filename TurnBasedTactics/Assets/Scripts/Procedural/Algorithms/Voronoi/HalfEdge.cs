﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public sealed class Halfedge : IDisposable
    {
        private static Stack<Halfedge> _pool = new Stack<Halfedge>();

        public static Halfedge Create(Edge edge, Side? lr)
        {
            if (_pool.Count > 0)
            {
                return _pool.Pop().Init(edge, lr);
            }
            return new Halfedge(edge, lr);
        }

        public static Halfedge CreateDummy()
        {
            return Create(null, null);
        }

        public Halfedge EdgeListLeftNeighbor, EdgeListRightNeighbor;
        public Halfedge NextInPriorityQueue;

        public Edge Edge;
        public Side? LeftRight;
        public Vertex Vertex;

        // the vertex's y-coordinate in the transformed Voronoi space V*
        public float Ystar;

        public Halfedge(Edge edge = null, Side? lr = null)
        {
            this.Init(edge, lr);
        }

        private Halfedge Init(Edge edge, Side? lr)
        {
            this.Edge = edge;
            this.LeftRight = lr;
            this.NextInPriorityQueue = null;
            this.Vertex = null;
            return this;
        }

        public override string ToString()
        {
            return "Halfedge (leftRight: " + this.LeftRight + "; vertex: " + this.Vertex + ")";
        }

        public void Dispose()
        {
            if (this.EdgeListLeftNeighbor != null || this.EdgeListRightNeighbor != null)
            {
                // still in EdgeList
                return;
            }
            if (this.NextInPriorityQueue != null)
            {
                // still in PriorityQueue
                return;
            }
            this.Edge = null;
            this.LeftRight = null;
            this.Vertex = null;
            _pool.Push(this);
        }

        public void ReallyDispose()
        {
            this.EdgeListLeftNeighbor = null;
            this.EdgeListRightNeighbor = null;
            this.NextInPriorityQueue = null;
            this.Edge = null;
            this.LeftRight = null;
            this.Vertex = null;
            _pool.Push(this);
        }

        internal bool IsLeftOf(Vector2 p)
        {
            bool above;

            var topSite = this.Edge.RightSite;
            bool rightOfSite = p.x > topSite.X;
            if (rightOfSite && this.LeftRight == Side.Left)
            {
                return true;
            }
            if (!rightOfSite && this.LeftRight == Side.Right)
            {
                return false;
            }

            if (this.Edge.a == 1.0)
            {
                float dyp = p.y - topSite.Y;
                float dxp = p.x - topSite.X;
                bool fast = false;
                if ((!rightOfSite && this.Edge.b < 0.0) || (rightOfSite && this.Edge.b >= 0.0))
                {
                    above = dyp >= this.Edge.b * dxp;
                    fast = above;
                }
                else
                {
                    above = p.x + p.y * this.Edge.b > this.Edge.c;
                    if (this.Edge.b < 0.0)
                    {
                        above = !above;
                    }
                    if (!above)
                    {
                        fast = true;
                    }
                }
                if (!fast)
                {
                    float dxs = topSite.X - this.Edge.LeftSite.X;
                    above = this.Edge.b * (dxp * dxp - dyp * dyp) <
                            dxs * dyp * (1.0 + 2.0 * dxp / dxs + this.Edge.b * this.Edge.b);
                    if (this.Edge.b < 0.0)
                    {
                        above = !above;
                    }
                }
            }
            else
            {
                /* edge.b == 1.0 */
                float yl = this.Edge.c - this.Edge.a * p.x;
                float t1 = p.y - yl;
                float t2 = p.x - topSite.X;
                float t3 = yl - topSite.Y;
                above = t1 * t1 > t2 * t2 + t3 * t3;
            }
            return this.LeftRight == Side.Left ? above : !above;
        }

    }
}