﻿namespace Procedural.Algorithms
{
    using UnityEngine;

    public sealed class LineSegment
    {
        public static int CompareLengths_MAX(LineSegment segment0, LineSegment segment1)
        {
            float length0 = Vector2.Distance((Vector2) segment0.P0, (Vector2) segment0.P1);
            float length1 = Vector2.Distance((Vector2) segment1.P0, (Vector2) segment1.P1);
            if (length0 < length1)
            {
                return 1;
            }
            if (length0 > length1)
            {
                return -1;
            }
            return 0;
        }

        public static int CompareLengths(LineSegment edge0, LineSegment edge1)
        {
            return -CompareLengths_MAX(edge0, edge1);
        }

        public Vector2? P0;
        public Vector2? P1;

        public LineSegment(Vector2? p0, Vector2? p1)
        {
            this.P0 = p0;
            this.P1 = p1;
        }

        public LineSegment(Edge edge)
        {
            this.P0 = edge.ClippedEnds[Side.Left];
            this.P1 = edge.ClippedEnds[Side.Right];
        }
    }
}