﻿namespace Procedural.Algorithms
{
    using UnityEngine;

    public sealed class Circle
    {
        public Vector2 center;
        public float radius;

        public Circle(float centerX, float centerY, float radius)
        {
            this.center = new Vector2(centerX, centerY);
            this.radius = radius;
        }

        public override string ToString()
        {
            return string.Format("Circle (center: {0}; radius: {1})", this.center, this.radius);
        }

    }
}