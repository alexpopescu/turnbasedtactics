﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System.Collections.Generic;

    public sealed class Polygon
    {
        private readonly List<Vector2> _vertices;

        public Polygon(List<Vector2> vertices)
        {
            this._vertices = vertices;
        }

        public float Area()
        {
            return Mathf.Abs(this.SignedDoubleArea() * 0.5f); // XXX: I'm a bit nervous about this; not sure what the * 0.5 is for, bithacking?
        }

        public Winding GetWinding()
        {
            float signedDoubleArea = this.SignedDoubleArea();
            if (signedDoubleArea < 0)
            {
                return Winding.CLOCKWISE;
            }
            if (signedDoubleArea > 0)
            {
                return Winding.COUNTERCLOCKWISE;
            }
            return Winding.NONE;
        }

        private float SignedDoubleArea() // XXX: I'm a bit nervous about this because Actionscript represents everything as doubles, not floats
        {
            int index;
            int n = this._vertices.Count;
            float signedDoubleArea = 0; // Losing lots of precision?
            for (index = 0; index < n; ++index)
            {
                int nextIndex = (index + 1) % n;
                var point = this._vertices[index];
                var next = this._vertices[nextIndex];
                signedDoubleArea += point.x * next.y - next.x * point.y;
            }
            return signedDoubleArea;
        }
    }
}