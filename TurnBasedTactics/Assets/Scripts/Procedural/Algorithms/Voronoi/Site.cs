﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public sealed class Site : ICoord, IComparable
    {
        private const float EPSILON = .005f;

        private static bool CloseEnough(Vector2 p0, Vector2 p1)
        {
            return Vector2.Distance(p0, p1) < EPSILON;
        }

        // #region Landform
        // private LandformType? _landformType;
        // public LandformType LandformType
        // {
        //     get
        //     {
        //         if (!this._landformType.HasValue)
        //         {
        //             throw new NullReferenceException("Undefined Landscape Type");
        //         }
        //         return this._landformType.Value;
        //     }
        // }
        //
        // public void SetLandformType(LandformType newType)
        // {
        //     if (this._landformType.HasValue) return;
        //     this._landformType = newType;
        // }
        //
        // private string _landform;
        // public string Landform { get { return this._landform; } }
        //
        // public void SetLandform(string name)
        // {
        //     this._landform = name;
        // }letObsoleteAttribute me c
        public Vector2 Coord { get; private set; }

        public uint Color;
        public float Weight;

        private uint _siteIndex;
        private static Stack<Site> _pool = new Stack<Site>();

        // the edges that define this Site's Voronoi region:
        internal List<Edge> Edges { get; private set; }

        // which end of each edge hooks up with the previous edge in _edges:
        private List<Side> _edgeOrientations;
        // ordered list of points that define the region clipped to bounds:
        private List<Vector2> _region;

        public float X => this.Coord.x;

        internal float Y => this.Coord.y;

        public float Dist(ICoord p)
        {
            return Vector2.Distance(p.Coord, this.Coord);
        }

        private Site(Vector2 p, uint index, float weight, uint color)
        {
            //			if (lock != PrivateConstructorEnforcer)
            //			{
            //				throw new Error("Site constructor is private");
            //			}
            this.Init(p, index, weight, color);
        }

        private Site Init(Vector2 p, uint index, float weight, uint color)
        {
            this.Coord = p;
            this._siteIndex = index;
            this.Weight = weight;
            this.Color = color;
            this.Edges = new List<Edge>();
            this._region = null;
            return this;
        }

        public static Site Create(Vector2 p, uint index, float weight, uint color)
        {
            if (_pool.Count > 0)
            {
                return _pool.Pop().Init(p, index, weight, color);
            }
            else
            {
                return new Site(p, index, weight, color);
            }
        }

        internal static void SortSites(List<Site> sites)
        {
            //			sites.sort(Site.compare);
            sites.Sort(); // XXX: Check if this works
        }

        /**
         * sort sites on y, then x, coord
         * also change each site's _siteIndex to match its new position in the list
         * so the _siteIndex can be used to identify the site for nearest-neighbor queries
         * 
         * haha "also" - means more than one responsibility...
         * 
         */
        public int CompareTo(System.Object obj) // XXX: Really, really worried about this because it depends on how sorting works in AS3 impl - Julian
        {
            var s2 = (Site)obj;

            int returnValue = Voronoi.CompareByYThenX(this, s2);

            // swap _siteIndex values if necessary to match new ordering:
            uint tempIndex;
            if (returnValue == -1)
            {
                if (this._siteIndex > s2._siteIndex)
                {
                    tempIndex = this._siteIndex;
                    this._siteIndex = s2._siteIndex;
                    s2._siteIndex = tempIndex;
                }
            }
            else if (returnValue == 1)
            {
                if (s2._siteIndex > this._siteIndex)
                {
                    tempIndex = s2._siteIndex;
                    s2._siteIndex = this._siteIndex;
                    this._siteIndex = tempIndex;
                }

            }

            return returnValue;
        }

        public override string ToString()
        {
            return string.Format("Site {0}: {1}", this._siteIndex, this.Coord);
        }

        private void Move(Vector2 p)
        {
            this.Clear();
            this.Coord = p;
        }

        public void Dispose()
        {
            //			_coord = null;
            this.Clear();
            _pool.Push(this);
        }

        private void Clear()
        {
            if (this.Edges != null)
            {
                this.Edges.Clear();
                this.Edges = null;
            }
            if (this._edgeOrientations != null)
            {
                this._edgeOrientations.Clear();
                this._edgeOrientations = null;
            }
            if (this._region != null)
            {
                this._region.Clear();
                this._region = null;
            }
        }

        public void AddEdge(Edge edge)
        {
            this.Edges.Add(edge);
        }

        public Edge NearestEdge()
        {
            this.Edges.Sort(Edge.CompareSitesDistances);
            return this.Edges[0];
        }

        public List<Site> NeighborSites()
        {
            if (this.Edges == null || this.Edges.Count == 0)
            {
                return new List<Site>();
            }
            if (this._edgeOrientations == null)
            {
                this.ReorderEdges();
            }
            return this.Edges.Select(this.NeighborSite).ToList();
        }

        public Site NeighborSite(Edge edge)
        {
            if (this == edge.LeftSite) return edge.RightSite;
            if (this == edge.RightSite) return edge.LeftSite;
            return null;
        }

        internal List<Vector2> Region(Rect clippingBounds)
        {
            if (this.Edges == null || this.Edges.Count == 0)
            {
                return new List<Vector2>();
            }
            if (this._edgeOrientations == null)
            {
                this.ReorderEdges();
                this._region = this.ClipToBounds(clippingBounds);
                if (new Polygon(this._region).GetWinding() == Winding.CLOCKWISE)
                {
                    this._region.Reverse();
                }
            }
            return this._region;
        }

        private void ReorderEdges()
        {
            var reorderer = new EdgeReorderer(this.Edges, VertexOrSite.Vertex);
            this.Edges = reorderer.edges;
            this._edgeOrientations = reorderer.EdgeOrientations;
            reorderer.Dispose();
        }

        private List<Vector2> ClipToBounds(Rect bounds)
        {
            var points = new List<Vector2>();
            int n = this.Edges.Count;
            int i = 0;
            while (i < n && !this.Edges[i].Visible) ++i;

            if (i == n)
            {
                // no edges visible
                return new List<Vector2>();
            }
            var edge = this.Edges[i];
            var orientation = this._edgeOrientations[i];

            if (edge.ClippedEnds[orientation] == null)
            {
                Debug.LogError("XXX: Null detected when there should be a Vector2!");
            }
            if (edge.ClippedEnds[SideHelper.Other(orientation)] == null)
            {
                Debug.LogError("XXX: Null detected when there should be a Vector2!");
            }
            points.Add((Vector2)edge.ClippedEnds[orientation]);
            points.Add((Vector2)edge.ClippedEnds[SideHelper.Other(orientation)]);

            for (int j = i + 1; j < n; ++j)
            {
                edge = this.Edges[j];
                if (edge.Visible == false)
                {
                    continue;
                }
                this.Connect(points, j, bounds);
            }
            // close up the polygon by adding another corner point of the bounds if needed:
            this.Connect(points, i, bounds, true);

            return points;
        }

        private void Connect(List<Vector2> points, int j, Rect bounds, bool closingUp = false)
        {
            var rightPoint = points[points.Count - 1];
            var newEdge = this.Edges[j];
            var newOrientation = this._edgeOrientations[j];
            // the point that  must be connected to rightPoint:
            if (newEdge.ClippedEnds[newOrientation] == null)
            {
                Debug.LogError("XXX: Null detected when there should be a Vector2!");
            }
            var newPoint = (Vector2)newEdge.ClippedEnds[newOrientation];
            if (!CloseEnough(rightPoint, newPoint))
            {
                // The points do not coincide, so they must have been clipped at the bounds;
                // see if they are on the same border of the bounds:
                if (rightPoint.x != newPoint.x && rightPoint.y != newPoint.y)
                {
                    // They are on different borders of the bounds;
                    // insert one or two corners of bounds as needed to hook them up:
                    // (NOTE this will not be correct if the region should take up more than
                    // half of the bounds rect, for then we will have gone the wrong way
                    // around the bounds and included the smaller part rather than the larger)
                    int rightCheck = BoundsCheck.Check(rightPoint, bounds);
                    int newCheck = BoundsCheck.Check(newPoint, bounds);
                    float px, py;
                    if ((rightCheck & BoundsCheck.RIGHT) != 0)
                    {
                        px = bounds.xMax;
                        if ((newCheck & BoundsCheck.BOTTOM) != 0)
                        {
                            py = bounds.yMax;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.TOP) != 0)
                        {
                            py = bounds.yMin;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.LEFT) != 0)
                        {
                            if (rightPoint.y - bounds.y + newPoint.y - bounds.y < bounds.height)
                            {
                                py = bounds.yMin;
                            }
                            else
                            {
                                py = bounds.yMax;
                            }
                            points.Add(new Vector2(px, py));
                            points.Add(new Vector2(bounds.xMin, py));
                        }
                    }
                    else if ((rightCheck & BoundsCheck.LEFT) != 0)
                    {
                        px = bounds.xMin;
                        if ((newCheck & BoundsCheck.BOTTOM) != 0)
                        {
                            py = bounds.yMax;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.TOP) != 0)
                        {
                            py = bounds.yMin;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.RIGHT) != 0)
                        {
                            if (rightPoint.y - bounds.y + newPoint.y - bounds.y < bounds.height)
                            {
                                py = bounds.yMin;
                            }
                            else
                            {
                                py = bounds.yMax;
                            }
                            points.Add(new Vector2(px, py));
                            points.Add(new Vector2(bounds.xMax, py));
                        }
                    }
                    else if ((rightCheck & BoundsCheck.TOP) != 0)
                    {
                        py = bounds.yMin;
                        if ((newCheck & BoundsCheck.RIGHT) != 0)
                        {
                            px = bounds.xMax;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.LEFT) != 0)
                        {
                            px = bounds.xMin;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.BOTTOM) != 0)
                        {
                            if (rightPoint.x - bounds.x + newPoint.x - bounds.x < bounds.width)
                            {
                                px = bounds.xMin;
                            }
                            else
                            {
                                px = bounds.xMax;
                            }
                            points.Add(new Vector2(px, py));
                            points.Add(new Vector2(px, bounds.yMax));
                        }
                    }
                    else if ((rightCheck & BoundsCheck.BOTTOM) != 0)
                    {
                        py = bounds.yMax;
                        if ((newCheck & BoundsCheck.RIGHT) != 0)
                        {
                            px = bounds.xMax;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.LEFT) != 0)
                        {
                            px = bounds.xMin;
                            points.Add(new Vector2(px, py));
                        }
                        else if ((newCheck & BoundsCheck.TOP) != 0)
                        {
                            if (rightPoint.x - bounds.x + newPoint.x - bounds.x < bounds.width)
                            {
                                px = bounds.xMin;
                            }
                            else
                            {
                                px = bounds.xMax;
                            }
                            points.Add(new Vector2(px, py));
                            points.Add(new Vector2(px, bounds.yMin));
                        }
                    }
                }
                if (closingUp)
                {
                    // newEdge's ends have already been added
                    return;
                }
                points.Add(newPoint);
            }
            if (newEdge.ClippedEnds[SideHelper.Other(newOrientation)] == null)
            {
                Debug.LogError("XXX: Null detected when there should be a Vector2!");
            }
            var newRightPoint = (Vector2)newEdge.ClippedEnds[SideHelper.Other(newOrientation)];
            if (!CloseEnough(points[0], newRightPoint))
            {
                points.Add(newRightPoint);
            }
        }
    }


    //	class PrivateConstructorEnforcer {}

    //	import flash.geom.Point;
    //	import flash.geom.Rectangle;

    static class BoundsCheck
    {
        public const int TOP = 1;
        public const int BOTTOM = 2;
        public const int LEFT = 4;
        public const int RIGHT = 8;

        /**
             * 
             * @param point
             * @param bounds
             * @return an int with the appropriate bits set if the Point lies on the corresponding bounds lines
             * 
             */
        public static int Check(Vector2 point, Rect bounds)
        {
            int value = 0;
            if (point.x == bounds.xMin)
            {
                value |= LEFT;
            }
            if (point.x == bounds.xMax)
            {
                value |= RIGHT;
            }
            if (point.y == bounds.yMin)
            {
                value |= TOP;
            }
            if (point.y == bounds.yMax)
            {
                value |= BOTTOM;
            }
            return value;
        }
    }
}