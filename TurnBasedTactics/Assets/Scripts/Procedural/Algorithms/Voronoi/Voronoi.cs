﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;

    public sealed class Voronoi : IDisposable
    {
        private Dictionary<Vector2, Site> _sitesIndexedByLocation;
        private List<Triangle> _triangles;
        private List<Edge> _edges;
        private List<uint> _colors;
        private List<Vector2> _points;

        public List<Vector2> Points
        {
            get { return this._points ?? (this._points = this.Sites.SiteCoords()); }
        }

        public SiteList Sites { get; private set; }


        // TODO generalize this so it doesn't have to be a rectangle;
        // then we can make the fractal voronois-within-voronois
        public Rect PlotBounds { get; private set; }

        public void Dispose()
        {
            int i, n;
            if (this.Sites != null)
            {
                this.Sites.Dispose();
                this.Sites = null;
            }
            if (this._triangles != null)
            {
                n = this._triangles.Count;
                for (i = 0; i < n; ++i)
                {
                    this._triangles[i].Dispose();
                }
                this._triangles.Clear();
                this._triangles = null;
            }
            if (this._edges != null)
            {
                n = this._edges.Count;
                for (i = 0; i < n; ++i)
                {
                    this._edges[i].Dispose();
                }
                this._edges.Clear();
                this._edges = null;
            }
            //			_plotBounds = null;
            this._sitesIndexedByLocation = null;
        }

        public Voronoi(List<Vector2> points, List<uint> colors, Rect plotBounds)
        {
            this._colors = colors;
            this.Init(points, plotBounds);
        }

        private void Init(List<Vector2> points, Rect plotBounds)
        {
            this.Sites = new SiteList();
            this._sitesIndexedByLocation = new Dictionary<Vector2, Site>(); // XXX: Used to be Dictionary(true) -- weak refs. 
            this.AddSites(points, this._colors);
            this.PlotBounds = plotBounds;
            this._triangles = new List<Triangle>();
            this._edges = new List<Edge>();
            this.FortunesAlgorithm();
        }

        private void AddSites(IList<Vector2> points, IList<uint> colors)
        {
            var length = points.Count;
            for (var i = 0; i < length; ++i)
            {
                this.AddSite(points[i], (colors != null) ? colors[i] : 0, i);
            }
        }

        private void AddSite(Vector2 p, uint color, int index)
        {
            if (this._sitesIndexedByLocation.ContainsKey(p))
                return; // Prevent duplicate site! (Adapted from https://github.com/nodename/as3delaunay/issues/1)
            var weight = UnityEngine.Random.value * 100f;
            var site = Site.Create(p, (uint) index, weight, color);
            this.Sites.Add(site);
            this._sitesIndexedByLocation[p] = site;
        }

        public List<Edge> Edges()
        {
            return this._edges;
        }

        public List<Vector2> Region(Vector2 p)
        {
            var site = this._sitesIndexedByLocation[p];
            return site == null ? new List<Vector2>() : site.Region(this.PlotBounds);
        }

        // TODO: bug: if you call this before you call region(), something goes wrong :(
        public List<Vector2> NeighborSitesForSite(Vector2 coord)
        {
            var points = new List<Vector2>();
            var site = this._sitesIndexedByLocation[coord];
            if (site == null)
            {
                return points;
            }
            var sites = site.NeighborSites();
            for (var nIndex = 0; nIndex < sites.Count; nIndex++)
            {
                var neighbor = sites[nIndex];
                points.Add(neighbor.Coord);
            }
            return points;
        }

        public List<Circle> Circles()
        {
            return this.Sites.Circles();
        }

        public List<LineSegment> VoronoiBoundaryForSite(Vector2 coord)
        {
            return DelaunayHelpers.VisibleLineSegments(DelaunayHelpers.SelectEdgesForSitePoint(coord, this._edges));
        }

        public List<LineSegment> DelaunayLinesForSite(Vector2 coord)
        {
            return DelaunayHelpers.DelaunayLinesForEdges(DelaunayHelpers.SelectEdgesForSitePoint(coord, this._edges));
        }

        public List<LineSegment> VoronoiDiagram()
        {
            return DelaunayHelpers.VisibleLineSegments(this._edges);
        }

        public List<LineSegment> DelaunayTriangulation( /*BitmapData keepOutMask = null*/)
        {
            return DelaunayHelpers.DelaunayLinesForEdges(DelaunayHelpers.SelectNonIntersectingEdges( /*keepOutMask,*/this._edges));
        }

        public List<LineSegment> Hull()
        {
            return DelaunayHelpers.DelaunayLinesForEdges(this.HullEdges());
        }

        private List<Edge> HullEdges()
        {
            return this._edges.FindAll(edge => edge.IsPartOfConvexHull());
        }

        public List<Vector2> HullPointsInOrder()
        {
            var hullEdges = this.HullEdges();

            var points = new List<Vector2>();
            if (hullEdges.Count == 0)
            {
                return points;
            }

            var reorderer = new EdgeReorderer(hullEdges, VertexOrSite.Site);
            hullEdges = reorderer.edges;
            var orientations = reorderer.EdgeOrientations;
            reorderer.Dispose();

            var n = hullEdges.Count;
            for (var i = 0; i < n; ++i)
            {
                var edge = hullEdges[i];
                var orientation = orientations[i];
                points.Add(edge.Site(orientation).Coord);
            }
            return points;
        }

        public List<LineSegment> SpanningTree(KruskalType type = KruskalType.Minimum /*, BitmapData keepOutMask = null*/)
        {
            var edges = DelaunayHelpers.SelectNonIntersectingEdges( /*keepOutMask,*/this._edges);
            var segments = DelaunayHelpers.DelaunayLinesForEdges(edges);
            return DelaunayHelpers.Kruskal(segments, type);
        }

        public List<List<Vector2>> Regions()
        {
            return this.Sites.Regions(this.PlotBounds);
        }

        public List<uint> SiteColors( /*BitmapData referenceImage = null*/)
        {
            return this.Sites.SiteColors( /*referenceImage*/);
        }

        /**
         * 
         * @param proximityMap a BitmapData whose regions are filled with the site index values; see PlanePointsCanvas::fillRegions()
         * @param x
         * @param y
         * @return coordinates of nearest Site to (x, y)
         * 
         */
        public Vector2? NearestSitePoint( /*BitmapData proximityMap,*/ float x, float y)
        {
            return this.Sites.NearestSitePoint( /*proximityMap,*/x, y);
        }

        public List<Vector2> SiteCoords()
        {
            return this.Sites.SiteCoords();
        }

        private Site _fortunesAlgorithmBottomMostSite;

        private void FortunesAlgorithm()
        {
            var newintstar = Vector2.zero; //Because the compiler doesn't know that it will have a value - Julian

            var dataBounds = this.Sites.GetSitesBounds();

            var sqrtNsites = (int) Mathf.Sqrt(this.Sites.Count + 4);
            var heap = new HalfedgePriorityQueue(dataBounds.y, dataBounds.height, sqrtNsites);
            var edgeList = new EdgeList(dataBounds.x, dataBounds.width, sqrtNsites);
            var halfEdges = new List<Halfedge>();
            var vertices = new List<Vertex>();

            this._fortunesAlgorithmBottomMostSite = this.Sites.Next();
            var newSite = this.Sites.Next();

            for (;;)
            {
                if (!heap.IsEmpty()) newintstar = heap.Min();

                Site bottomSite;
                Halfedge lbnd;
                Halfedge rbnd;
                Halfedge bisector;
                if (newSite != null && (heap.IsEmpty() || CompareByYThenX(newSite, newintstar) < 0))
                {
                    /* new site is smallest */
                    //trace("smallest: new site " + newSite);

                    // Step 8:
                    lbnd = edgeList.EdgeListLeftNeighbor(newSite.Coord); // the Halfedge just to the left of newSite
                    //trace("lbnd: " + lbnd);
                    rbnd = lbnd.EdgeListRightNeighbor; // the Halfedge just to the right
                    //trace("rbnd: " + rbnd);
                    bottomSite = this.FortunesAlgorithm_rightRegion(lbnd); // this is the same as leftRegion(rbnd)
                    // this Site determines the region containing the new site
                    //trace("new Site is in region of existing site: " + bottomSite);

                    // Step 9:
                    var edge = Edge.CreateBisectingEdge(bottomSite, newSite);
                    //trace("new edge: " + edge);
                    this._edges.Add(edge);

                    bisector = Halfedge.Create(edge, Side.Left);
                    halfEdges.Add(bisector);
                    // inserting two Halfedges into edgeList constitutes Step 10:
                    // insert bisector to the right of lbnd:
                    edgeList.Insert(lbnd, bisector);

                    // first half of Step 11:
                    {
                        var vertex = Vertex.Intersect(lbnd, bisector);
                        if (vertex != null)
                        {
                            vertices.Add(vertex);
                            heap.Remove(lbnd);
                            lbnd.Vertex = vertex;
                            lbnd.Ystar = vertex.Y + newSite.Dist(vertex);
                            heap.Insert(lbnd);
                        }
                    }

                    lbnd = bisector;
                    bisector = Halfedge.Create(edge, Side.Right);
                    halfEdges.Add(bisector);
                    // second Halfedge for Step 10:
                    // insert bisector to the right of lbnd:
                    edgeList.Insert(lbnd, bisector);

                    // second half of Step 11:
                    {
                        var vertex = Vertex.Intersect(bisector, rbnd);
                        if (vertex != null)
                        {
                            vertices.Add(vertex);
                            bisector.Vertex = vertex;
                            bisector.Ystar = vertex.Y + newSite.Dist(vertex);
                            heap.Insert(bisector);
                        }
                    }

                    newSite = this.Sites.Next();
                }
                else if (heap.IsEmpty() == false)
                {
                    /* intersection is smallest */
                    lbnd = heap.ExtractMin();
                    var llbnd = lbnd.EdgeListLeftNeighbor;
                    rbnd = lbnd.EdgeListRightNeighbor;
                    var rrbnd = rbnd.EdgeListRightNeighbor;
                    bottomSite = this.FortunesAlgorithm_leftRegion(lbnd);
                    var topSite = this.FortunesAlgorithm_rightRegion(rbnd);
                    // these three sites define a Delaunay triangle
                    // (not actually using these for anything...)
                    //_triangles.push(new Triangle(bottomSite, topSite, rightRegion(lbnd)));

                    var v = lbnd.Vertex;
                    v.SetIndex();
                    if (lbnd.LeftRight.HasValue) lbnd.Edge.SetVertex(lbnd.LeftRight.Value, v);
                    if (rbnd.LeftRight.HasValue) rbnd.Edge.SetVertex(rbnd.LeftRight.Value, v);
                    edgeList.Remove(lbnd);
                    heap.Remove(rbnd);
                    edgeList.Remove(rbnd);
                    var leftRight = Side.Left;
                    if (bottomSite.Y > topSite.Y)
                    {
                        var tempSite = bottomSite;
                        bottomSite = topSite;
                        topSite = tempSite;
                        leftRight = Side.Right;
                    }
                    var edge = Edge.CreateBisectingEdge(bottomSite, topSite);
                    this._edges.Add(edge);
                    bisector = Halfedge.Create(edge, leftRight);
                    halfEdges.Add(bisector);
                    edgeList.Insert(llbnd, bisector);
                    edge.SetVertex(leftRight.Other(), v);
                    {
                        var vertex = Vertex.Intersect(llbnd, bisector);
                        if (vertex != null)
                        {
                            vertices.Add(vertex);
                            heap.Remove(llbnd);
                            llbnd.Vertex = vertex;
                            llbnd.Ystar = vertex.Y + bottomSite.Dist(vertex);
                            heap.Insert(llbnd);
                        }
                    }
                    {
                        var vertex = Vertex.Intersect(bisector, rrbnd);
                        if (vertex != null)
                        {
                            vertices.Add(vertex);
                            bisector.Vertex = vertex;
                            bisector.Ystar = vertex.Y + bottomSite.Dist(vertex);
                            heap.Insert(bisector);
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            // heap should be empty now
            heap.Dispose();
            edgeList.Dispose();

            for (int hIndex = 0; hIndex < halfEdges.Count; hIndex++)
            {
                var halfEdge = halfEdges[hIndex];
                halfEdge.ReallyDispose();
            }
            halfEdges.Clear();

            // we need the vertices to clip the edges
            for (int eIndex = 0; eIndex < this._edges.Count; eIndex++)
            {
                var edge = this._edges[eIndex];
                edge.ClipVertices(this.PlotBounds);
            }
            // but we don't actually ever use them again!
            for (int vIndex = 0; vIndex < vertices.Count; vIndex++)
            {
                var vertex = vertices[vIndex];
                vertex.Dispose();
            }
            vertices.Clear();
        }

        private Site FortunesAlgorithm_leftRegion(Halfedge he)
        {
            var edge = he.Edge;
            if (edge == null) return this._fortunesAlgorithmBottomMostSite;
            return he.LeftRight.HasValue ? edge.Site(he.LeftRight.Value) : null;
        }

        private Site FortunesAlgorithm_rightRegion(Halfedge he)
        {
            var edge = he.Edge;
            if (edge == null)
            {
                return this._fortunesAlgorithmBottomMostSite;
            }
            return he.LeftRight.HasValue ? edge.Site(he.LeftRight.Value.Other()) : null;
        }

        public static int CompareByYThenX(Site s1, Site s2)
        {
            if (s1.Y < s2.Y) return -1;
            if (s1.Y > s2.Y) return 1;
            if (s1.X < s2.X) return -1;
            if (s1.X > s2.X) return 1;
            return 0;
        }

        public static int CompareByYThenX(Site s1, Vector2 s2)
        {
            if (s1.Y < s2.y) return -1;
            if (s1.Y > s2.y) return 1;
            if (s1.X < s2.x) return -1;
            if (s1.X > s2.x) return 1;
            return 0;
        }

        public void LloydRelaxation(int nbIterations)
        {
            // Reapeat the whole process for the number of iterations asked
            for (int i = 0; i < nbIterations; i++)
            {
                var newPoints = new List<Vector2>();
                // Go thourgh all sites
                this.Sites.ResetListIndex();
                Site site = this.Sites.Next();

                while (site != null)
                {
                    // Loop all corners of the site to calculate the centroid
                    var region = site.Region(this.PlotBounds);
                    if (region.Count < 1)
                    {
                        site = this.Sites.Next();
                        continue;
                    }

                    var centroid = Vector2.zero;
                    float signedArea = 0;
                    float x0;
                    float y0;
                    float x1;
                    float y1;
                    float a;
                    // For all vertices except last
                    for (int j = 0; j < region.Count - 1; j++)
                    {
                        x0 = region[j].x;
                        y0 = region[j].y;
                        x1 = region[j + 1].x;
                        y1 = region[j + 1].y;
                        a = x0 * y1 - x1 * y0;
                        signedArea += a;
                        centroid.x += (x0 + x1) * a;
                        centroid.y += (y0 + y1) * a;
                    }
                    // Do last vertex
                    x0 = region[region.Count - 1].x;
                    y0 = region[region.Count - 1].y;
                    x1 = region[0].x;
                    y1 = region[0].y;
                    a = x0 * y1 - x1 * y0;
                    signedArea += a;
                    centroid.x += (x0 + x1) * a;
                    centroid.y += (y0 + y1) * a;

                    signedArea *= 0.5f;
                    centroid.x /= (6 * signedArea);
                    centroid.y /= (6 * signedArea);
                    // Move site to the centroid of its Voronoi cell
                    newPoints.Add(centroid);
                    site = this.Sites.Next();
                }

                // Between each replacement of the cendroid of the cell,
                // we need to recompute Voronoi diagram:
                Rect origPlotBounds = this.PlotBounds;
                this.Dispose();
                this.Init(newPoints, origPlotBounds);
            }
        }
    }
}