﻿namespace Procedural.Algorithms
{
    using UnityEngine;
    using System.Collections.Generic;

    public sealed class Vertex : ICoord
    {
        public static readonly Vertex Infinity = new Vertex(float.NaN, float.NaN);

        private static Stack<Vertex> _pool = new Stack<Vertex>();

        private static Vertex Create(float x, float y)
        {
            if (float.IsNaN(x) || float.IsNaN(y))
            {
                return Infinity;
            }
            if (_pool.Count > 0)
            {
                return _pool.Pop().Init(x, y);
            }
            return new Vertex(x, y);
        }


        private static int _nvertices;

        private Vector2 _coord;

        public Vector2 Coord
        {
            get { return this._coord; }
        }

        private int _vertexIndex;

        public int vertexIndex
        {
            get { return this._vertexIndex; }
        }

        public Vertex(float x, float y)
        {
            this.Init(x, y);
        }

        private Vertex Init(float x, float y)
        {
            this._coord = new Vector2(x, y);
            return this;
        }

        public void Dispose()
        {
            _pool.Push(this);
        }

        public void SetIndex()
        {
            this._vertexIndex = _nvertices++;
        }

        public override string ToString()
        {
            return "Vertex (" + this._vertexIndex + ")";
        }

        /**
         * This is the only way to make a Vertex
         * 
         * @param halfedge0
         * @param halfedge1
         * @return 
         * 
         */
        public static Vertex Intersect(Halfedge halfedge0, Halfedge halfedge1)
        {
            Edge edge;
            Halfedge halfedge;

            var edge0 = halfedge0.Edge;
            var edge1 = halfedge1.Edge;
            if (edge0 == null || edge1 == null)
            {
                return null;
            }
            if (edge0.RightSite == edge1.RightSite)
            {
                return null;
            }

            float determinant = edge0.a * edge1.b - edge0.b * edge1.a;
            if (-1.0e-10 < determinant && determinant < 1.0e-10)
            {
                // the edges are parallel
                return null;
            }

            float intersectionX = (edge0.c * edge1.b - edge1.c * edge0.b) / determinant;
            float intersectionY = (edge1.c * edge0.a - edge0.c * edge1.a) / determinant;

            if (Voronoi.CompareByYThenX(edge0.RightSite, edge1.RightSite) < 0)
            {
                halfedge = halfedge0;
                edge = edge0;
            }
            else
            {
                halfedge = halfedge1;
                edge = edge1;
            }
            bool rightOfSite = intersectionX >= edge.RightSite.X;
            if (rightOfSite && halfedge.LeftRight == Side.Left
                || !rightOfSite && halfedge.LeftRight == Side.Right)
            {
                return null;
            }

            return Create(intersectionX, intersectionY);
        }

        public float X
        {
            get { return this._coord.x; }
        }

        public float Y
        {
            get { return this._coord.y; }
        }

    }
}