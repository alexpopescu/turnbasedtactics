﻿namespace Procedural.Algorithms
{
using System;
using System.Collections.Generic;

    public sealed class Triangle : IDisposable
    {
        private List<Site> _sites;

        public List<Site> Sites
        {
            get { return this._sites; }
        }

        public Triangle(Site a, Site b, Site c)
        {
            this._sites = new List<Site> {a, b, c};
        }

        public void Dispose()
        {
            this._sites.Clear();
            this._sites = null;
        }
    }
}