﻿namespace Procedural.Algorithms
{
    using UnityEngine;

    public interface ICoord
    {
        Vector2 Coord { get; }
    }
}