﻿namespace Procedural.Algorithms
{
    using System.Collections.Generic;
    using UnityEngine;

    public class SegmentScrambler
    {
        private readonly float _minPointDistance;
        private readonly float _segmentSkewFactor;

        public SegmentScrambler(float minPointDistance, float segmentSkewFactor)
        {
            this._minPointDistance = minPointDistance;
            this._segmentSkewFactor = segmentSkewFactor;
        }

        public List<Vector2> ScrambleLine(Vector2 p1, Vector2 p2)
        {
            var pqDiff = new Vector2(p2.x - p1.x, p2.y - p1.y);
            var perp = new Vector2(this._segmentSkewFactor * pqDiff.y, -this._segmentSkewFactor * pqDiff.x);
            var pqMed = 0.5f * (p1 + p2);

            var points = new List<Vector2>();

            points.Add(p1);
            points.AddRange(this.Subdivide(p1, pqMed + perp, p2, pqMed - perp));
            points.Add(p2);
            return points;
        }

        private IEnumerable<Vector2> Subdivide(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
        {
            var points = new List<Vector2>();

            if (Vector2.Distance(a, c) < this._minPointDistance || Vector2.Distance(b, d) < this._minPointDistance)
            {
                points.Add(c);
            }
            else
            {
                // Subdivide the quadrilateral
                float p = Random.Range(0.1f, 0.9f); // vertical (along A-D and B-C)
                float q = Random.Range(0.1f, 0.9f); // horizontal (along A-B and D-C)

                // Midpoints
                var e = Interpolate(a, d, p);
                var f = Interpolate(b, c, p);
                var g = Interpolate(a, b, q);
                var i = Interpolate(d, c, q);

                // Central point
                var h = Interpolate(e, f, q);

                // Divide the quad into subquads, but meet at H
                float s = Random.Range(-0.4f, 0.4f);
                float t = Random.Range(-0.4f, 0.4f);

                points.AddRange(this.Subdivide(a, Interpolate(g, b, s), h, Interpolate(e, d, t)));
                points.Add(h);
                points.AddRange(this.Subdivide(h, Interpolate(f, c, s), c, Interpolate(i, d, t)));
            }

            return points;
        }

        private static Vector2 Interpolate(Vector2 p, Vector2 q, float f)
        {
            return new Vector2(p.x * (1 - f) + q.x * f, p.y * (1 - f) + q.y * f);
        }

    }
}
