﻿namespace Procedural.Algorithms.Improv
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using Random = UnityEngine.Random;

    public class CannotGenerateTextException : Exception
    {
    }

    public class Sentence
    {
        public Snippet[] Snippets { get; private set; }

        public Sentence(string sentence)
        {
            this.ParseTextSentence(sentence);
        }

        public string Generate(Dictionary<string, Group[]> data, Model model)
        {
            string ret = string.Empty;
            foreach (var snippet in this.Snippets)
            {
                switch (snippet.Type)
                {
                    case SnippetType.Literal:
                        ret += snippet.Value;
                        break;
                    case SnippetType.Recursive:
                        try
                        {
                            ret += GenerateForRandomGroup(data, model, snippet.Value);
                        }
                        catch (CannotGenerateTextException ex)
                        {
                            ret += string.Format("{{recursive:{0}}}", snippet.Value);
                        }
                        break;
                    case SnippetType.Model:
                        if (!model.Properties.ContainsKey(snippet.Value))
                        {
                            try
                            {
                                ret += GenerateForRandomGroup(data, model, snippet.Value);
                            }
                            catch (CannotGenerateTextException ex)
                            {
                                ret += string.Format("{{model:{0}}}", snippet.Value);
                            }
                            break;
                        }

                        ret += model.Properties[snippet.Value];
                        break;
                    case SnippetType.Optional:
                        if (Random.Range(0f, 1f) > 0.5f)
                            ret += snippet.Value;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return ret.Trim();
        }

        private static string GenerateForRandomGroup(Dictionary<string, Group[]> data, Model model, string text)
        {
            if (!data.ContainsKey(text)) throw new CannotGenerateTextException();
            var groups = data[text].Where(x => x.Tags.Matches(model.Tags)).ToArray();

            if (groups.Length == 0) throw new CannotGenerateTextException();
            return groups[Random.Range(0, groups.Length)].Generate(data, model);
        }

        private void ParseTextSentence(string sentence)
        {
            var chunks = new List<Snippet>();
            string currentChunk = string.Empty;
            int currentIndex = 0;
            int sentenceLength = sentence.Length;
            var currentType = SnippetType.Literal;

            while (currentIndex < sentenceLength)
            {
                char cChar = sentence[currentIndex];
                switch (cChar)
                {
                    case '{':
                        if (currentChunk.Length > 0)
                        {
                            chunks.Add(new Snippet(currentType, currentChunk));
                            currentChunk = string.Empty;
                        }
                        currentType = SnippetType.Model;
                        break;
                    case '[':
                        if (currentChunk.Length > 0)
                        {
                            chunks.Add(new Snippet(currentType, currentChunk));
                            currentChunk = string.Empty;
                        }
                        currentType = SnippetType.Optional;
                        break;
                    case '}':
                    case ']':
                        if (currentChunk.Length > 0)
                        {
                            chunks.Add(new Snippet(currentType, currentChunk));
                            currentChunk = string.Empty;
                            currentType = SnippetType.Literal;
                        }
                        break;
                    case ':':
                        if (currentType == SnippetType.Model && currentChunk.Length == 0)
                        {
                            currentType = SnippetType.Recursive;
                        }
                        break;
                    default:
                        currentChunk += cChar;
                        break;
                }

                currentIndex++;
            }

            if (currentChunk.Length > 0)
            {
                chunks.Add(new Snippet(currentType, currentChunk));
            }
            this.Snippets = chunks.ToArray();
        }
    }
}
