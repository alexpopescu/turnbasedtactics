﻿namespace Procedural.Algorithms.Improv
{
    using System.Linq;

    public class Tag
    {
        public string[] Values { get; }

        public Tag(params string[] values)
        {
            this.Values = values;
        }

        public bool IsIncludedIn(Tag other)
        {
            if (other.Values.Length < this.Values.Length)
                return false;

            return !this.Values.Where((t, i) => t != other.Values[i]).Any();
        }
    }
}
