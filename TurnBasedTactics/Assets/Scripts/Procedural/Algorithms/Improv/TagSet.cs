﻿namespace Procedural.Algorithms.Improv
{
    using System.Linq;

    public class TagSet
    {
        public Tag[] Tags { get; }

        public TagSet(params Tag[] tags)
        {
            this.Tags = tags;
        }

        public bool Matches(TagSet modelTags)
        {
            return this.Tags.All(t => modelTags.Tags.Any(t.IsIncludedIn));
        }
    }
}
