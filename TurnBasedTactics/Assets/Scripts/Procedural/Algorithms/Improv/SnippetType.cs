﻿namespace Procedural.Algorithms.Improv
{
    public enum SnippetType
    {
        Literal,
        Recursive,
        Model,
        Optional
    }
}
