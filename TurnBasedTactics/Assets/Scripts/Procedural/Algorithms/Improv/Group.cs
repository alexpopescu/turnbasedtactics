﻿namespace Procedural.Algorithms.Improv
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class Group
    {
        public TagSet Tags { get; }
        public Sentence[] Sentences { get; }

        public Group(TagSet tags, IEnumerable<string> sentences)
        {
            this.Tags = tags;
            this.Sentences = sentences.Select(x => new Sentence(x)).ToArray();
        }

        public string Generate(Dictionary<string, Group[]> data, Model model)
        {
            var sentence = this.Sentences[Random.Range(0, this.Sentences.Length)];
            return sentence.Generate(data, model);
        }
    }
}
