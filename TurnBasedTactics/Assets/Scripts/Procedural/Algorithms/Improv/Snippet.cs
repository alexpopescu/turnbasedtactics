﻿namespace Procedural.Algorithms.Improv
{
    public class Snippet
    {
        public SnippetType Type { get; }
        public string Value { get; }

        public Snippet(SnippetType type, string value)
        {
            this.Type = type;
            this.Value = value;
        }
    }
}
