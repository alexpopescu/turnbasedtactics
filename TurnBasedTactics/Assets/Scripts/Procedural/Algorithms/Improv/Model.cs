﻿namespace Procedural.Algorithms.Improv
{
    using System.Collections.Generic;

    public class Model
    {
        public Dictionary<string, string> Properties { get; }
        public TagSet Tags { get; }

        public Model(Dictionary<string, string> properties, TagSet tags)
        {
            this.Properties = properties;
            this.Tags = tags;
        }
    }
}
