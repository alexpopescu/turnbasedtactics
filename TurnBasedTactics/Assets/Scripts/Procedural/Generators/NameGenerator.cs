﻿namespace Procedural.Generators
{
    using System.Collections.Generic;
    using System.Linq;
    using Algorithms.Improv;
    using MapGen;
    using UnityEngine;

    public static class NameGenerator
    {
        public static string GenerateLandformName(PhraseData phraseData, Landform landform)
        {
            var modelProps = new Dictionary<string, string>();
            if (landform.Fauna.Count > 0)
            {
                string animal = landform.Fauna.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
                modelProps.Add("animal", animal);
            }
            if (landform.Flora.Count > 0)
            {
                string plant = landform.Flora.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
                modelProps.Add("plant", plant);
            }

            string size = "small";
            if (landform.Size > 1)
            {
                size = landform.Size > 4 ? "large" : "medium";
            }

            var modelAnimal = new Model(modelProps, 
                new TagSet(
                    new Tag("class", landform.Type.BaseName, size),
                    new Tag("feature", "animal")));
            var modelPlant = new Model(modelProps, 
                new TagSet(
                    new Tag("class", landform.Type.BaseName, size), 
                    new Tag("feature", "plant")));

            string[] options = new string[6];
            for (int i = 0; i < 3; i++)
            {
                options[i*2] = phraseData.Generate("title", modelAnimal);
                options[i*2+1] = phraseData.Generate("title", modelPlant);
            }

            string landformName = options[Random.Range(0, options.Length)];
            return landformName;
        }
    }
}