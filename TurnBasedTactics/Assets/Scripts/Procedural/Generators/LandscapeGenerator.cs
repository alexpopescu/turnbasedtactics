﻿namespace Procedural.Generators
{
    using System.Collections.Generic;
    using System.Linq;
    using Algorithms;
    using MapGen;
    using UnityEngine;

    public static class LandscapeGenerator
    {
        public static IEnumerable<Region> GenerateRegions(
            int width, int height, int regionCount, BiomeDefinition[] biomeDefinition)
        {
            Debug.Log("----------> GENERATING REGIONS");

            // generate areas and borders
            var colors = new List<uint>();
            var points = new List<Vector2>();

            for (int i = 0; i < regionCount; i++)
            {
                colors.Add(0);
                points.Add(new Vector2(
                    Random.Range(0, width),
                    Random.Range(0, height))
                );
            }

            var voronoi = new Voronoi(points, colors, new Rect(0, 0, width, height));
            voronoi.LloydRelaxation(10);

            // generate heights
            var altitudeGenerator = new PerlinGenerator(width, height, 2f);
            var humidityGenerator = new PerlinGenerator(width, height, 4f);
            var altitudeRange = new Range(1f, 0f);
            var humidityRange = new Range(1f, 0f);

            // define regions
            var regions = new Dictionary<Vector2, Region>(regionCount);
            foreach (var site in voronoi.Sites)
            {
                float altitude = altitudeGenerator[site.X, site.Y];
                altitudeRange.Stretch(altitude);

                float humidity = humidityGenerator[site.X, site.Y];
                humidityRange.Stretch(humidity);

                var region = new Region(site.Coord)
                {
                    Altitude = altitude,
                    Humidity = humidity
                };
                regions.Add(region.Location, region);
            }

            // normalize heights and humidity
            float altitudeDivisor = altitudeRange.Start + altitudeRange.End;
            float humidityDivisor = humidityRange.Start + humidityRange.End;
            foreach (var region in regions.Values)
            {
                region.Altitude /= altitudeDivisor;
                region.Humidity /= humidityDivisor;
                region.BiomeDefinition = biomeDefinition.FirstOrDefault(
                    biome => biome.Contains(region.Altitude, region.Humidity));
            }

            // define borders
            foreach (var site in voronoi.Sites)
            {
                var region = regions[site.Coord];
                var borders = new List<Border>();
                foreach (var edge in site.Edges)
                {
                    var neighbour = regions[site.NeighborSite(edge).Coord];
                    var v0 = edge.VoronoiEdge().P0;
                    var v1 = edge.VoronoiEdge().P1;
                    if (!v0.HasValue || !v1.HasValue) continue;
                    borders.Add(new Border(region, neighbour, v0.Value, v1.Value));
                }

                region.Borders = borders.ToArray();
            }

            return regions.Values;
        }
        
        public static IEnumerable<Lineform> GenerateRivers(IEnumerable<Region> regions, Range riverSourceAltitudeRange)
        {
            Debug.Log("----------> GENERATING RIVERS");
            var highSites = regions.Where(region => riverSourceAltitudeRange.Contains(region.Altitude)).ToArray();
            Debug.Log(string.Format("Found {0} high sites", highSites.Length));
            var rivers = new List<List<Region>>();
        
            // compute rivers
            foreach (var region in highSites)
            {
                var allRivers = GetRiverFlowFromRegion(region);
                foreach (var river in allRivers)
                {
                    int idx = rivers.FindIndex(x => x[0] == river[0] && x[x.Count - 1] == river[river.Count - 1]);
                    if (idx == -1) rivers.Add(river);
                    else if (rivers[idx].Count > river.Count) rivers[idx] = river;
                } 
            }
            Debug.Log(string.Format("Found {0} total rivers", rivers.Count));
            Debug.Log(string.Format("Selecting {0} rivers", highSites.Length));
            var remainingRivers = rivers.GetRandomElements(highSites.Length);
        
            for (int r = 0; r < remainingRivers.Count; r++)
            {
                string riverName = string.Format("River{0}", r);
                var river = new Lineform(riverName, LineformType.River, remainingRivers[r].Select(p => p.Location).ToArray());
                yield return river;
            }
        }

        private static List<List<Region>> GetRiverFlowFromRegion(Region source)
        {
            if (source.IsWater)
            {
                return new List<List<Region>> { new List<Region> { source } };
            }
            
            var lowerNeighbours = source.GetBorderingRegions().Where(x => x.CompareHeight(source) < 0);
            var potentialFromHere = lowerNeighbours.SelectMany(GetRiverFlowFromRegion).ToList();
            var riversFromHere = potentialFromHere.Where(x => x != null && x.Count > 0 && !x.Contains(source)).ToList();
            foreach (var river in riversFromHere)
            {
                river.Insert(0, source);
            }
            return riversFromHere;
        }
        
        public static IEnumerable<Landform> GenerateLandforms(Region[] regions)
        {
            Debug.Log("----------> GENERATING LANDFORM GROUPS");

            var landformCounts = new Dictionary<string, int>();

            while (true)
            {
                var startingRegion = regions.FirstOrDefault(s => s.Landform == null);
                if (startingRegion == null) yield break;

                var biomeDef = startingRegion.BiomeDefinition;
                if (!landformCounts.ContainsKey(biomeDef.Name))
                {
                    landformCounts.Add(biomeDef.Name, 0);
                }

                string groupKey = string.Format("{0}{1}", biomeDef.Name, ++landformCounts[biomeDef.Name]);
                var landform = new Landform(groupKey, biomeDef);

                foreach (var region in GetRegionLandform(startingRegion))
                {
                    region.Landform = landform;
                    landform.Regions.Add(region);
                }

                yield return landform;
            }
        }

        private static IEnumerable<Region> GetRegionLandform(Region region)
        {
            if (region.Landform != null) yield break;
            yield return region;
            
            foreach (var neighbour in region.GetBorderingRegions())
            {
                if (region.BiomeDefinition != neighbour.BiomeDefinition) continue;
                foreach (var region1 in GetRegionLandform(neighbour))
                {
                    yield return region1;
                }
            }
        }
    }
}