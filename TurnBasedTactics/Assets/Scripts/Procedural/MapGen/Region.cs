﻿namespace Procedural.MapGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class Region : IEquatable<Region>
    {
        private BiomeDefinition _biomeDefinition;
        public float Altitude { get; set; }
        public float Humidity { get; set; }
        public Vector2 Location { get; private set; }

        public Dictionary<string, float> Fauna { get; private set; }
        public Dictionary<string, float> Flora { get; private set; }

        public bool IsWater => this.BiomeDefinition.BaseName == "water";

        private float _foodFactor = float.NaN;
        public float FoodFactor
        {
            get
            {
                if (float.IsNaN(this._foodFactor))
                {
                    this._foodFactor =
                        this.Flora.Sum(x => this.BiomeDefinition.IndexedFlora[x.Key].FoodFactor * x.Value) +
                        this.Fauna.Sum(x => this.BiomeDefinition.IndexedFauna[x.Key].FoodFactor * x.Value);
                }

                return this._foodFactor;
            }
        }

        private float _fearFactor = float.NaN;
        public float FearFactor
        {
            get
            {
                if (float.IsNaN(this._fearFactor))
                {
                    this._fearFactor =
                        this.Flora.Sum(x => this.BiomeDefinition.IndexedFlora[x.Key].FearFactor * x.Value) +
                        this.Fauna.Sum(x => this.BiomeDefinition.IndexedFauna[x.Key].FearFactor * x.Value);
                }

                return this._fearFactor;
            }
        }

        public BiomeDefinition BiomeDefinition
        {
            get => this._biomeDefinition;
            set
            {
                this._biomeDefinition = value;

                float ambientCoefficient = this.Humidity * (1 - this.Altitude);
                this.GrassCoverage = ambientCoefficient * this._biomeDefinition.GrassMultiplier;
                this.PlantCoverage = ambientCoefficient * this._biomeDefinition.PlantMultiplier;
                this.TreeCoverage = ambientCoefficient * this._biomeDefinition.TreeMultiplier;
            }
        }

        public Landform Landform { get; set; }
        public Border[] Borders { get; set; }
        public Node Node { get; set; }

        public float GrassCoverage { get; private set; }
        public float PlantCoverage { get; private set; }
        public float TreeCoverage { get; private set; }

        public Region(Vector2 location)
        {
            this.Location = location;
            this.Fauna = new Dictionary<string, float>();
            this.Flora = new Dictionary<string, float>();
        }

        public IEnumerable<Region> GetBorderingRegions()
        {
            return this.Borders.Select(border => border.GetOtherRegion(this));
        }

        public Border GetBorderWith(Region other)
        {
            return this.Borders.FirstOrDefault(x => x.GetOtherRegion(this) == other);
        }
        public int CompareHeight(Region otherSite)
        {
            return this.Altitude.CompareTo(otherSite.Altitude);
        }

        public void GenerateFauna()
        {
            foreach (var fauna in Generate(this.BiomeDefinition.Fauna))
            {
                this.Fauna.Add(fauna.Key, fauna.Value);
            }
        }

        public void GenerateFlora()
        {
            foreach (var flora in Generate(this.BiomeDefinition.Flora))
            {
                this.Flora.Add(flora.Key, flora.Value);
            }
        }

        private static IEnumerable<KeyValuePair<string, float>> Generate(IEnumerable<OrganismDefinition> organisms)
        {
            float threshold = 0f;
            foreach (var organism in organisms.OrderBy(x => Random.value))
            {
                if (Random.value > threshold)
                {
                    float weight = Random.value * organism.OccurrenceMean + organism.OccurrenceStdDev;
                    yield return new KeyValuePair<string, float>(organism.name, weight);
                    threshold += 0.4f;
                }
                else
                {
                    threshold -= 0.2f;
                }
            }
        }

        #region IEquatable
        public bool Equals(Region other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.Location.Equals(other.Location);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Region)obj);
        }

        public override int GetHashCode()
        {
            return this.Location.GetHashCode();
        }

        public static bool operator ==(Region left, Region right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Region left, Region right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}
