﻿namespace Procedural.MapGen
{
    using System;
    using UnityEngine;

    public class Lineform : IEquatable<Lineform>
    {
        public string Name { get; private set; }
        public LineformType Type { get; private set; }
        public Vector2[] Points { get; private set; }

        public Lineform(string name, LineformType type, params Vector2[] points)
        {
            this.Name = name;
            this.Type = type;
            this.Points = points;
        }

        #region IEquatable
        public bool Equals(Lineform other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(this.Name, other.Name) && this.Type == other.Type;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Lineform)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((this.Name != null ? this.Name.GetHashCode() : 0) * 397) ^ (int)this.Type;
            }
        }

        public static bool operator ==(Lineform left, Lineform right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Lineform left, Lineform right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}
