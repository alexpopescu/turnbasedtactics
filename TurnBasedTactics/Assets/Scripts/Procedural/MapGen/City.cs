﻿namespace Procedural.MapGen
{
    public class City : Node
    {
        public int Population { get; set; }

        public Culture Culture { get; set; }

        public City(string name, Region region) : base(name, region)
        {
        }
    }
}
