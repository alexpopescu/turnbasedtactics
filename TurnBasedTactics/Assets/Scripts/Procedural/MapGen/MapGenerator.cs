﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Procedural.Algorithms;
using Procedural.Algorithms.Improv;
using Procedural.Generators;
using Procedural.MapGen;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class MapGenerator : MonoBehaviour
{
    public BiomeDefinition[] BiomeDefinitions;
    public Text Tooltip;

    public int PointCount = 120;
    public int MapWidth = 1920;
    public int MapHeight = 1200;
    public float MinBorderSegmentationLength = 1.0f;
    public float SegmentSkewFactor = 0.4f;

    [IntervalRange(0f, 1f)]
    public Range RiverSourceRange = new Range(0.8f, 1f);
    public float RiverDensity = 0.2f;

    public float MinCityDistance = 100f;
    [IntervalRange(0f, 1f)]
    public Range CityHeightRange = new Range(0.3f, 0.6f);
    [IntervalRange(0f, 1f)]
    public Range CityHumidityRange = new Range(0.2f, 0.8f);

    public ProbabilityDistribution PopulationStartDistribution = new ProbabilityDistribution(1f, 0.4f);
    public ProbabilityDistribution PopulationGrowthDistribution = new ProbabilityDistribution(3f, 1f);

    private DataController _dataController;
    private SpriteRenderer _waterBordersRenderer;
    private SpriteRenderer _riversRenderer;
    private GameObject _biomeObjectContainer;
    private GameObject _cityObjectContainer;

    private World _world;

    private Object _city;

    void Awake()
    {
        this.Tooltip.enabled = false;
        this._city = Resources.Load("Map/city", typeof(GameObject));
        this._world = new World(this.MapWidth, this.MapHeight);
    }

    void Start()
    {
        this._dataController = this.GetComponent<DataController>();

        this.GenerateMap();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F5))
        {
            this.GenerateMap();
        }
        if (Input.GetKeyUp(KeyCode.F6))
        {
            var modelProps = new Dictionary<string, string>
            {
                {"animal", "seahorse"},
                {"plant", "anemone"}
            };
            var model_animal = new Model(modelProps, new TagSet(
                new Tag("class", "water", "small"), new Tag("feature", "animal")));
            var model_tree = new Model(modelProps, new TagSet(
                new Tag("class", "forest"), new Tag("feature", "plant")));
            for (int i = 0; i < 5; i++)
            {
                Debug.Log(this._dataController.PhraseData.Generate("title", model_animal));
                Debug.Log(this._dataController.PhraseData.Generate("title", model_tree));
            }

            Debug.Log(this._dataController.PhraseData.Generate("root", new Model(new Dictionary<string, string>(), new TagSet())));
        }
    }

    private void GenerateMap()
    {
        this._world.Clear();
        
        var regions = LandscapeGenerator.GenerateRegions(this.MapWidth, this.MapHeight, this.PointCount, this.BiomeDefinitions);
        foreach (var region in regions)
        {
            region.GenerateFauna();
            region.GenerateFlora();
            this._world.AddRegion(region);
        }

        var rivers = LandscapeGenerator.GenerateRivers(this._world.RegionList, this.RiverSourceRange);
        foreach (var river in rivers) this._world.AddRiver(river);

        this.GenerateCities();

        this.GeneratePopulation();
        this.GenerateCulture();
        // this.GenerateReligion();
        // this.GenerateHistory();

        var landforms = LandscapeGenerator.GenerateLandforms(this._world.RegionList.ToArray()).ToArray();
        foreach (var landform in landforms)
        {
            landform.ComputeFaunaAndFlora();
            string newName = null;
            while (string.IsNullOrEmpty(newName) || landforms.Any(lf => lf.Name == newName))
            {
                newName = NameGenerator.GenerateLandformName(this._dataController.PhraseData, landform);
                Debug.Log($"--> Rename: {landform.Name} -> {newName}");
            }
            landform.Name = newName;
            this._world.AddLandform(landform);
        }
        
        this.GenerateBiomeObjects();

        this.DrawWaterBorders();
        this.DrawRivers();
    }

    #region Map Generation
    private void GenerateBiomeObjects()
    {
        Debug.Log("Generating BiomeDefinition Objects");
        if (this._biomeObjectContainer == null)
        {
            this._biomeObjectContainer = new GameObject("BiomeDefinition Object Container");
            this._biomeObjectContainer.transform.SetParent(this.transform, false);
        }
        else
        {
            this._biomeObjectContainer.transform.Clear();
        }

        var offset = this.transform.position;
        foreach (var kvp in this._world.Regions)
        {
            var obj = kvp.Value.BiomeDefinition.ResourceFiles;
            if (obj.Length > 0)
            {
                var x = Instantiate(obj[0], new Vector3(kvp.Key.x, kvp.Key.y) - offset, Quaternion.identity) as GameObject;
                if (x != null)
                {
                    var info = x.GetComponent<MapLandformInfo>();
                    if (info != null)
                    {
                        info.Name = kvp.Value.Landform.Name;
                        info.Tooltip = this.Tooltip;
                    }
                    x.transform.SetParent(this._biomeObjectContainer.transform, false);
                }
            }
        }
        
        if (this._cityObjectContainer == null)
        {
            this._cityObjectContainer = new GameObject("City Object Container");
            this._cityObjectContainer.transform.SetParent(this.transform, false);
        }
        else
        {
            this._cityObjectContainer.transform.Clear();
        }

        foreach (var city in this._world.CityList)
        {
            var x = Instantiate(this._city, new Vector3(city.Location.x, city.Location.y) - offset, Quaternion.identity) as GameObject;
            if (x == null) continue;

            var info = x.GetComponent<MapLandformInfo>();
            if (info != null)
            {
                info.Name = city.Name;
                info.Tooltip = this.Tooltip;
            }
            x.transform.SetParent(this._biomeObjectContainer.transform, false);
        }
    }

    private void GenerateCities()
    {
        Debug.Log("Generating Cities");
        int cityCounter = 0;
        while (true)
        {
            var region = this._world.RegionList.FirstOrDefault(
                r => this.CityHeightRange.Contains(r.Altitude)
                     && this.CityHumidityRange.Contains(r.Humidity)
                     && this._world.CityList.All(c => Vector2.Distance(c.Location, r.Location) >= this.MinCityDistance));
            if (region == null) break;
            var city = new City(string.Format("City{0}", ++cityCounter), region);
            region.Node = city;
            this._world.AddCity(city);
        }
    }

    private void GeneratePopulation()
    {
        foreach (var city in this._world.CityList)
        {
            float totalFood = city.Region.FoodFactor;
            totalFood += city.Region.GetBorderingRegions().Sum(x => x.FoodFactor);
            city.Population = Mathf.FloorToInt(totalFood * this.PopulationStartDistribution.GetObservation());
            Debug.Log(string.Format("{0} has population {1}", city.Name, city.Population));
        }
    }

    private void GenerateCulture()
    {
        foreach (var city in this._world.CityList)
        {
            city.Culture = Culture.GenerateRandom();
            Debug.Log(string.Format("{0} has culture {1}", city.Name, city.Culture));
        }
    }
    #endregion

    #region Drawing Functions
    private void DrawWaterBorders()
    {
        Debug.Log("Drawing Water Points");
        // get all segments between water and land
        var borders = new List<Tuple<Vector2, Vector2>>();
        foreach (var region in this._world.RegionList)
        {
            if (region.IsWater)
            {
                foreach (var border in region.Borders)
                {
                    var otherRegion = border.GetOtherRegion(region);
                    if (otherRegion.IsWater) continue;

                    var borderVector = new Tuple<Vector2, Vector2>(border.Point1, border.Point2);
                    if (!borders.Contains(borderVector)) borders.Add(borderVector);
                }
            }
        }

        // draw noisy segments on texture
        if (this._waterBordersRenderer == null)
        {
            var gObj = new GameObject("Points Renderer");
            gObj.transform.SetParent(this.transform, false);
            this._waterBordersRenderer = gObj.AddComponent<SpriteRenderer>();
        }
        this._waterBordersRenderer.sprite = this.GetNoisySegmentsSprite(borders);
    }

    private void DrawRivers()
    {
        // draw noisy segments on texture
        var borders = new List<Tuple<Vector2, Vector2>>();
        Debug.Log("Drawing Rivers: " + this._world.Rivers.Count);
        foreach (var river in this._world.RiverList)
        {
            //Debug.Log("Points: " + river.Points.Length);
            int riverPointCount = river.Points.Length - 1;
            for (int p = 0; p < riverPointCount; p++)
            {
                var start = river.Points[p];
                var end = river.Points[p + 1];
                var edgeVector = new Tuple<Vector2, Vector2>(start, end);
                //Debug.Log(edgeVector);
                if (!borders.Contains(edgeVector)) borders.Add(edgeVector);
                if (borders.Exists(b => b != edgeVector && (b.First == end || b.Second == end))) break;
            }
        }

        if (this._riversRenderer == null)
        {
            var gObj = new GameObject("Rivers Renderer");
            gObj.transform.SetParent(this.transform, false);
            this._riversRenderer = gObj.AddComponent<SpriteRenderer>();
        }
        this._riversRenderer.sprite = this.GetNoisySegmentsSprite(borders);
    }
    #endregion

    void OnDrawGizmos()
    {
        if (this._world == null) return;

        Gizmos.color = Color.red;
        foreach (var region in this._world.RegionList)
        {
            Gizmos.DrawSphere(region.Location, 10f);
        }

        Gizmos.color = Color.white;
        foreach (var region in this._world.RegionList)
        {
            foreach (var border in region.Borders)
            {
                Gizmos.DrawLine(border.Point1, border.Point2);
            }
        }

        Gizmos.color = Color.magenta;
        foreach (var region in this._world.RegionList)
        {
            foreach (var border in region.Borders)
            {
                Gizmos.DrawLine(border.Region1Location, border.Region2Location);
            }
        }

        Gizmos.color = Color.blue;
        foreach (var river in this._world.RiverList)
        {
            int riverPointCount = river.Points.Length - 1;
            for (int p = 0; p < riverPointCount; p++)
            {
                Gizmos.DrawLine(river.Points[p], river.Points[p + 1]);
            }
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(new Vector2(0, 0), new Vector2(0, this.MapHeight));
        Gizmos.DrawLine(new Vector2(0, 0), new Vector2(this.MapWidth, 0));
        Gizmos.DrawLine(new Vector2(this.MapWidth, 0), new Vector2(this.MapWidth, this.MapHeight));
        Gizmos.DrawLine(new Vector2(0, this.MapHeight), new Vector2(this.MapWidth, this.MapHeight));
    }

    #region Helper Methods
    private bool IsInHeightRange(Region region, Vector2 range)
    {
        return region.Altitude >= range.x && region.Altitude <= range.y;
    }

    private Sprite GetNoisySegmentsSprite(IEnumerable<Tuple<Vector2, Vector2>> borders)
    {
        var texture = new Texture2D(this.MapWidth, this.MapHeight, TextureFormat.ARGB32, false);
        texture.Fill(Color.clear);
        var segmentScrambler = new SegmentScrambler(this.MinBorderSegmentationLength, this.SegmentSkewFactor);
        foreach (var border in borders)
        {
            var points = segmentScrambler.ScrambleLine(border.First, border.Second);
            for (int p = 0; p < points.Count - 1; p++)
            {
                texture.DrawLine(points[p], points[p + 1], Color.black);
            }
        }

        texture.Apply();
        return Sprite.Create(texture, new Rect(0, 0, this.MapWidth, this.MapHeight), new Vector2(0.5f, 0.5f), 1);
    }
    #endregion
}