﻿namespace Procedural.MapGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class Landform : IEquatable<Landform>
    {
        public string Name { get; set; }
        public BiomeDefinition Type { get; }
        public List<Region> Regions { get; }
        public int Size => this.Regions.Count;

        public Dictionary<string, float> Fauna { get; }
        public Dictionary<string, float> Flora { get; }

        public Landform(string name, BiomeDefinition type)
        {
            this.Name = name;
            this.Type = type;
            this.Regions = new List<Region>();
            this.Fauna = new Dictionary<string, float>();
            this.Flora = new Dictionary<string, float>();
        }

        public void ComputeFaunaAndFlora()
        {
            string[] faunaKeys = this.Regions.Select(r => r.Fauna.Keys.ToArray()).Aggregate((l, r) => l.Union(r).ToArray());
            foreach (string k in faunaKeys)
            {
                this.Fauna.Add(k, this.Regions.Average(r => r.Fauna.ContainsKey(k) ? r.Fauna[k] : 0));
            }
            Debug.Log(this.Name + " fauna: " + string.Join(", ", this.Fauna.Select(x => string.Format("{0}: {1}", x.Key, x.Value)).ToArray()));

            string[] floraKeys = this.Regions.Select(r => r.Flora.Keys.ToArray()).Aggregate((l, r) => l.Union(r).ToArray());
            foreach (string k in floraKeys)
            {
                this.Flora.Add(k, this.Regions.Average(r => r.Flora.ContainsKey(k) ? r.Flora[k] : 0));
            }
            Debug.Log(this.Name + " flora: " + string.Join(", ", this.Flora.Select(x => string.Format("{0}: {1}", x.Key, x.Value)).ToArray()));
        }

        #region IEquatable
        public bool Equals(Landform other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(this.Name, other.Name) && this.Type == other.Type;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Landform)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((this.Name != null ? this.Name.GetHashCode() : 0) * 397) ^ this.Type.GetHashCode();
            }
        }

        public static bool operator ==(Landform left, Landform right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Landform left, Landform right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}
