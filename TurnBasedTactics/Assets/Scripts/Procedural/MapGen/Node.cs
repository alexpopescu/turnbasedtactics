﻿namespace Procedural.MapGen
{
    using UnityEngine;

    public abstract class Node
    {
        public string Name { get; private set; }
        public Region Region { get; private set; }
        public Vector2 Location => this.Region.Location;

        protected Node(string name, Region region)
        {
            this.Name = name;
            this.Region = region;
        }
    }
}
