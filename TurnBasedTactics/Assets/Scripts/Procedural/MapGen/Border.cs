﻿namespace Procedural.MapGen
{
    using System;
    using UnityEngine;

    public class Border : IEquatable<Border>
    {
        public Region Region1 { get; private set; }
        public Region Region2 { get; private set; }
        public Vector2 Point1 { get; private set; }
        public Vector2 Point2 { get; private set; }

        public Vector2 Region1Location { get { return this.Region1.Location; } }
        public Vector2 Region2Location { get { return this.Region2.Location; } }

        public Border(Region region1, Region region2, Vector2 point1, Vector2 point2)
        {
            this.Region1 = region1;
            this.Region2 = region2;
            this.Point1 = point1;
            this.Point2 = point2;
        }

        public Region GetOtherRegion(Region region)
        {
            return this.Region1 == region ? this.Region2 : this.Region1;
        }

        #region IEquatable
        public bool Equals(Border other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(this.Region1, other.Region1) && Equals(this.Region2, other.Region2);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this.Equals((Border)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((this.Region1 != null ? this.Region1.GetHashCode() : 0) * 397)
                    ^ (this.Region2 != null ? this.Region2.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Border left, Border right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Border left, Border right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}
