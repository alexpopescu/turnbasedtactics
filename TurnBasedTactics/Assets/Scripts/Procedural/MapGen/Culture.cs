﻿using UnityEngine;

public class Culture
{
    /// <summary>
    /// The power distance index is defined as “the extent to which the less powerful members of organizations
    /// and institutions (like the family) accept and expect that power is distributed unequally.”
    /// In this dimension, inequality and power is perceived from the followers, or the lower level.
    /// A higher degree of the Index indicates that hierarchy is clearly established and executed in society,
    /// without doubt or reason. A lower degree of the Index signifies that people question authority and
    /// attempt to distribute power.
    /// </summary>
    public float PowerDistance { get; }

    /// <summary>
    /// This index explores the “degree to which people in a society are integrated into groups.”
    /// Individualistic societies have loose ties that often only relates an individual to his/her
    /// immediate family. They emphasize the “I” versus the “we.” Its counterpart, collectivism,
    /// describes a society in which tightly-integrated relationships tie extended families and others
    /// into in-groups. These in-groups are laced with undoubted loyalty and support each other when a
    /// conflict arises with another in-group.
    /// </summary>
    public float Individualism { get; }

    /// <summary>
    /// The uncertainty avoidance index is defined as “a society's tolerance for ambiguity,” in which
    /// people embrace or avert an event of something unexpected, unknown, or away from the status quo.
    /// Societies that score a high degree in this index opt for stiff codes of behavior, guidelines, laws,
    /// and generally rely on absolute truth, or the belief that one lone truth dictates everything and
    /// people know what it is. A lower degree in this index shows more acceptance of differing thoughts
    /// or ideas. Society tends to impose fewer regulations, ambiguity is more accustomed to, and the
    /// environment is more free-flowing.
    /// </summary>
    public float UncertaintyAvoidance { get; }

    /// <summary>
    /// In this dimension, masculinity is defined as “a preference in society for achievement, heroism,
    /// assertiveness and material rewards for success.” Its counterpart represents “a preference for
    /// cooperation, modesty, caring for the weak and quality of life.” Women in the respective societies
    /// tend to display different values. In feminine societies, they share modest and caring views
    /// equally with men. In more masculine societies, women are somewhat assertive and competitive, but
    /// notably less than men. In other words, they still recognize a gap between male and female values.
    /// This dimension is frequently viewed as taboo in highly masculine societies.
    /// </summary>
    public float Masculinity { get; }

    /// <summary>
    /// This dimension associates the connection of the past with the current and future actions/challenges.
    /// A lower degree of this index (short-term) indicates that traditions are honored and kept, while
    /// steadfastness is valued. Societies with a high degree in this index (long-term) view adaptation
    /// and circumstantial, pragmatic problem-solving as a necessity. A poor country that is short-term
    /// oriented usually has little to no economic development, while long-term oriented countries continue
    /// to develop to a point.
    /// </summary>
    public float LongTermOrientation { get; }

    /// <summary>
    /// This dimension is essentially a measure of happiness; whether or not simple joys are fulfilled.
    /// Indulgence is defined as “a society that allows relatively free gratification of basic and natural
    /// human desires related to enjoying life and having fun.” Its counterpart is defined as “a society
    /// that controls gratification of needs and regulates it by means of strict social norms.”
    /// Indulgent societies believe themselves to be in control of their own life and emotions;
    /// restrained societies believe other factors dictate their life and emotions.
    /// </summary>
    public float Indulgence { get; }

    public Culture(float powerDistance, float individualism, float uncertaintyAvoidance,
        float masculinity, float longTermOrientation, float indulgence)
    {
        this.PowerDistance = powerDistance;
        this.Individualism = individualism;
        this.UncertaintyAvoidance = uncertaintyAvoidance;
        this.Masculinity = masculinity;
        this.LongTermOrientation = longTermOrientation;
        this.Indulgence = indulgence;
    }

    public static Culture GenerateRandom()
    {
        return new Culture(Random.value, Random.value, Random.value, Random.value, Random.value, Random.value);
    }

    public override string ToString()
    {
        return string.Format("PDI={0}, IDV={1}, UAI={2}, MAS={3}, LTO={4}, IND={5}",
            this.PowerDistance, this.Individualism, this.UncertaintyAvoidance,
            this.Masculinity, this.LongTermOrientation, this.Indulgence);
    }
}
