﻿public enum LandformType { 
    Mountain,
    Hill,
    Forest,
    PineForest,
    Shrubland,
    Desert,
    Grassland,
    Water
}
