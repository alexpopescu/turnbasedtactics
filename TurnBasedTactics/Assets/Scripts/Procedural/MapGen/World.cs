﻿using System.Collections.Generic;
using System.Linq;
using Procedural.MapGen;
using UnityEngine;

public class World
{
    public int Width { get; }
    public int Height { get; }
    public Dictionary<Vector2, Region> Regions { get; }
    public IEnumerable<Region> RegionList => this.Regions.Values.ToArray();
    public Dictionary<string, Landform> Landforms { get; }
    public IEnumerable<Landform> LandformList => this.Landforms.Values.ToArray();
    public Dictionary<string, Lineform> Rivers { get; }
    public IEnumerable<Lineform> RiverList => this.Rivers.Values.ToArray();
    public Dictionary<string, City> Cities { get; }
    public IEnumerable<City> CityList => this.Cities.Values.ToArray();

    public World(int width, int height)
    {
        this.Width = width;
        this.Height = height;
        
        this.Regions = new Dictionary<Vector2, Region>();
        this.Landforms = new Dictionary<string, Landform>();
        this.Rivers = new Dictionary<string, Lineform>();
        this.Cities = new Dictionary<string, City>();
    }

    public void Clear()
    {
        this.Regions.Clear();
        this.Landforms.Clear();
        this.Rivers.Clear();
        this.Cities.Clear();
    }

    public void AddRegion(Region region)
    {
        this.Regions.Add(region.Location, region);
    }

    public void AddLandform(Landform landform)
    {
        this.Landforms.Add(landform.Name, landform);
    }

    public void AddRiver(Lineform lineform)
    {
        this.Rivers.Add(lineform.Name, lineform);
    }

    public void AddCity(City city)
    {
        this.Cities.Add(city.Name, city);
    }
}