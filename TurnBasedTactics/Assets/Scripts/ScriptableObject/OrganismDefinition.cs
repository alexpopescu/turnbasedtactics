﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data/Organism Definition")]
public class OrganismDefinition : ScriptableObject
{
    public float OccurrenceMean;
    public float OccurrenceStdDev;
    public float PopulationFactor;
    public float FoodFactor;
    public float FearFactor;
}
