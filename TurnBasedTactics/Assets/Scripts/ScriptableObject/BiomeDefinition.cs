﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[CreateAssetMenu(menuName = "Data/Biome Definition")]
public class BiomeDefinition : ScriptableObject
{
    public enum NamingType
    {
        History,
        Appearance,
        Fauna,
        Flora,
        Randomized
    }

    [Serializable]
    public class NamingEntry
    {
        public NamingType Type;
        [Range(0f, 1f)]
        public float Probability;
    }

    public string Name;
    public string BaseName;
    [IntervalRange(0f, 1f)]
    public Range AltitudeRange;
    [IntervalRange(0f, 1f)]
    public Range HumidityRange;

    public float GrassMultiplier;
    public float PlantMultiplier;
    public float TreeMultiplier;

    public Object[] ResourceFiles;

    public List<OrganismDefinition> Flora;
    private Dictionary<string, OrganismDefinition> _indexedFlora = null;
    public Dictionary<string, OrganismDefinition> IndexedFlora
    {
        get
        {
            if (this._indexedFlora == null || this._indexedFlora.Count != this.Flora.Count)
            {
                this._indexedFlora = new Dictionary<string, OrganismDefinition>();
                foreach (var plant in this.Flora)
                {
                    this._indexedFlora.Add(plant.name, plant);
                }
            }

            return this._indexedFlora;
        }
    }

    public List<OrganismDefinition> Fauna;
    private Dictionary<string, OrganismDefinition> _indexedFauna = null;
    public Dictionary<string, OrganismDefinition> IndexedFauna
    {
        get
        {
            if (this._indexedFauna == null || this._indexedFauna.Count != this.Fauna.Count)
            {
                this._indexedFauna = new Dictionary<string, OrganismDefinition>();
                foreach (var animal in this.Fauna)
                {
                    this._indexedFauna.Add(animal.name, animal);
                }
            }

            return this._indexedFauna;
        }
    }

    public NamingEntry[] Naming;

    public bool Contains(float altitude, float humidity)
    {
        return this.AltitudeRange.Contains(altitude) && this.HumidityRange.Contains(humidity);
    }
}
