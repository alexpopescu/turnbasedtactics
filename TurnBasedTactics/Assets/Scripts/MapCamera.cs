﻿using UnityEngine;

public class MapCamera : MonoBehaviour
{
    public float SlideSpeed = 10;

    private const float MIN_MOVEMENT = 0.001f;
    private Camera _camera;

    // Use this for initialization
    void Start()
    {
        this._camera = this.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        float scrollWheel = Input.GetAxis("Mouse ScrollWheel");
        if (scrollWheel < 0) // back
        {
            this._camera.orthographicSize = Mathf.Min(this._camera.orthographicSize + 10, 700);
        }
        if (scrollWheel > 0) // forward
        {
            this._camera.orthographicSize = Mathf.Max(this._camera.orthographicSize - 10, 200);
        }

        float xAxisValue = Input.GetAxis("Horizontal");
        float yAxisValue = Input.GetAxis("Vertical");
        if (Mathf.Abs(xAxisValue) > MIN_MOVEMENT || Mathf.Abs(yAxisValue) > MIN_MOVEMENT)
        {
            this._camera.transform.Translate(new Vector3(xAxisValue * this.SlideSpeed, yAxisValue * this.SlideSpeed, 0.0f));
        }
    }
}

