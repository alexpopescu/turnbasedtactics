﻿using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Vectrosity;

public class BattleManager : MonoBehaviour
{
    public const float ABOVE_GROUND = 0.3f;

    public CharacterInfo CharacterInfo;
    public NonPlayerCharacterInfo NpcInfo;
    public Squad PlayerSquad;
    public Squad AiSquad;

    private NavMeshPath _lastPath;
    private VectorLine _pathVectorLine;

    private bool _unitIsMoving = false;

    // Use this for initialization
    private void Start()
    {
        this._lastPath = new NavMeshPath();

        InputManager.Instance.OnNextUnit.AddListener(this.NextUnit);
        InputManager.Instance.OnUnitClick.AddListener(this.ClickUnit);
        InputManager.Instance.OnUnitHover.AddListener(this.PeekNpc);
        InputManager.Instance.OnGroundHover.AddListener(this.HoverGround);
        InputManager.Instance.OnGroundClick.AddListener(this.ClickGround);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void NextUnit()
    {
        this.PlayerSquad.NextUnit();
        this.CharacterInfo.SetUnitInfo(this.PlayerSquad.SelectedUnit.Unit);
    }

    private void ClickUnit(UnitControl unit)
    {
        if (unit.PlayerType == PlayerType.Human)
        {
            this.PlayerSquad.SetSelected(unit);
            this.CharacterInfo.SetUnitInfo(this.PlayerSquad.SelectedUnit.Unit);
        }
        else
        {
            if (this.PlayerSquad.HasSelectedUnit)
            {
                this.PlayerSquad.SelectedUnit.Target = unit;
            }
        }
    }

    private void ClickGround(Vector3 point)
    {
        if (this.PlayerSquad.HasSelectedUnit)
        {
            this.PlayerSquad.SelectedUnit.Move(point);
        }
    }

    private void PeekNpc(UnitControl npcControl)
    {
        this.NpcInfo.SetInfo(npcControl != null ? npcControl.Unit : null);
    }

    private void HoverGround(Vector3 point)
    {
        if (this.PlayerSquad.HasSelectedUnit)
        {
            this.GetAndDrawPath(this.PlayerSquad.SelectedUnit.transform.position, point);

        }
    }

    public NavMeshPath GetAndDrawPath(Vector3 start, Vector3 end)
    {
        //if (this._lastPath.corners.Length >= 2
        //    && Vector3.Distance(this._lastPath.corners[0], start) < 0.5
        //    && Vector3.Distance(this._lastPath.corners[this._lastPath.corners.Length - 1], end) < 0.5)
        //    return;

        if (NavMesh.CalculatePath(start, end, NavMesh.AllAreas, this._lastPath)
            && this._lastPath.status != NavMeshPathStatus.PathPartial)
        {
            //Debug.Log(this._lastPath.corners.Length);
            if (this._pathVectorLine == null)
            {
                this._pathVectorLine = VectorLine.SetLine3D(
                    Color.yellow,
                    this._lastPath.corners.Select(x => this.SetAboveGround(x)).ToArray());
                this._pathVectorLine.lineWidth = 5;
                this._pathVectorLine.joins = Joins.Weld;
            }
            else
            {
                this._pathVectorLine.points3.Clear();
                this._pathVectorLine.points3.AddRange(this._lastPath.corners.Select(x => this.SetAboveGround(x)).ToArray());
                this._pathVectorLine.Draw3D();
            }
            this._pathVectorLine.color = this._lastPath.status == NavMeshPathStatus.PathInvalid ? Color.red : Color.yellow;
            var distance = 0f;
            for (var i = 0; i < this._lastPath.corners.Length - 1; i++)
                distance += Vector3.Distance(this._lastPath.corners[i], this._lastPath.corners[i + 1]);
            this.CharacterInfo.SetInfoText(
                string.Format("{0:N2} / {1:N2} AP", distance / 5, this.PlayerSquad.SelectedUnit.Unit.CurrentActionPoints));
        }

        return this._lastPath;
    }

    private Vector3 SetAboveGround(Vector3 point, float y = 0.1f)
    {
        return new Vector3(point.x, y, point.z);
    }
}
