﻿using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;

    private Cooldown _keyCooldown;

    public static InputManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(InputManager)) as InputManager;

                if (_instance == null)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    _instance.Init();
                }
            }

            return _instance;
        }
    }

    private UnityEvent _onNextUnit;
    public UnityEvent OnNextUnit
    {
        get { return this._onNextUnit ?? (this._onNextUnit = new UnityEvent()); }
    }

    private UnitControlEvent _onUnitClick;
    public UnitControlEvent OnUnitClick
    {
        get { return this._onUnitClick ?? (this._onUnitClick = new UnitControlEvent()); }
    }

    private Vector3Event _onGroundClick;
    public Vector3Event OnGroundClick
    {
        get { return this._onGroundClick ?? (this._onGroundClick = new Vector3Event()); }
    }

    private UnitControlEvent _onUnitHover;
    public UnitControlEvent OnUnitHover
    {
        get { return this._onUnitHover ?? (this._onUnitHover = new UnitControlEvent()); }
    }

    private Vector3Event _onGroundHover;
    public Vector3Event OnGroundHover
    {
        get { return this._onGroundHover ?? (this._onGroundHover = new Vector3Event()); }
    }

    // Use this for initialization
    void Start()
    {
        this._keyCooldown = new Cooldown(0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
        {
            var unit = hit.transform.gameObject.GetComponent<UnitControl>();
            if (Input.GetMouseButtonDown(0))
            {
                if (unit != null)
                {
                    if (this._onUnitClick != null) this._onUnitClick.Invoke(unit);
                }
                else
                {
                    if (this._onGroundClick != null) this._onGroundClick.Invoke(hit.point);
                }
            }

            if (unit != null)
            {
                if (this._onUnitHover != null) this._onUnitHover.Invoke(unit);
            }
            else
            {
                if (this._onGroundHover != null) this._onGroundHover.Invoke(hit.point);
            }
        }

        this._keyCooldown.Update();
        if (this._keyCooldown.IsCooldownEnded && Input.GetKeyDown(KeyCode.Tab))
        {
            if (this._onNextUnit != null) this._onNextUnit.Invoke();
            this._keyCooldown.Trigger();
        }
    }

    private void Init()
    {
        
    }
}
