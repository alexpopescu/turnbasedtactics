﻿using UnityEngine;

public static class Texture2DExtensions
{
    public static void Fill(this Texture2D tex, Color col)
    {
        var fillPixels = new Color[tex.width * tex.height];
        for (int i = 0; i < fillPixels.Length; i++) fillPixels[i] = col;
        tex.SetPixels(fillPixels);
    }

    public static void DrawLine(this Texture2D tex, Vector2 v0, Vector2 v1, Color col)
    {
        tex.DrawLine((int)v0.x, (int)v0.y, (int)v1.x, (int)v1.y, col);
    }

    public static void DrawLine(this Texture2D tex, int x0, int y0, int x1, int y1, Color col)
    {
        int dy = y1 - y0;
        int dx = x1 - x0;
        int stepx, stepy;

        if (dy < 0) { dy = -dy; stepy = -1; } else { stepy = 1; }
        if (dx < 0) { dx = -dx; stepx = -1; } else { stepx = 1; }
        dy <<= 1;
        dx <<= 1;

        float fraction;

        tex.SetPixel(x0, y0, col);
        if (dx > dy)
        {
            fraction = dy - (dx >> 1);
            while (Mathf.Abs(x0 - x1) > 1)
            {
                if (fraction >= 0)
                {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                tex.SetPixel(x0, y0, col);
            }
        }
        else
        {
            fraction = dx - (dy >> 1);
            while (Mathf.Abs(y0 - y1) > 1)
            {
                if (fraction >= 0)
                {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                tex.SetPixel(x0, y0, col);
            }
        }
    }
}

