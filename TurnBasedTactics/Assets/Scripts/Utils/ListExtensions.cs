﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ListExtensions
{
    public static List<T> GetRandomElements<T>(this List<T> originalList, int count = 1)
    {
        var indices = new int[count];
        for (int i = 0; i < indices.Length; i++)
        {
            bool exists;
            do
            {
                indices[i] = Random.Range(0, originalList.Count);
                exists = false;
                for (int j = 0; j < i; j++)
                    if (indices[j] == indices[i]) exists = true;
            } while (exists);
        }
        return originalList.Where((obj, idx) => indices.Contains(idx)).ToList();
    }
}

