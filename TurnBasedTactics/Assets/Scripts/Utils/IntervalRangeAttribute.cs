﻿using UnityEngine;

public class IntervalRangeAttribute : PropertyAttribute
{
    public IntervalRangeAttribute(float minLimit, float maxLimit)
    {
        this.MinLimit = minLimit;
        this.MaxLimit = maxLimit;
    }

    public float MinLimit { get; private set; }

    public float MaxLimit { get; private set; }
}
