﻿using System;
using System.Collections;
using UnityEngine;

static class MonoBehaviourExtensions
{
    public static IEnumerator RunWithDelay(this MonoBehaviour monoBehaviour, Action action, float seconds)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }
}

