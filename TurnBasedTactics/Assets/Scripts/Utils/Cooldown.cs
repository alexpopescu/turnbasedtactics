﻿using System;
using UnityEngine;

class Cooldown
{
    public delegate void CooldownEndedHandler(object sender, EventArgs e);

    public event CooldownEndedHandler OnCooldownEnded;

    private readonly float _seconds;
    private float _currentTimer;
    private bool _isCooling;

    public bool IsCooldownEnded { get { return !this._isCooling; } }

    public Cooldown(float seconds)
    {
        this._seconds = seconds;
    }

    public void Update()
    {
        if (this._isCooling)
        {
            this._currentTimer += Time.deltaTime;

            if (this._currentTimer >= this._seconds)
            {
                this.Reset();
                if (this.OnCooldownEnded != null)
                    this.OnCooldownEnded.Invoke(this, null);
            }
        }
    }

    private void Reset(bool trigger=false)
    {
        this._currentTimer = 0;
        this._isCooling = trigger;
    }

    public void Trigger()
    {
        this.Reset(true);
    }
}

