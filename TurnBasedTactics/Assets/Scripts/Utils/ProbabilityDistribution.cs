﻿using System;
using Random = UnityEngine.Random;

[Serializable]
public class ProbabilityDistribution
{
    public float Mean;
    public float StdDev;

    public ProbabilityDistribution(float mean, float stdDev)
    {
        this.Mean = mean;
        this.StdDev = stdDev;
    }

    public float GetObservation()
    {
        return Random.value * this.StdDev + this.Mean;
    }
}
