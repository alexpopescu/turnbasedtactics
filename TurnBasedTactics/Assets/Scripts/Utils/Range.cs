﻿using System;
using Random = UnityEngine.Random;

[Serializable]
public class Range
{
    public float Start;
    public float End;

    public Range(float start, float end)
    {
        this.Start = start;
        this.End = end;
    }

    public override string ToString()
    {
        return string.Format("[{0}, {1}]", this.Start, this.End);
    }

    public float GetRandomValue()
    {
        return Random.Range(this.Start, this.End);
    }

    public bool Contains(float value)
    {
        return value >= this.Start && value <= this.End;
    }

    public void Stretch(float value)
    {
        if (this.Start > value) this.Start = value;
        if (this.End < value) this.End = value;
    }
}