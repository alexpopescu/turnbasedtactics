﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Tuple<T1, T2>
{
    public T1 First;
    public T2 Second;

    private static readonly IEqualityComparer Item1Comparer = EqualityComparer<T1>.Default;
    private static readonly IEqualityComparer Item2Comparer = EqualityComparer<T2>.Default;

    public Tuple(T1 first, T2 second)
    {
        this.First = first;
        this.Second = second;
    }

    public override string ToString()
    {
        return string.Format("<{0}, {1}>", this.First, this.Second);
    }

    public static bool operator ==(Tuple<T1, T2> a, Tuple<T1, T2> b)
    {
        if (IsNull(a) && !IsNull(b)) return false;
        if (!IsNull(a) && IsNull(b)) return false;
        if (IsNull(a) && IsNull(b)) return true;

        return a.First.Equals(b.First) && a.Second.Equals(b.Second);
    }

    public static bool operator !=(Tuple<T1, T2> a, Tuple<T1, T2> b)
    {
        return !(a == b);
    }

    public override int GetHashCode()
    {
        int hash = 17;
        hash = hash * 23 + this.First.GetHashCode();
        hash = hash * 23 + this.Second.GetHashCode();
        return hash;
    }

    public override bool Equals(object obj)
    {
        var other = obj as Tuple<T1, T2>;
        if (ReferenceEquals(other, null))
            return false;
        return Item1Comparer.Equals(this.First, other.First) &&
               Item2Comparer.Equals(this.Second, other.Second);
    }

    private static bool IsNull(object obj)
    {
        return ReferenceEquals(obj, null);
    }
}