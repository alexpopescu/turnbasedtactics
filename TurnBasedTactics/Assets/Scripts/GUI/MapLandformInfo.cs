﻿using UnityEngine;
using UnityEngine.UI;

public class MapLandformInfo : MonoBehaviour
{
    public string Name { get; set; }
    public Text Tooltip { get; set; }

    void OnMouseEnter()
    {
        this.Tooltip.text = this.Name;
        var pos = this.transform.position;
        this.Tooltip.rectTransform.anchoredPosition = pos;
        this.Tooltip.enabled = true;
    }

    void OnMouseExit()
    {
        this.Tooltip.enabled = false;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
