﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterInfo : MonoBehaviour
{
    public Text Name;
    public Slider Strength;
    public Text Info;

    // Use this for initialization
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetUnitInfo(Unit character)
    {
        this.gameObject.SetActive(character != null);
        if (character == null) return;

        this.Name.text = character.Name;
        this.Strength.maxValue = character.MaxStrength;
        this.Strength.value = character.CurrentStrength;
    }

    public void SetInfoText(string text)
    {
        this.Info.text = text;
    }
}
