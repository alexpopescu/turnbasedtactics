﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NonPlayerCharacterInfo : MonoBehaviour
{
    public Text Name;
    public Text Strength;

    private Dictionary<StrengthQualifier, Color> _strengthQualifierColors;

    // Use this for initialization
    void Start()
    {
        this.gameObject.SetActive(false);

        this._strengthQualifierColors = new Dictionary<StrengthQualifier, Color>
        {
            { StrengthQualifier.Dead, Color.black },
            { StrengthQualifier.Low, Color.red },
            { StrengthQualifier.Medium, Color.yellow },
            { StrengthQualifier.High, Color.green },
            { StrengthQualifier.Full, Color.cyan },
        };
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetInfo(Unit character)
    {
        this.gameObject.SetActive(character != null);
        if (character == null) return;

        this.Name.text = character.Name;
        this.Strength.text = character.CurrentStrengthQualifier.ToString();
        this.Strength.color = this._strengthQualifierColors[character.CurrentStrengthQualifier];
    }
}
