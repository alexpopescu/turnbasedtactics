﻿using System;
using UnityEngine;

public class Squad : MonoBehaviour
{
    public UnitControl[] Characters;

    private int _selectedUnitIndex = -1;

    public bool HasSelectedUnit { get { return this._selectedUnitIndex > -1; } }

    public UnitControl SelectedUnit
    {
        get
        {
            if (this._selectedUnitIndex < 0 || this._selectedUnitIndex >= this.Characters.Length)
                return null;
            return this.Characters[this._selectedUnitIndex];
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetSelected(int index)
    {
        if (this.HasSelectedUnit)
            this.Characters[this._selectedUnitIndex].SetSelected(false);
        this._selectedUnitIndex = index;
        this.Characters[index].SetSelected(true);
        // ISTopDownCamera.SetTargetObjectForMain(this.Characters[index].transform);
    }

    public void SetSelected(UnitControl unit)
    {
        for (var i = 0; i < this.Characters.Length; i++)
        {
            if (this.Characters[i] == unit)
            {
                this.SetSelected(i);
                return;
            }
        }
        throw new ArgumentOutOfRangeException("unit");
    }

    public void NextUnit()
    {
        int nextIndex = this._selectedUnitIndex+1;
        if (nextIndex >= this.Characters.Length) nextIndex = 0;
        this.SetSelected(nextIndex);
    }

    public void ResetTurn()
    {
        foreach (var character in this.Characters)
        {
            character.Unit.ResetTurn();
        }
    }
}
